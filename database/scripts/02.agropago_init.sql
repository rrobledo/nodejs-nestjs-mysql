--
-- Dumping data for table api_bank
--
INSERT INTO tenant_config (id, decidir_api_key, payment_api_key, agropago_rate, agropago_iva_rate, operation_iva_rate, term_iva_rate, installment_iva_rate) values
(1, 'ca5b6ce5ac5048e196ece9506a2592ce', '4vc50CuNw68VSjkJctjZqyXa8OGSprXUV3dWR43CWKrjUaaHr74NBo6RfK1F', 0.01, 0.21, 0.21, 0.105, 0.105);

INSERT INTO bank (id, when_created, when_last_edit, is_deleted, when_deleted, name, initials, who_created_id, who_deleted_id, who_last_edit_id) VALUES
(1, '2017-05-15 23:52:00.000000', '2017-05-15 23:52:00.000000', 0, NULL, 'Banco Nación', 'BNA', 1, NULL, 1),
(2, '2017-05-15 23:56:55.000000', '2017-05-15 23:56:55.000000', 0, NULL, 'Banco Galicia', 'BG', 1, NULL, 1),
(3, '2017-05-15 23:56:55.000000', '2017-05-15 23:56:55.000000', 0, NULL, 'Santander Río', 'SAN', 1, NULL, 1),
(4, '2017-05-15 23:56:55.000000', '2017-05-15 23:56:55.000000', 1, '2018-04-30 16:33:27.000000', 'Macro', 'BMA', 1, 1, 1),
(5, '2017-05-15 23:56:55.000000', '2017-05-15 23:56:55.000000', 0, NULL, 'BBVA Francés', 'BBVA', 1, NULL, 1),
(6, '2017-05-15 23:56:55.000000', '2017-05-15 23:56:55.000000', 0, NULL, 'ICBC', 'ICBC', 1, NULL, 1),
(7, '2017-12-26 22:04:45.000000', '2017-12-26 22:04:45.000000', 0, NULL, 'HSBC', 'HSBC', 1, NULL, 1),
(8, '2018-04-30 16:32:51.000000', '2018-04-30 16:32:51.000000', 0, NULL, 'Banco de Córdoba', 'BCOR', 1, NULL, 1),
(9, '2018-04-30 16:32:51.000000', '2018-04-30 16:32:51.000000', 0, NULL, 'Credicoop', 'CREDICOOP', 1, NULL, 1),
(10, '2018-04-30 16:32:51.000000', '2018-04-30 16:32:51.000000', 0, NULL, 'Banco de Entre Ríos', 'BER', 1, NULL, 1),
(11, '2018-04-30 16:32:51.000000', '2018-04-30 16:32:51.000000', 0, NULL, 'Patagonia', 'BPAT', 1, NULL, 1),
(12, '2018-04-30 16:32:51.000000', '2018-04-30 16:32:51.000000', 0, NULL, 'Banco de Santa Fe', 'BSF', 1, NULL, 1),
(13, '2018-04-30 16:32:51.000000', '2018-04-30 16:32:51.000000', 0, NULL, 'Banco de La Pampa', 'BLP', 1, NULL, 1),
(14, '2018-04-30 16:32:51.000000', '2018-04-30 16:32:51.000000', 0, NULL, 'Banco de Formosa', 'BF', 1, NULL, 1),
(15, '2018-04-30 16:32:51.000000', '2018-04-30 16:32:51.000000', 0, NULL, 'Banco de Corrientes', 'BC', 1, NULL, 1),
(16, '2018-04-30 16:32:51.000000', '2018-04-30 16:32:51.000000', 0, NULL, 'Banco Ciudad', 'CIUDAD', 1, NULL, 1),
(17, '2018-04-30 16:32:51.000000', '2018-04-30 16:32:51.000000', 0, NULL, 'Supervielle', 'SV', 1, NULL, 1);

INSERT INTO payment_types(payment_type, currency, description, hidden)
values ('CREDIT_CARD', 'ARS', 'TARJETA DE CREDITO EN PESOS', 1);
INSERT INTO payment_types(payment_type, currency, description, hidden)
values ('VIRTUAL_CASH', 'ARS', 'VIRTUAL CASH EN PESOS', 0);

--
-- Insert Payment Methods
--
INSERT INTO payment_method(id, bank_id, name, assets, payment_type, payment_index, operation_rate, options, enabled) values(
1,
8,
'Bancor',
'{
	"logo": "../assets/images/banks/bancor.png",
	"cc_logo": "../assets/images/banks/bancor_card.png"
}',
'CREDIT_CARD',
'1234',
'0.02',
'
{
	"mask": 1234,
	"payment_method_id": 84,
	"card_type": "agro",
	"terms": [
		{
			"term": 30,
			"term_labels": "Contado",
			"rate": 0,
			"rate_label": "Contado"
		},
		{
			"term": 60,
			"term_labels": "Hasta 60",
			"rate": 0.5,
			"rate_label": "50% Tasa TNA."
		},
		{
			"term": 90,
			"term_labels": "Hasta 90",
			"rate": 0.5,
			"rate_label": "50% Tasa TNA."
		},
		{
			"term": 120,
			"term_labels": "Hasta 120",
			"rate": 0.5,
			"rate_label": "50% Tasa TNA."
		}
	],
	"installments": [
		{
			"installment": 1,
			"label": "Contado",
			"rate": 0,
			"rate_label": "Contado"
		},
        {
			"installment": 13,
			"label": "Ahora 3",
			"rate": 0.0245,
			"rate_label": "2,45% TNA"
		},
        {
			"installment": 16,
			"label": "Ahora 6",
			"rate": 0.0476,
			"rate_label": "4,76% TNA"
		},
        {
			"installment": 7,
			"label": "Ahora 12",
			"rate": 0.0912,
			"rate_label": "9,12% TNA"
		}
	]
}
',
true
);


INSERT INTO payment_method(id, bank_id, name, assets, payment_type, payment_index, operation_rate, options, enabled) values(
2,
2,
'Galicia Rural',
'{
	"logo": "../assets/images/banks/galicia.png",
	"cc_logo": "../assets/images/banks/galicia_card.png"
}',
'CREDIT_CARD',
'4765',
'0.02',
'
{
	"mask": 4765,
	"payment_method_id": 98,
	"card_type": "agro",
	"terms": [
		{
			"term": 30,
			"term_labels": "Contado",
			"rate": 0,
			"rate_label": "Contado"
		},
		{
			"term": 60,
			"term_labels": "Hasta 60",
			"rate": 0.82,
			"rate_label": "82% Tasa TNA."
		},
		{
			"term": 150,
			"term_labels": "Hasta 150",
			"rate": 0.82,
			"rate_label": "82% Tasa TNA."
		},
		{
			"term": 180,
			"term_labels": "Hasta 180",
			"rate": 0.82,
			"rate_label": "82% Tasa TNA."
		}
	],
	"installments": [
		{
			"installment": 1,
			"label": "Contado",
			"rate": 0,
			"rate_label": "Contado"
		}
	]
}
',
true
);


commit;