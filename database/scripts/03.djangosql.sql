-- phpMyAdmin SQL Dump
-- version 4.7.1
-- https://www.phpmyadmin.net/
--
-- Host: mysql
-- Generation Time: Jan 10, 2019 at 07:41 PM
-- Server version: 5.7.18
-- PHP Version: 7.0.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: django
--
-- ==================================================
-- Table django_content_type
-- ==================================================
CREATE TABLE django_content_type (
  id int(11) NOT NULL AUTO_INCREMENT,
  app_label varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  model varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY django_content_type_app_label_45f3b1d93ec8c61c_uniq (app_label,model)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ==================================================
-- Table django_admin_log
-- ==================================================
CREATE TABLE django_admin_log (
  id int(11) NOT NULL AUTO_INCREMENT,
  action_time datetime(6) NOT NULL,
  object_id longtext COLLATE utf8_unicode_ci,
  object_repr varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  action_flag smallint(5) UNSIGNED NOT NULL,
  change_message longtext COLLATE utf8_unicode_ci NOT NULL,
  content_type_id int(11) DEFAULT NULL,
  user_id int(11) NOT NULL,
  
  PRIMARY KEY (id),
  KEY djang_content_type_id_697914295151027a_fk_django_content_type_id (content_type_id),
  KEY django_admin_log_user_id_52fdd58701c5f563_fk_security_user_id (user_id),
  CONSTRAINT djang_content_type_id_697914295151027a_fk_django_content_type_id FOREIGN KEY (content_type_id) REFERENCES django_content_type (id),
  CONSTRAINT django_admin_log_user_id_52fdd58701c5f563_fk_security_user_id FOREIGN KEY (user_id) REFERENCES security_user (id)
  
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ==================================================
-- Table django_migrations
-- ==================================================
CREATE TABLE django_migrations (
  id int(11) NOT NULL AUTO_INCREMENT,
  app varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  name varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  applied datetime(6) NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- ==================================================
-- Table django_session
-- ==================================================
CREATE TABLE django_session (
  session_key varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  session_data longtext COLLATE utf8_unicode_ci NOT NULL,
  expire_date datetime(6) NOT NULL,
  PRIMARY KEY (session_key),
  KEY django_session_de54fa62 (expire_date)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ==================================================
-- Table general_log
-- ==================================================
CREATE TABLE general_log (
  event_time timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  user_host mediumtext NOT NULL,
  thread_id bigint(21) unsigned NOT NULL,
  server_id int(10) unsigned NOT NULL,
  command_type varchar(64) NOT NULL,
  argument mediumtext NOT NULL
) ENGINE=CSV DEFAULT CHARSET=utf8 COMMENT='General log';

-- ==================================================
-- Table base_objectsequence 
-- ==================================================
CREATE TABLE base_objectsequence (
  id int(11) NOT NULL AUTO_INCREMENT,
  subclass varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  value int(10) UNSIGNED NOT NULL,
  content_type_id int(11) NOT NULL,
  
  PRIMARY KEY (id),
  UNIQUE KEY base_objectsequence_subclass_50d06d6ac20b1414_uniq (subclass,content_type_id),
  KEY base__content_type_id_3e39e538101ff18f_fk_django_content_type_id (content_type_id),
  CONSTRAINT base__content_type_id_3e39e538101ff18f_fk_django_content_type_id FOREIGN KEY (content_type_id) REFERENCES django_content_type (id)

) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
