-- phpMyAdmin SQL Dump
-- version 4.7.1
-- https://www.phpmyadmin.net/
--
-- Host: mysql
-- Generation Time: Jan 10, 2019 at 07:41 PM
-- Server version: 5.7.18
-- PHP Version: 7.0.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: identity
--
-- ==================================================
-- Table tenants
-- ==================================================
CREATE TABLE tenant (
  id 				int(11) 		NOT NULL,
  
  name		 		varchar(150) 	COLLATE utf8_unicode_ci NOT NULL,
  
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ==================================================
-- Table users
-- ==================================================
CREATE TABLE users (
  id 				int(11) 		NOT NULL AUTO_INCREMENT,
  tenant_id			int(11) 		NOT NULL,
  
  password 			varchar(128) 	NULL,
 
  email 			varchar(100) 	NOT NULL,
  first_name 		varchar(150) 	NULL,
  last_name 		varchar(150) 	NULL,
  
  birthdate 		date 			DEFAULT NULL,
  cuit 				varchar(11) 	DEFAULT NULL,
  dni 				varchar(8) 		NULL,
  legal_name 		varchar(150) 	NULL,
  phone 			varchar(20) 	NULL,
  terms_accepted 	tinyint(1) 		NOT NULL DEFAULT 0,

  new_email 		varchar(100) 	NULL,

  is_active 		tinyint(1) 		NOT NULL,
  when_active 		datetime(6) 	DEFAULT NULL,

  valid_email 		tinyint(1) 		NOT NULL DEFAULT FALSE,
  when_valid_email 	datetime(6) 	DEFAULT NULL,

  when_inactive 	datetime(6) 	DEFAULT NULL,
  who_inactive_id 	int(11) 		DEFAULT NULL,

  is_superuser 		tinyint(1) 		NOT NULL DEFAULT FALSE,
  last_login 		datetime(6) 	DEFAULT NULL,
  
  when_created 		datetime	 	NOT NULL DEFAULT CURRENT_TIMESTAMP,
  who_created_id 	int(11) 		DEFAULT NULL,

  when_last_edit 	datetime	 	NOT NULL DEFAULT CURRENT_TIMESTAMP,
  who_last_edit_id 	int(11) 		DEFAULT NULL,

  is_deleted 		tinyint(1) 		NOT NULL DEFAULT FALSE,
  when_deleted 		datetime(6) 	DEFAULT NULL,
  who_deleted_id 	int(11) 		DEFAULT NULL,
  
  PRIMARY KEY (id),
  FOREIGN KEY (tenant_id) REFERENCES tenant(id),
  UNIQUE KEY email (email),
  UNIQUE KEY users_cuit_21f1721fe03237fb_uniq (cuit),
  KEY users_3db7701a (who_inactive_id),
  KEY users_who_created_id_43334463411c1d5_fk_users_id (who_created_id),
  KEY security_use_who_deleted_id_783524641b7a708e_fk_users_id (who_deleted_id),
  KEY security_us_who_last_edit_id_b9569b25dba4c34_fk_users_id (who_last_edit_id),
  FOREIGN KEY (who_inactive_id) REFERENCES users (id),
  FOREIGN KEY (who_last_edit_id) REFERENCES users (id),
  FOREIGN KEY (who_deleted_id) REFERENCES users (id),
  FOREIGN KEY (who_created_id) REFERENCES users (id)

) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- ==================================================
-- Table authtoken_token
-- ==================================================

CREATE TABLE authtoken_token (
  `key` 		varchar(40) 	COLLATE utf8_unicode_ci NOT NULL,
  created 		datetime(6) 	NOT NULL,
  user_id 		int(11) 		NOT NULL,
  
  PRIMARY KEY (`key`),
  UNIQUE KEY user_id (user_id),
  FOREIGN KEY (user_id) REFERENCES users (id)
  
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- ==================================================
-- Table validation_codes
-- ==================================================
CREATE TABLE validation_codes (
  id 				int(11) 		NOT NULL AUTO_INCREMENT,
  user_id 			int(11) 		NOT NULL,
  code			 	int(6)  		NOT NULL,
  created 			datetime	 	NOT NULL DEFAULT CURRENT_TIMESTAMP,
  
  PRIMARY KEY (id),
  UNIQUE KEY user_id_code (user_id, code),
  CONSTRAINT validation_codes__user_fk_users_id FOREIGN KEY (user_id) REFERENCES users (id)
  
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

