--
-- Dumping data for table security_user
--

INSERT INTO tenant (id, name) values(1, 'agropago');

INSERT INTO users(id, tenant_id, password, last_login, is_superuser, when_created, when_last_edit, is_deleted, when_deleted, email, first_name, last_name, is_active, who_created_id, who_deleted_id, who_last_edit_id, birthdate, cuit, dni, valid_email, when_inactive, when_valid_email, who_inactive_id, when_active, legal_name, new_email, phone) VALUES
(1, 1, 'pbkdf2_sha256$20000$PqBgGbm0xXi1$SWmy4zeF/hR8TI0+66E+Phos3QTSyVn8YWEjqFOAODQ=', '2019-01-10 18:41:12.607879', 1, '2017-06-14 22:57:23.664232', '2017-06-14 22:57:23.664535', 0, NULL, 'agropago.adm@gmail.com', 'Admin', 'User', 1, 1, NULL, 1, '1980-01-01', '20301231238', '30123123', 1, NULL, '2017-06-16 21:02:40.000000', NULL, NULL, 'Agropago SAS', '', '351-5505050');
