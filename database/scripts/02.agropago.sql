SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

SET SQL_SAFE_UPDATES = 0;

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: agropago
--

-- ==================================================
-- Table tenant_config
-- ==================================================
CREATE TABLE IF NOT EXISTS tenant_config (
  id 					int(11) 		NOT NULL,

  decidir_api_key 		varchar(150) 	NOT NULL,
  payment_api_key 		varchar(150) 	NOT NULL,
  agropago_rate 		float			NULL DEFAULT 0.00,
  agropago_iva_rate 	float			NULL DEFAULT 0.00,
  operation_iva_rate 	float			NULL DEFAULT 0.00,
  term_iva_rate 		float			NULL DEFAULT 0.00,
  installment_iva_rate 	float			NULL DEFAULT 0.00,
  
  PRIMARY KEY (id),
  FOREIGN KEY (id) REFERENCES tenant(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- ==================================================
-- Table bank
-- ==================================================
CREATE TABLE IF NOT EXISTS bank (
  id 				int(11) 		NOT NULL,
  name 				varchar(150) 	COLLATE utf8_unicode_ci NOT NULL,
  initials 			varchar(50) 	COLLATE utf8_unicode_ci NOT NULL,

  when_created 		datetime    	NOT NULL,
  who_created_id 	int(11) 		DEFAULT NULL,

  when_last_edit 	datetime    	NOT NULL,
  who_last_edit_id 	int(11) 		DEFAULT NULL,

  is_deleted 		tinyint(1) 		NOT NULL,
  when_deleted 		datetime    	DEFAULT NULL,
  who_deleted_id 	int(11) 		DEFAULT NULL,
  
  PRIMARY KEY (id),
  FOREIGN KEY (who_created_id) REFERENCES users (id),
  FOREIGN KEY (who_deleted_id) REFERENCES users (id),
  FOREIGN KEY (who_last_edit_id) REFERENCES users (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
ALTER TABLE bank
  MODIFY id int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

-- ==================================================
-- Table payment_types
-- ==================================================
CREATE TABLE IF NOT EXISTS payment_types (
  payment_type		char(15)		NOT NULL,
  currency			char(4)			NOT NULL,
  hidden			tinyint(1) 		NOT NULL DEFAULT 1,
  
  description		varchar(50)		NOT NULL,
  PRIMARY KEY (payment_type, currency)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ==================================================
-- Table payment_method
-- ==================================================
CREATE TABLE IF NOT EXISTS payment_method (
  id 					int(11) 		NOT NULL AUTO_INCREMENT,
  bank_id 				int(11) 		NOT NULL,

  name 					varchar(30) 	NOT NULL,
  assets				json			NOT NULL,
  
  payment_type			char(15)     	NOT NULL,
  payment_index			char(30)     	NOT NULL,
  operation_rate 		float 			NOT NULL,

  options				json			NOT NULL,
  
  enabled				tinyint(1) 		NOT NULL DEFAULT 1,
  
  PRIMARY KEY (id),
  INDEX (payment_type, payment_index),
  FOREIGN KEY (bank_id) REFERENCES bank(id),
  FOREIGN KEY (payment_type) REFERENCES payment_types(payment_type)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ==================================================
-- Table user_bank_account
-- ==================================================
CREATE TABLE IF NOT EXISTS user_bank_account (
  id 				int(11) 		NOT NULL AUTO_INCREMENT,

  bank_id 			int(11) 		NOT NULL,
  user_id 			int(11) 		NOT NULL,
  
  account_number 	varchar(25) 	COLLATE utf8_unicode_ci NOT NULL,
  cbu 				varchar(22) 	COLLATE utf8_unicode_ci NOT NULL,
  alias 			varchar(50) 	COLLATE utf8_unicode_ci NOT NULL,

  valid_account 	tinyint(1) 		NOT NULL,
  
  PRIMARY KEY (id),
  FOREIGN KEY (bank_id) REFERENCES bank (id),
  FOREIGN KEY (user_id) REFERENCES users (id)
  
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ==================================================
-- Table payment_order
-- ==================================================
CREATE TABLE IF NOT EXISTS payment_order (
  id 				int(11) 		NOT NULL AUTO_INCREMENT,
  uuid 				char(50) 		NOT NULL,
  code 				varchar(50) 	NULL,

  date 				datetime 		NOT NULL DEFAULT CURRENT_TIMESTAMP,
  status 			char(15) 		NOT NULL DEFAULT 'PENDING' CHECK( status in ('PENDING', 'REVIEW', 'ACCEPTED', 'PAID', 'CANCELED')),

  beneficiary_id 	int(11) 		NOT NULL,
  payer_email 		varchar(100) 	COLLATE utf8_unicode_ci NOT NULL,
  payer_id 			int(11) 		NOT NULL,

  payment_order_type 	char(15) 	NOT NULL DEFAULT 'TWO_FACES' CHECK( status in ('TWO_FACES', 'FACE_TO_FACE')),
  accepted_payment_methods 	json	NULL,

  reference_number 	varchar(150) 	COLLATE utf8_unicode_ci NOT NULL,
  invoice_number 	varchar(50) 	COLLATE utf8_unicode_ci NOT NULL,
  description 		longtext 		COLLATE utf8_unicode_ci NOT NULL,

  amount 			double 			NOT NULL,
  amount_to_transfer double 		DEFAULT NULL,
  available_amount 	double 			DEFAULT NULL,

  when_paid 		datetime    	NULL,

  PRIMARY KEY (id),
  UNIQUE KEY (uuid),
  FOREIGN KEY (beneficiary_id) REFERENCES users (id),
  FOREIGN KEY (payer_id) REFERENCES users (id)

) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ==================================================
-- Table payment_order_detail
-- ==================================================
CREATE TABLE IF NOT EXISTS payment_order_detail (
  id 				int(11) 		NOT NULL AUTO_INCREMENT,
  payment_order_id 	int(11) 		NOT NULL,
  
  item_id			varchar(20) 	NULL,
  description		varchar(100) 	NOT NULL,
  
  amount 			double 			NOT NULL,

  PRIMARY KEY (id),
  FOREIGN KEY (payment_order_id) REFERENCES payment_order (id)

) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ==================================================
-- Table payment
-- ==================================================
CREATE TABLE IF NOT EXISTS payment (
  id 				int(11) 			NOT NULL AUTO_INCREMENT,
  uuid 				char(50) 			NOT NULL,
  code 				varchar(50) 		NULL,

  date 				datetime 			NOT NULL DEFAULT CURRENT_TIMESTAMP,
  status 			char(15) 			NOT NULL DEFAULT 'ACCEPTED' CHECK( status in ('REVIEW', 'ACCEPTED', 'REJECTED', 'CANCELED', 'PAID')),

  amount			double 				NOT NULL,
  payment_order_id 	int(11) 			NOT NULL,

  fees				json 				DEFAULT NULL,

  PRIMARY KEY (id),
  UNIQUE KEY (uuid),
  FOREIGN KEY (payment_order_id) REFERENCES payment_order (id)
  
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ==================================================
-- Table transfer
-- ==================================================
CREATE TABLE IF NOT EXISTS transfer (
  id 				int(11) 		NOT NULL AUTO_INCREMENT,
  uuid 				char(50) 		NOT NULL,
  code 				varchar(50) 	NULL,

  date 				datetime 		NOT NULL DEFAULT CURRENT_TIMESTAMP,
  
  status 			char(15) 		NOT NULL DEFAULT 'TRANSFERRED' CHECK( status in ('TRANSFERRED', 'CANCELED')),

  sender_id 		int(11) 		NOT NULL,
  receiver_email 	varchar(100) 	NULL,
  receiver_id 		int(11) 		NOT NULL,

  description 		longtext 		NOT NULL,

  amount 			double 			NOT NULL,
  amount_to_transfer double 		DEFAULT NULL,

  fees				json 			DEFAULT NULL,
 
  validation_code	int(6)  		NOT NULL,
  
  PRIMARY KEY (id),
  UNIQUE KEY (uuid),
  FOREIGN KEY (sender_id) REFERENCES users (id),
  FOREIGN KEY (receiver_id) REFERENCES users (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ==================================================
-- Table checkout
-- ==================================================
CREATE TABLE IF NOT EXISTS checkout (
  id 				int(11) 		NOT NULL AUTO_INCREMENT,
  uuid 				char(50) 		NOT NULL,
  code 				varchar(50) 	NULL,

  date 				datetime 		NOT NULL DEFAULT CURRENT_TIMESTAMP,
  status 			char(15) 		NOT NULL DEFAULT 'PENDING' CHECK( status in ('PENDING', 'REJECTED', 'TRANSFERRED', 'CANCELED')),

  user_id 			int(11) 		NOT NULL,
  user_bank_account_id int(11) 		DEFAULT NULL,

  amount 			double 			NOT NULL,
  amount_to_transfer double 		DEFAULT NULL,

  fees				json 			DEFAULT NULL,
 
  validation_code	int(6)  		NOT NULL,

  transfer_date		datetime	 	NULL,
  transaction_code	varchar(50)		NULL,

  PRIMARY KEY (id),
  UNIQUE KEY (uuid),
  FOREIGN KEY (user_id) REFERENCES users (id),
  FOREIGN KEY (user_bank_account_id) REFERENCES user_bank_account (id)

) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ==================================================
-- Table transaction_values
-- ==================================================
CREATE TABLE IF NOT EXISTS transaction_values (
  id 				int(11) 		NOT NULL AUTO_INCREMENT,

  process_date 		datetime 		NOT NULL DEFAULT CURRENT_TIMESTAMP,

  operation_type	char(20)		NOT NULL,
  user_id 			int(11) 		NOT NULL,
  afected_id 		int(11) 		NOT NULL,
  
  status 			char(15) 		NOT NULL DEFAULT 'ACCEPTED' CHECK( status in ('ACCEPTED', 'REVIEW', 'REJECTED', 'CANCELED')),

  payment_type		char(15)		NOT NULL,
  payment_method_id	int(11) 		NULL,
  currency			char(4)			NOT NULL,

  amount 			double 			NOT NULL,

  payment_id 		int(11) 		NULL,
  transfer_id 		int(11) 		NULL,
  checkout_id		int(11) 		NULL,

  PRIMARY KEY (id),
  FOREIGN KEY (payment_type, currency) REFERENCES payment_types (payment_type, currency),
  FOREIGN KEY (payment_method_id) REFERENCES payment_method (id),
  FOREIGN KEY (payment_id) REFERENCES payment (id),
  FOREIGN KEY (transfer_id) REFERENCES transfer (id),
  FOREIGN KEY (checkout_id) REFERENCES checkout (id),
  FOREIGN KEY (user_id) REFERENCES users (id),
  FOREIGN KEY (afected_id) REFERENCES users (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ==================================================
-- Table transaction_credit_card
-- ==================================================
CREATE TABLE IF NOT EXISTS transaction_credit_card
(
  id 				int(11) 		NOT NULL,
   
  payment_method_id	int(11) 		NOT NULL,   

  token 			varchar(100) 	DEFAULT NULL,
  token_agro		varchar(10) 	DEFAULT NULL,
  
  owner_name 		varchar(150) 	NOT NULL,

  bin				int(6)	 		DEFAULT NULL,
  last_four_digits	int(4)			DEFAULT NULL,
  expiration_month 	int(2)	 		DEFAULT NULL,
  expiration_year 	int(4)	 		DEFAULT NULL,

  amount 			double 			NOT NULL,
  amount_to_conciliate	double 		NOT NULL DEFAULT 0,
  amount_conciliated 	double 		NOT NULL DEFAULT 0,

  process_date 		datetime 		NOT NULL DEFAULT CURRENT_TIMESTAMP,
  aut 				varchar(50) 	NULL,
  cupon 			varchar(50) 	NULL,
  observations 		longtext 		NULL,

  status 			char(15) 		NOT NULL DEFAULT 'REVIEW' CHECK( status in ('REVIEW', 'ACCEPTED', 'REJECTED', 'CONCILIATED', 'CANCELED')),

  conciliation_date datetime	 	NULL,
  transaction_code	varchar(50)		NULL,

  PRIMARY KEY (id),
  FOREIGN KEY (id) REFERENCES transaction_values (id),
  FOREIGN KEY (payment_method_id) REFERENCES payment_method (id)

) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- ==================================================
-- Table wallet
-- ==================================================
CREATE TABLE IF NOT EXISTS wallet
(
  id 				int(11) 		NOT NULL AUTO_INCREMENT,

  user_id 			int(11) 		NOT NULL,
  payment_type		char(15)		NOT NULL,
  currency			char(4)			NOT NULL,

  amount_inprocess	double 			NOT NULL DEFAULT 0,
  amount_available 	double 			NOT NULL DEFAULT 0,
  amount_canceled	double 			NOT NULL DEFAULT 0,

  PRIMARY KEY (id),
  UNIQUE KEY (user_id, payment_type, currency),
  FOREIGN KEY (user_id) REFERENCES users (id),
  FOREIGN KEY (payment_type, currency) REFERENCES payment_types (payment_type, currency)
 
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- ==================================================
-- Table wallet_balance
-- ==================================================
CREATE TABLE IF NOT EXISTS wallet_balance
(
  id 				int(11) 		NOT NULL AUTO_INCREMENT,

  wallet_id			int(11)			NOT NULL,
  
  user_id 			int(11) 		NOT NULL,
  payment_type		char(15)		NOT NULL,
  currency			char(4)			NOT NULL,
  
  date 				datetime	 	NOT NULL,
  description 		longtext 		NOT NULL,
  cred_deb			char(1)			NOT NULL,

  value_id 			int(11) 		NULL,

  amount 			double 			NOT NULL,
  status 			char(15) 		NOT NULL DEFAULT 'IN_PROCESS' CHECK( status in ('IN_PROCESS', 'AVAILABLE', 'CANCELED')),


  PRIMARY KEY (id),
  KEY (user_id),
  KEY (value_id),
  FOREIGN KEY (wallet_id) REFERENCES wallet(id),
  FOREIGN KEY (value_id) REFERENCES transaction_values (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


-- ==================================================
-- VIEWS
-- ==================================================
DROP VIEW if exists transactions;

CREATE VIEW transactions 
AS
select * from 
(select po.id, 
	   'PAYMENT_ORDER' as operation_type,
	   'C' as deb_cred,
	   po.uuid as uuid,
       po.code as code,
       po.beneficiary_id as user_id,
	     b.legal_name as user,
       po.date,
       po.description,
       po.invoice_number as ticket,
       po.status,
       po.payer_id as target_id,
       pa.legal_name as target,
       po.amount
  from payment_order po
	join users b
	  on b.id = po.beneficiary_id
	left join users pa
	  on pa.id = po.payer_id
UNION
select po.id,
       'PAYMENT_ORDER' as operation_type,
	   'D' as deb_cred,
	   po.uuid as uuid,
       po.code as code,
       po.payer_id as user_id,
	     p.legal_name as user,
       po.date,
       po.description,
       po.invoice_number as ticket,
       po.status,
       po.beneficiary_id as target_id,
       b.legal_name as target,
       po.amount
  from payment_order po
	join users b
	  on b.id = po.beneficiary_id
	left join users p
	  on p.id = po.payer_id
UNION
select t.id,
       'TRANSFER' as operation_type,
	   'D' as deb_cred,
	   t.uuid as uuid,
       t.code as code,
       t.sender_id as user_id,
       s.legal_name as user,
       t.date,
       t.description,
       '' as ticket,
       t.status,
       t.receiver_id as target_id,
	     d.legal_name as target,
       t.amount
  from transfer t
	join users s
	  on s.id = t.sender_id
	join users d
	  on d.id = t.receiver_id
UNION
select t.id,
       'TRANSFER' as operation_type,
	   'C' as deb_cred,
	   t.uuid as uuid,
       t.code as code,
       t.receiver_id as user_id,
       '' as ticket,
	     d.legal_name as user,
       t.date,
       t.description,
       t.status,
       t.sender_id as afected_id,
       s.legal_name as afected,
       t.amount
  from transfer t
	join users s
	  on s.id = t.sender_id
	join users d
	  on d.id = t.receiver_id
UNION
select t.id,
       'CHECKOUT' as operation_type,
	   'D' as deb_cred,
	   t.uuid as uuid,
       t.code as code,
       t.user_id,
       s.legal_name as user,
       t.date,
       "Retiro Dinero" as description,
       '' as ticket,
       t.status,
       c.id as target_id,
	   Concat("cbu: ", ifnull(c.cbu,""), ", banco:" , ifnull(b.name,"")) as target,
       t.amount
  from checkout t
	join users s
	  on s.id = t.user_id
	join user_bank_account c
      on c.id = t.user_bank_account_id
	join bank b
      on b.id = c.bank_id
) as t
order by date desc;


-- ==================================================
-- Triggers
-- ==================================================
delimiter //

CREATE TRIGGER payment_insert AFTER INSERT ON payment
	FOR EACH ROW
    BEGIN
		if NEW.status = "REVIEW" AND NEW.status != OLD.status THEN
			# Update payment order
			UPDATE payment_order po
				join payment p
				  on po.id = p.payment_order_id
			   SET po.status = 'REVIEW'
			 WHERE p.id = NEW.id;
        END IF;

	END; //


CREATE TRIGGER transaction_values_wallet_insert AFTER INSERT ON transaction_values
	FOR EACH ROW
    BEGIN
		if NEW.status = "ACCEPTED" THEN
			# Insert record on wallet in case it doesn't exist
			INSERT INTO wallet (user_id, payment_type, currency)
			SELECT NEW.user_id, vt.payment_type, vt.currency
			  FROM payment_types vt
			 WHERE NOT EXISTS (select * 
								 from wallet w
								where w.user_id = NEW.user_id
								  and w.payment_type = vt.payment_type
								  and w.currency = vt.currency);
								  
			INSERT INTO wallet (user_id, payment_type, currency)
			SELECT NEW.afected_id, vt.payment_type, vt.currency
			  FROM payment_types vt
			 WHERE NOT EXISTS (select * 
								 from wallet w
								where w.user_id = NEW.afected_id
								  and w.payment_type = vt.payment_type
								  and w.currency = vt.currency);
								  
			SET @OPERATION_DESC = CASE
									WHEN NEW.operation_type = 'PAYMENT'
										THEN 'Pago'
									WHEN NEW.operation_type = 'TRANSFER'
										THEN 'Transferencia'
									WHEN NEW.operation_type = 'CHECKOUT'
										THEN 'Retiro'
									ELSE ''
								 END;
			SET @payment_type = 'VIRTUAL_CASH';

			IF NEW.payment_type = 'CREDIT_CARD' AND NEW.operation_type IN ('PAYMENT', 'TRANSFER') THEN
				SELECT id
				  INTO @WALLET_ID
				  FROM wallet
				 WHERE user_id = NEW.user_id
                   AND payment_type = @payment_type
                   AND currency = NEW.currency;
				INSERT INTO wallet_balance (wallet_id, user_id, payment_type, currency, date, description, cred_deb, status, amount, value_id)
				VALUES (@WALLET_ID, NEW.user_id, @payment_type, NEW.currency, NEW.process_date, "Ingreso dinero", "C", 'IN_PROCESS', NEW.amount, NEW.id);
				INSERT INTO wallet_balance (wallet_id, user_id, payment_type, currency, date, description, cred_deb, status, amount, value_id)
				VALUES (@WALLET_ID, NEW.user_id, @payment_type, NEW.currency, NEW.process_date, CONCAT(@OPERATION_DESC, " a"), "D", 'IN_PROCESS', NEW.amount, NEW.id);

				SELECT id
				  INTO @WALLET_ID
				  FROM wallet
				 WHERE user_id = NEW.afected_id
                   AND payment_type = @payment_type
                   AND currency = NEW.currency;
				INSERT INTO wallet_balance (wallet_id, user_id, payment_type, currency, date, description, cred_deb, status, amount, value_id)
				VALUES (@WALLET_ID, NEW.afected_id, @payment_type, NEW.currency, NEW.process_date, CONCAT(@OPERATION_DESC, " de"), "C", 'IN_PROCESS', NEW.amount, NEW.id);
				
			END IF;
			
			IF NEW.payment_type = 'VIRTUAL_CASH' AND NEW.operation_type IN ('PAYMENT', 'TRANSFER') THEN
				SELECT id
				  INTO @WALLET_ID
				  FROM wallet
				 WHERE user_id = NEW.user_id
                   AND payment_type = @payment_type
                   AND currency = NEW.currency;

				INSERT INTO wallet_balance (wallet_id, user_id, payment_type, currency, date, description, cred_deb, status, amount, value_id)
				VALUES (@WALLET_ID, NEW.user_id, @payment_type, NEW.currency, NEW.process_date, CONCAT(@OPERATION_DESC, " a"), "D", 'AVAILABLE', NEW.amount, NEW.id);
				
				SELECT id
				  INTO @WALLET_ID
				  FROM wallet
				 WHERE user_id = NEW.afected_id
                   AND payment_type = @payment_type
                   AND currency = NEW.currency;
				INSERT INTO wallet_balance (wallet_id, user_id, payment_type, currency, date, description, cred_deb, status, amount, value_id)
				VALUES (@WALLET_ID, NEW.afected_id, @payment_type, NEW.currency, NEW.process_date, CONCAT(@OPERATION_DESC, " de"), "C", 'AVAILABLE', NEW.amount, NEW.id);
			END IF;

			IF NEW.payment_type = 'VIRTUAL_CASH' AND NEW.operation_type IN ('CHECKOUT') THEN
				SELECT id
				  INTO @WALLET_ID
				  FROM wallet
				 WHERE user_id = NEW.user_id
                   AND payment_type = @payment_type
                   AND currency = NEW.currency;
				INSERT INTO wallet_balance (wallet_id, user_id, payment_type, currency, date, description, cred_deb, status, amount, value_id)
				VALUES (@WALLET_ID, NEW.user_id, @payment_type, NEW.currency, NEW.process_date, @OPERATION_DESC, "D", 'AVAILABLE', NEW.amount, NEW.id);
			END IF;

			IF NEW.operation_type = 'FEE' THEN
				SELECT id
				  INTO @WALLET_ID
				  FROM wallet
				 WHERE user_id = NEW.user_id
                   AND payment_type = @payment_type
                   AND currency = NEW.currency;
				# Insert record on wallet in case it doesn't exist
				INSERT INTO wallet_balance (wallet_id, user_id, payment_type, currency, date, description, cred_deb, status, amount, value_id)
				VALUES (@WALLET_ID, NEW.user_id, @payment_type, NEW.currency, NEW.process_date, "Comisiones", "D", 'IN_PROCESS', NEW.amount, NEW.id);
			END IF;
        END IF;
	END; //


CREATE TRIGGER transaction_values_wallet_updated AFTER UPDATE ON transaction_values
	FOR EACH ROW
    BEGIN
		if NEW.status = "ACCEPTED" AND NEW.status != OLD.status THEN
			# Insert record on wallet in case it doesn't exist
			INSERT INTO wallet (user_id, payment_type, currency)
			SELECT NEW.user_id, vt.payment_type, vt.currency
			  FROM payment_types vt
			 WHERE NOT EXISTS (select * 
								 from wallet w
								where w.user_id = NEW.user_id
								  and w.payment_type = vt.payment_type
								  and w.currency = vt.currency);
								  
			INSERT INTO wallet (user_id, payment_type, currency)
			SELECT NEW.afected_id, vt.payment_type, vt.currency
			  FROM payment_types vt
			 WHERE NOT EXISTS (select * 
								 from wallet w
								where w.user_id = NEW.afected_id
								  and w.payment_type = vt.payment_type
								  and w.currency = vt.currency);
								  
			SET @OPERATION_DESC = CASE
									WHEN NEW.operation_type = 'PAYMENT'
										THEN 'Pago'
									WHEN NEW.operation_type = 'TRANSFER'
										THEN 'Transferencia'
									WHEN NEW.operation_type = 'CHECKOUT'
										THEN 'Retiro'
									ELSE ''
								 END;
			SET @payment_type = 'VIRTUAL_CASH';

			IF NEW.payment_type = 'CREDIT_CARD' AND NEW.operation_type IN ('PAYMENT', 'TRANSFER') THEN
				SELECT id
				  INTO @WALLET_ID
				  FROM wallet
				 WHERE user_id = NEW.user_id
                   AND payment_type = @payment_type
                   AND currency = NEW.currency;
				INSERT INTO wallet_balance (wallet_id, user_id, payment_type, currency, date, description, cred_deb, status, amount, value_id)
				VALUES (@WALLET_ID, NEW.user_id, @payment_type, NEW.currency, NEW.process_date, "Ingreso dinero", "C", 'IN_PROCESS', NEW.amount, NEW.id);
				INSERT INTO wallet_balance (wallet_id, user_id, payment_type, currency, date, description, cred_deb, status, amount, value_id)
				VALUES (@WALLET_ID, NEW.user_id, @payment_type, NEW.currency, NEW.process_date, CONCAT(@OPERATION_DESC, " a"), "D", 'IN_PROCESS', NEW.amount, NEW.id);

				SELECT id
				  INTO @WALLET_ID
				  FROM wallet
				 WHERE user_id = NEW.afected_id
                   AND payment_type = @payment_type
                   AND currency = NEW.currency;
				INSERT INTO wallet_balance (wallet_id, user_id, payment_type, currency, date, description, cred_deb, status, amount, value_id)
				VALUES (@WALLET_ID, NEW.afected_id, @payment_type, NEW.currency, NEW.process_date, CONCAT(@OPERATION_DESC, " de"), "C", 'IN_PROCESS', NEW.amount, NEW.id);
				
			END IF;
			
			IF NEW.payment_type = 'VIRTUAL_CASH' AND NEW.operation_type IN ('PAYMENT', 'TRANSFER') THEN
				SELECT id
				  INTO @WALLET_ID
				  FROM wallet
				 WHERE user_id = NEW.user_id
                   AND payment_type = @payment_type
                   AND currency = NEW.currency;

				INSERT INTO wallet_balance (wallet_id, user_id, payment_type, currency, date, description, cred_deb, status, amount, value_id)
				VALUES (@WALLET_ID, NEW.user_id, @payment_type, NEW.currency, NEW.process_date, CONCAT(@OPERATION_DESC, " a"), "D", 'AVAILABLE', NEW.amount, NEW.id);
				
				SELECT id
				  INTO @WALLET_ID
				  FROM wallet
				 WHERE user_id = NEW.afected_id
                   AND payment_type = @payment_type
                   AND currency = NEW.currency;
				INSERT INTO wallet_balance (wallet_id, user_id, payment_type, currency, date, description, cred_deb, status, amount, value_id)
				VALUES (@WALLET_ID, NEW.afected_id, @payment_type, NEW.currency, NEW.process_date, CONCAT(@OPERATION_DESC, " de"), "C", 'AVAILABLE', NEW.amount, NEW.id);
			END IF;

			IF NEW.payment_type = 'VIRTUAL_CASH' AND NEW.operation_type IN ('CHECKOUT') THEN
				SELECT id
				  INTO @WALLET_ID
				  FROM wallet
				 WHERE user_id = NEW.user_id
                   AND payment_type = @payment_type
                   AND currency = NEW.currency;
				INSERT INTO wallet_balance (wallet_id, user_id, payment_type, currency, date, description, cred_deb, status, amount, value_id)
				VALUES (@WALLET_ID, NEW.user_id, @payment_type, NEW.currency, NEW.process_date, @OPERATION_DESC, "D", 'AVAILABLE', NEW.amount, NEW.id);
			END IF;

			IF NEW.operation_type = 'FEE' THEN
				SELECT id
				  INTO @WALLET_ID
				  FROM wallet
				 WHERE user_id = NEW.user_id
                   AND payment_type = @payment_type
                   AND currency = NEW.currency;
				# Insert record on wallet in case it doesn't exist
				INSERT INTO wallet_balance (wallet_id, user_id, payment_type, currency, date, description, cred_deb, status, amount, value_id)
				VALUES (@WALLET_ID, NEW.user_id, @payment_type, NEW.currency, NEW.process_date, "Comisiones", "D", 'IN_PROCESS', NEW.amount, NEW.id);
			END IF;
        END IF;
		IF NEW.status != "ACCEPTED" AND OLD.status = "ACCEPTED" THEN
			SIGNAL SQLSTATE '05000' SET MESSAGE_TEXT = 'Error: El estado no puede ser cambiado luego que fue ACCEPTED!';
        END IF;

	END; //


CREATE TRIGGER wallet_balance_wallet_validate_update BEFORE UPDATE ON wallet_balance
	FOR EACH ROW
    BEGIN
		# validate amount in order to prevent to be updated
        if NEW.amount != OLD.amount then
			SIGNAL SQLSTATE '05000' SET MESSAGE_TEXT = 'Error: la cantidad no puede ser modificada!';
		end if;
	
	END;//

CREATE TRIGGER wallet_balance_wallet_insert AFTER INSERT ON wallet_balance
	FOR EACH ROW
    BEGIN
		# Update wallet 
        UPDATE wallet
           SET amount_inprocess = CASE
									WHEN NEW.status = 'IN_PROCESS' AND NEW.cred_deb = 'C' THEN amount_inprocess + NEW.amount
									WHEN NEW.status = 'IN_PROCESS' AND NEW.cred_deb = 'D' THEN amount_inprocess - NEW.amount
									ELSE amount_inprocess
								  END,
			   amount_available = CASE
									WHEN NEW.status = 'AVAILABLE' AND NEW.cred_deb = 'C' THEN amount_available + NEW.amount
									WHEN NEW.status = 'AVAILABLE' AND NEW.cred_deb = 'D' THEN amount_available - NEW.amount
									ELSE amount_available
								  END,
			   amount_canceled = CASE
									WHEN NEW.status = 'CANCELED' AND NEW.cred_deb = 'C' THEN amount_canceled + NEW.amount
									WHEN NEW.status = 'CANCELED' AND NEW.cred_deb = 'D' THEN amount_canceled - NEW.amount
									ELSE amount_canceled
								  END
		 WHERE wallet.id = NEW.wallet_id;
	
	END;//

CREATE TRIGGER wallet_balance_wallet_update AFTER UPDATE ON wallet_balance
	FOR EACH ROW
    BEGIN
		# Update wallet 
        UPDATE wallet
           SET amount_inprocess = CASE
									WHEN OLD.status = 'IN_PROCESS' AND NEW.status IN ('AVAILABLE', 'CANCELED') AND NEW.cred_deb = 'C' THEN amount_inprocess - NEW.amount
									WHEN OLD.status = 'IN_PROCESS' AND NEW.status IN ('AVAILABLE', 'CANCELED') AND NEW.cred_deb = 'D' THEN amount_inprocess + NEW.amount
									WHEN NEW.status = 'IN_PROCESS' AND OLD.status IN ('AVAILABLE', 'CANCELED') AND NEW.cred_deb = 'C' THEN amount_inprocess + NEW.amount
									WHEN NEW.status = 'IN_PROCESS' AND OLD.status IN ('AVAILABLE', 'CANCELED') AND NEW.cred_deb = 'D' THEN amount_inprocess - NEW.amount
									ELSE amount_inprocess
								  END,
			   amount_available = CASE
									WHEN NEW.status = 'AVAILABLE' AND OLD.status IN ('IN_PROCESS', 'CANCELED') AND NEW.cred_deb = 'C' THEN amount_available + NEW.amount
									WHEN NEW.status = 'AVAILABLE' AND OLD.status IN ('IN_PROCESS', 'CANCELED') AND NEW.cred_deb = 'D' THEN amount_available - NEW.amount
									WHEN OLD.status = 'AVAILABLE' AND NEW.status IN ('IN_PROCESS', 'CANCELED') AND NEW.cred_deb = 'C' THEN amount_available - NEW.amount
									WHEN OLD.status = 'AVAILABLE' AND NEW.status IN ('IN_PROCESS', 'CANCELED') AND NEW.cred_deb = 'D' THEN amount_available + NEW.amount
									ELSE amount_available
								  END,
			   amount_canceled = CASE
									WHEN NEW.status = 'CANCELED' AND OLD.status IN ('IN_PROCESS', 'AVAILABLE') AND NEW.cred_deb = 'C' THEN amount_canceled + NEW.amount
									WHEN NEW.status = 'CANCELED' AND OLD.status IN ('IN_PROCESS', 'AVAILABLE') AND NEW.cred_deb = 'D' THEN amount_canceled - NEW.amount
									WHEN OLD.status = 'CANCELED' AND NEW.status IN ('IN_PROCESS', 'AVAILABLE') AND NEW.cred_deb = 'C' THEN amount_canceled - NEW.amount
									WHEN OLD.status = 'CANCELED' AND NEW.status IN ('IN_PROCESS', 'AVAILABLE') AND NEW.cred_deb = 'D' THEN amount_canceled + NEW.amount
									ELSE amount_canceled
								  END
		 WHERE id = NEW.wallet_id;
	
	END;//

CREATE TRIGGER transaction_credit_card_update AFTER UPDATE ON transaction_credit_card
	FOR EACH ROW
    BEGIN
		IF NEW.status = 'ACCEPTED' THEN
			SELECT payment_id
			  INTO @PAYMENT_ID
			  FROM transaction_values
			 WHERE id = NEW.id;
		
			# Update payment order
			UPDATE payment_order po
				join payment p
				  on po.id = p.payment_order_id
			   SET po.when_paid = NEW.conciliation_date,
                   po.status = 'ACCEPTED'
			 WHERE p.id = @PAYMENT_ID;

			# Update payment order
			UPDATE payment p
			   SET p.status = 'ACCEPTED'
			 WHERE p.id = @PAYMENT_ID;
             
			# Update transaction_values
			UPDATE transaction_values t
			   SET t.status = 'ACCEPTED'
			 WHERE t.payment_id = @PAYMENT_ID;

		 END IF;
         
		IF NEW.status = 'REJECTED' THEN
			SELECT payment_id
			  INTO @PAYMENT_ID
			  FROM transaction_values
			 WHERE id = NEW.id;
		
			# Update payment order
			UPDATE payment_order po
				join payment p
				  on po.id = p.payment_order_id
			   SET po.when_paid = NEW.conciliation_date,
                   po.status = 'PENDING'
			 WHERE p.id = @PAYMENT_ID;

			# Update payment order
			UPDATE payment p
			   SET p.status = 'REJECTED'
			 WHERE p.id = @PAYMENT_ID;

			# Update transaction_values
			UPDATE transaction_values t
			   SET t.status = 'REJECTED'
			 WHERE t.payment_id = @PAYMENT_ID;
		END IF;
		
		IF NEW.status = 'CONCILIATED' THEN
			SELECT payment_id
			  INTO @PAYMENT_ID
			  FROM transaction_values
			 WHERE id = NEW.id;
		
			# Update transaction_credit_card
			UPDATE wallet_balance wb
				join transaction_values ov
				  on ov.id = wb.value_id
			   SET wb.status = 'AVAILABLE'
			 WHERE ov.payment_id = @PAYMENT_ID;
             
			# Update payment order
			UPDATE payment_order po
				join payment p
				  on po.id = p.payment_order_id
			   SET po.when_paid = NEW.conciliation_date,
                   po.status = 'PAID'
			 WHERE p.id = @PAYMENT_ID;

			# Update payment order
			UPDATE payment p
			   SET p.status = 'PAID'
			 WHERE p.id = @PAYMENT_ID;

		END IF;
         
	END;//

/*
use agropago;
drop TABLE wallet_balance;
drop TABLE wallet;
drop TABLE transaction_credit_card;
drop TABLE transaction_values;

payment_order
OP1 U1 U2 1000

payment 

wallet_transactions
U2	C	CreditCard	1000		P1
U2	D	Pesos		1000		P1

U1	C 	Pesos		1000		P1
U1	D	Comisiones	100			P1

U1	D	Pesos		900			T1
U3	C	Pesos		900			T1

wallet_transactions_detail

*/





/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
