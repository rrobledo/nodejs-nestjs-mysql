use agropago_demo;

drop view if exists transactions;
drop view if exists movements;
drop view if exists payment_credit_cards;
drop table if exists wallet_balance;
drop table if exists wallet;
drop table if exists transaction_credit_card;
drop table if exists transaction_values;
drop table if exists checkout;
drop table if exists transfer;
drop table if exists payment;
drop table if exists payment_order;


