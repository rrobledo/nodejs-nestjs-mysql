--
-- Dumping data for table django_content_type
--

INSERT INTO django_content_type (id, app_label, model) VALUES
(9, 'admin', 'logentry'),
(3, 'api', 'bank'),
(4, 'api', 'creditcard'),
(8, 'api', 'payment'),
(7, 'api', 'paymentorder'),
(5, 'api', 'userbankaccount'),
(6, 'api', 'usercreditcard'),
(11, 'auth', 'group'),
(10, 'auth', 'permission'),
(14, 'authtoken', 'token'),
(1, 'base', 'objectsequence'),
(12, 'contenttypes', 'contenttype'),
(2, 'security', 'user'),
(13, 'sessions', 'session');

