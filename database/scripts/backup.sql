-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: mysql
-- Tiempo de generación: 05-09-2019 a las 13:39:42
-- Versión del servidor: 5.7.25
-- Versión de PHP: 7.2.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `agropago`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `asset`
--

CREATE TABLE `asset` (
  `id` int(11) NOT NULL,
  `logo` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cc_logo` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `asset`
--

INSERT INTO `asset` (`id`, `logo`, `cc_logo`) VALUES
(1, '../assets/images/banks/nacion.png', '../assets/images/banks/nacion_card.png'),
(2, '../assets/images/banks/galicia.png', '../assets/images/banks/galicia_card.png');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `authtoken_token`
--

CREATE TABLE `authtoken_token` (
  `key` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime(6) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `authtoken_token`
--

INSERT INTO `authtoken_token` (`key`, `created`, `user_id`) VALUES
('00189cf6a61b290c07c943dc4b6d7830dc8a7cb0', '2019-05-28 11:28:18.036987', 78),
('03e136d59edab429c7b21b8fa1dfbfb44067b555', '2019-03-01 13:26:08.031601', 55),
('040d5bad7a5db54c30ddc41a3cea288a30d2d95c', '2018-01-30 17:56:00.347363', 32),
('06c582a39185fe931cf0c371facd304323630d9d', '2019-06-01 12:16:05.168131', 104),
('06d46ea79cc6dd971b5906ef12056b7bfe522445', '2018-12-18 00:34:29.985056', 48),
('07e82b5c0c49899bf6fc657e5b553483ae0ce5e3', '2019-05-28 11:39:30.221646', 79),
('08ef1cce23f213067d9f2eede47c5df3dc93105e', '2019-06-19 19:47:27.850305', 112),
('0c3fa9e7865b7baba6e909fab72fde89ea666456', '2019-04-03 19:30:07.349281', 67),
('0e4872e960db25b2b6a7ab4b2505d2762f10b4e2', '2019-05-28 11:57:27.388220', 82),
('0efa57ca5efda17d12fff2613e38b9c07473def1', '2017-12-12 15:07:04.165469', 29),
('10e25642ac36f9ce40796281681887e61cef0929', '2019-06-03 22:49:33.947837', 108),
('13b0e6fe4850fada3830b4ef7e8a792a99601df3', '2019-05-20 18:20:24.465039', 76),
('16abc5a6c88dac3131c85f0d28a78c7edb1870a6', '2019-08-27 15:22:06.418591', 125),
('1b938ce9e5d4e94c1c71ebe0205f3b332cee76c2', '2019-05-29 01:15:06.577511', 93),
('1be06bd6ff85bee357ecd65912d5d7d1f31a16f9', '2018-01-18 22:52:59.470441', 5),
('21ca14f66fa19167053b9d357d88020c029ae4c4', '2019-03-20 19:27:08.766459', 65),
('22f809db5ab3e4fc7a6aae98a689f3b18cb0b86c', '2019-05-15 20:07:15.677689', 74),
('25c34e8d7822abcb4a28fd298e0019f4a4f4ce0c', '2018-06-22 18:11:05.887253', 36),
('25c3cc67bdc35491b160153bf5f12f10db794acd', '2019-05-29 22:06:41.745181', 96),
('27bb2a0888eff68b6998ee2609ab1cf517d3d827', '2019-02-20 18:22:31.831264', 51),
('2bbae6b8f77e6cfccfbd1986aa8384b6838e7d63', '2019-06-12 12:25:10.824306', 111),
('36f9fb22ad485aafdc9623c1fa48742b33a1d280', '2018-07-31 03:54:11.172341', 39),
('37b61e8844f53de6ff72a81c4dc97cd743dbc29e', '2018-05-28 18:37:06.524767', 35),
('3a1c3587d0a2012185666d3cc7c554c574455fb6', '2018-03-14 15:25:31.322621', 13),
('3a24ca04d25b85f298605c4222dabdc7bf695d40', '2019-08-14 20:33:19.234230', 124),
('3a7bab20a203291e2017976c073851b974f43f6c', '2017-09-27 12:05:56.452203', 14),
('3ef956bc67a1fd0d6457802208f22c0b6e81c57b', '2019-05-31 15:52:51.158040', 103),
('4430b5ff3089144bab98850491b86669de242ef5', '2018-06-28 16:53:10.535986', 15),
('4af61417c5d23e3d2dc1eb25cde28c90c106b272', '2019-03-20 12:06:52.449349', 62),
('4ec1bf5a170420f55f7271141c07b2016fcb751f', '2018-10-17 13:17:56.273359', 42),
('5257d35463a0c06c8f104fcb86c0bf4805a0de19', '2019-03-20 19:22:14.678466', 64),
('52fa21c0d79809add4cb85534100f83232343fe5', '2019-02-25 19:36:12.878724', 53),
('53dfde45093215c582480ad530309de06f392725', '2019-05-28 16:53:59.970365', 88),
('56c295ac49a36ae1d1ce60c884d3c7c2315f519a', '2019-02-23 13:11:42.747443', 52),
('5c037108c72dd7c8a87977ec4deeb4343a4d0d62', '2018-04-20 22:30:35.001508', 33),
('5d9e966d1c53c9e5c064a2e11afef3033e514674', '2018-05-18 12:04:14.969056', 34),
('5f0ff6492debb2115924c3923014c942392269bb', '2019-03-01 16:58:01.201696', 56),
('60488845b638790f81c18f15f02760e22d638b1c', '2019-08-28 16:06:54.225031', 126),
('646e5c917104544fd036d2650177b0ae6dd8adac', '2019-07-10 18:06:09.605058', 116),
('64f673abe4519c835468c5c83cc5cd92da0aebd9', '2017-10-26 18:41:18.085659', 21),
('65c62a85dd4355bc287f13c8eb83310a8504254c', '2019-05-31 13:30:30.907628', 102),
('6e53bc13bbcf84bb5ae64ab30b84ea26f651dcaf', '2019-06-04 12:25:56.228877', 107),
('7a9316057187a07708979e3323b0cfff6b8c0360', '2017-11-02 13:11:18.921289', 23),
('84ca412d13b0a990b2f4661e2935f8839fe245e3', '2018-10-09 04:03:20.684868', 46),
('87595f5d0b6a87f60945bf4678632188797f67e0', '2019-08-13 17:08:30.503187', 122),
('91206d5804ab9c9b1a5de7e331e50b7b77fbd947', '2019-05-29 11:31:04.063545', 97),
('92f84d2a3225fb91f654337d2820ad1b4dd25a31', '2019-03-20 12:47:41.463723', 63),
('9457f351d5e5fe4644bee64a554326cc6f7f4884', '2018-07-19 15:52:55.767930', 38),
('9a65e7dadc3982c97a6c0dff7d22021672380ecb', '2019-05-28 14:28:47.235272', 85),
('9b59f5a8035328c1b9d721877ab4b1cf3bc20ac4', '2019-06-01 23:14:42.865308', 105),
('9d007a9fd33ddded082d60b97c57452cafbf233d', '2019-05-29 02:54:56.405626', 94),
('a039a08e6c23c15484d4d67518cba6c4bb2148e7', '2019-05-28 20:22:32.878513', 91),
('a2d09ed0cbf57d01bb7af3e6d8c34577d30041c9', '2018-09-17 20:21:38.993754', 1),
('a874871aa0359fc4a463a4722e5cc427ea598e79', '2019-05-30 00:49:31.764095', 99),
('ab37df993aefe82f8b51d87979ba7cd809fa0547', '2019-05-30 11:29:01.695590', 101),
('ad08299700a6f048ba869a5bba02ebaebb5927a3', '2019-07-18 21:52:46.507372', 118),
('ae3c23766ccccb769d002ce1787075ab66d4c3ea', '2019-08-19 18:37:21.204722', 50),
('b6faee199f108e1c0a85cc5129d3bdd989d55ada', '2019-05-28 15:46:06.247624', 87),
('c8886dd6402545e0d22584ee243c3cfb87cd340d', '2019-07-22 21:01:32.134206', 120),
('c931a8546d3fa305e8080be7380feb50c63b9f3c', '2019-08-14 15:38:05.122115', 123),
('c940655f8ed02db55fa786039f46278770c7af91', '2017-10-31 13:11:17.186653', 22),
('cb2668d8a9a3e591a477d342b8c416ae696c143a', '2019-07-03 13:49:02.891816', 115),
('cbb2ec1081666d51db93b0a514d4dab302f31ff4', '2019-05-28 17:24:03.381035', 89),
('cd05d8e83afe95fe10325e8fc923aa4da1029f49', '2019-05-28 11:53:52.969205', 80),
('d52213a31d4e634ba436facd66ad336622f57449', '2017-11-16 15:35:44.791649', 27),
('da58ce7c6116027752894f9660e7c824797e6b8d', '2019-05-28 12:52:44.236877', 83),
('dce6b2885f75aeede60f7cb38c5dde15bd772f4e', '2019-05-28 17:29:24.368558', 90),
('de3bb5bc41de53add6cf548d21533fa4c8aa896a', '2019-05-28 11:04:09.227637', 77),
('e47844b351cbde78b8ae441c4caa7a29720eed35', '2018-09-24 22:31:29.063796', 43),
('e536ea475a3a620d45560ea0e1b124e4fbdbadc6', '2019-06-03 14:33:20.706600', 106),
('e6fab4a5b8768865254ce66adb2ceb977d8a26f9', '2018-10-29 13:19:28.431057', 37),
('e73c711587787f00fefa18ad28e675343f6b87f4', '2019-05-15 17:18:42.547011', 73),
('ed6ee6b85ef3394b47ad34bb74f9bfc8dac9ff03', '2019-03-05 01:31:44.112921', 58),
('eee23db79ac7f49d3d5a947c77f1ca95dfffc064', '2019-05-28 13:54:14.589972', 84),
('f192f9b064c41a35c5dfe36a1a6a6404f914fb1b', '2019-02-26 13:36:54.210004', 54),
('f24fa2b4d7f2d916a0976e25ffa7b7b96c47846e', '2018-09-25 20:28:40.020940', 40),
('f5f59cfa74fe0c0700ea8357a5589d2ae6e221e2', '2017-12-26 21:38:55.292165', 28),
('f77d4725434687e2a382db58fea650cd4c743d32', '2018-10-30 23:44:57.836234', 47),
('fa23b85855672c0d6a185233623cb3e38d958e58', '2019-03-01 17:56:16.550920', 57),
('fcfac29df0766c348a6fd7174a29f133a851dfaa', '2017-10-12 19:12:09.563152', 20);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_group`
--

CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL,
  `name` varchar(80) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_group_permissions`
--

CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `auth_permission`
--

CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `auth_permission`
--

INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES
(1, 'Can add object sequence', 1, 'add_objectsequence'),
(2, 'Can change object sequence', 1, 'change_objectsequence'),
(3, 'Can delete object sequence', 1, 'delete_objectsequence'),
(4, 'Can add user', 2, 'add_user'),
(5, 'Can change user', 2, 'change_user'),
(6, 'Can delete user', 2, 'delete_user'),
(7, 'Can add bank', 3, 'add_bank'),
(8, 'Can change bank', 3, 'change_bank'),
(9, 'Can delete bank', 3, 'delete_bank'),
(10, 'Can add credit card', 4, 'add_creditcard'),
(11, 'Can change credit card', 4, 'change_creditcard'),
(12, 'Can delete credit card', 4, 'delete_creditcard'),
(13, 'Can add user bank account', 5, 'add_userbankaccount'),
(14, 'Can change user bank account', 5, 'change_userbankaccount'),
(15, 'Can delete user bank account', 5, 'delete_userbankaccount'),
(16, 'Can add user credit card', 6, 'add_usercreditcard'),
(17, 'Can change user credit card', 6, 'change_usercreditcard'),
(18, 'Can delete user credit card', 6, 'delete_usercreditcard'),
(19, 'Can add payment order', 7, 'add_paymentorder'),
(20, 'Can change payment order', 7, 'change_paymentorder'),
(21, 'Can delete payment order', 7, 'delete_paymentorder'),
(22, 'Can add payment', 8, 'add_payment'),
(23, 'Can change payment', 8, 'change_payment'),
(24, 'Can delete payment', 8, 'delete_payment'),
(25, 'Can add log entry', 9, 'add_logentry'),
(26, 'Can change log entry', 9, 'change_logentry'),
(27, 'Can delete log entry', 9, 'delete_logentry'),
(28, 'Can add permission', 10, 'add_permission'),
(29, 'Can change permission', 10, 'change_permission'),
(30, 'Can delete permission', 10, 'delete_permission'),
(31, 'Can add group', 11, 'add_group'),
(32, 'Can change group', 11, 'change_group'),
(33, 'Can delete group', 11, 'delete_group'),
(34, 'Can add content type', 12, 'add_contenttype'),
(35, 'Can change content type', 12, 'change_contenttype'),
(36, 'Can delete content type', 12, 'delete_contenttype'),
(37, 'Can add session', 13, 'add_session'),
(38, 'Can change session', 13, 'change_session'),
(39, 'Can delete session', 13, 'delete_session'),
(40, 'Can add Token', 14, 'add_token'),
(41, 'Can change Token', 14, 'change_token'),
(42, 'Can delete Token', 14, 'delete_token');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bank`
--

CREATE TABLE `bank` (
  `id` int(11) NOT NULL,
  `name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `initials` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `when_created` datetime(6) NOT NULL,
  `who_created_id` int(11) DEFAULT NULL,
  `when_last_edit` datetime(6) NOT NULL,
  `who_last_edit_id` int(11) DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  `when_deleted` datetime(6) DEFAULT NULL,
  `who_deleted_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `bank`
--

INSERT INTO `bank` (`id`, `name`, `initials`, `when_created`, `who_created_id`, `when_last_edit`, `who_last_edit_id`, `is_deleted`, `when_deleted`, `who_deleted_id`) VALUES
(1, 'Banco Nación', 'BNA', '2017-05-15 23:52:00.000000', 1, '2017-05-15 23:52:00.000000', 1, 0, NULL, NULL),
(2, 'Banco Galicia', 'BG', '2017-05-15 23:56:55.000000', 1, '2017-05-15 23:56:55.000000', 1, 0, NULL, NULL),
(3, 'Santander Río', 'SAN', '2017-05-15 23:56:55.000000', 1, '2017-05-15 23:56:55.000000', 1, 0, NULL, NULL),
(4, 'Macro', 'BMA', '2017-05-15 23:56:55.000000', 1, '2017-05-15 23:56:55.000000', 1, 1, '2018-04-30 16:33:27.000000', 1),
(5, 'BBVA Francés', 'BBVA', '2017-05-15 23:56:55.000000', 1, '2017-05-15 23:56:55.000000', 1, 0, NULL, NULL),
(6, 'ICBC', 'ICBC', '2017-05-15 23:56:55.000000', 1, '2017-05-15 23:56:55.000000', 1, 0, NULL, NULL),
(7, 'HSBC', 'HSBC', '2017-12-26 22:04:45.000000', 1, '2017-12-26 22:04:45.000000', 1, 0, NULL, NULL),
(8, 'Banco de Córdoba', 'BCOR', '2018-04-30 16:32:51.000000', 1, '2018-04-30 16:32:51.000000', 1, 0, NULL, NULL),
(9, 'Credicoop', 'CREDICOOP', '2018-04-30 16:32:51.000000', 1, '2018-04-30 16:32:51.000000', 1, 0, NULL, NULL),
(10, 'Banco de Entre Ríos', 'BER', '2018-04-30 16:32:51.000000', 1, '2018-04-30 16:32:51.000000', 1, 0, NULL, NULL),
(11, 'Patagonia', 'BPAT', '2018-04-30 16:32:51.000000', 1, '2018-04-30 16:32:51.000000', 1, 0, NULL, NULL),
(12, 'Banco de Santa Fe', 'BSF', '2018-04-30 16:32:51.000000', 1, '2018-04-30 16:32:51.000000', 1, 0, NULL, NULL),
(13, 'Banco de La Pampa', 'BLP', '2018-04-30 16:32:51.000000', 1, '2018-04-30 16:32:51.000000', 1, 0, NULL, NULL),
(14, 'Banco de Formosa', 'BF', '2018-04-30 16:32:51.000000', 1, '2018-04-30 16:32:51.000000', 1, 0, NULL, NULL),
(15, 'Banco de Corrientes', 'BC', '2018-04-30 16:32:51.000000', 1, '2018-04-30 16:32:51.000000', 1, 0, NULL, NULL),
(16, 'Banco Ciudad', 'CIUDAD', '2018-04-30 16:32:51.000000', 1, '2018-04-30 16:32:51.000000', 1, 0, NULL, NULL),
(17, 'Supervielle', 'SV', '2018-04-30 16:32:51.000000', 1, '2018-04-30 16:32:51.000000', 1, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `base_objectsequence`
--

CREATE TABLE `base_objectsequence` (
  `id` int(11) NOT NULL,
  `subclass` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `value` int(10) UNSIGNED NOT NULL,
  `content_type_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `base_objectsequence`
--

INSERT INTO `base_objectsequence` (`id`, `subclass`, `value`, `content_type_id`) VALUES
(1, '17', 12, 7),
(2, '17', 6, 8),
(3, '18', 1, 7),
(4, '19', 19, 7),
(5, '19', 10, 8),
(6, '19', 2, 15);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `checkout`
--

CREATE TABLE `checkout` (
  `id` int(11) NOT NULL,
  `uuid` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `date` datetime(6) NOT NULL,
  `status` char(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'PENDING',
  `user_id` int(11) NOT NULL,
  `user_bank_account_id` int(11) DEFAULT NULL,
  `amount` double NOT NULL,
  `amount_to_transfer` double DEFAULT NULL,
  `feeds` json DEFAULT NULL,
  `validation_code` int(6) NOT NULL,
  `transfer_date` datetime(6) DEFAULT NULL,
  `transaction_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `when_created` datetime(6) NOT NULL,
  `who_created_id` int(11) DEFAULT NULL,
  `when_last_edit` datetime(6) NOT NULL,
  `who_last_edit_id` int(11) DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  `when_deleted` datetime(6) DEFAULT NULL,
  `who_deleted_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `checkout`
--

INSERT INTO `checkout` (`id`, `uuid`, `code`, `date`, `status`, `user_id`, `user_bank_account_id`, `amount`, `amount_to_transfer`, `feeds`, `validation_code`, `transfer_date`, `transaction_code`, `when_created`, `who_created_id`, `when_last_edit`, `who_last_edit_id`, `is_deleted`, `when_deleted`, `who_deleted_id`) VALUES
(1, '8465a05e4f5640768bd19152fa84a82f', 'CK-19-000002', '2019-09-05 13:28:57.458753', 'TRANSFERRED', 59, 3, 2000, 2000, '{\"cft\": 0, \"arancel\": 0, \"ivaArancel\": 0, \"ivaBankRate\": 0, \"totalAmount\": 2000, \"apCommission\": 0, \"arancelTotal\": 0, \"IIBBRetention\": 0, \"earningRetetion\": 0, \"ivaAPCommission\": 0, \"ivaArancelTotal\": 0, \"ivaBankRateTotal\": 0, \"apCommissionTotal\": 0, \"IIBBRetentionTotal\": 0, \"earningRetetionTotal\": 0, \"ivaAPCommissionTotal\": 0, \"proporcionalBankrate\": 0, \"proporcionalBankrateTotal\": 0}', 9587, '2019-09-05 13:28:57.459156', '', '2019-09-05 13:28:57.457911', 59, '2019-09-05 13:28:57.458313', 59, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `django_admin_log`
--

CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext COLLATE utf8_unicode_ci,
  `object_repr` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `action_flag` smallint(5) UNSIGNED NOT NULL,
  `change_message` longtext COLLATE utf8_unicode_ci NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `django_content_type`
--

CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL,
  `app_label` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `model` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `django_content_type`
--

INSERT INTO `django_content_type` (`id`, `app_label`, `model`) VALUES
(9, 'admin', 'logentry'),
(3, 'api', 'bank'),
(15, 'api', 'checkout'),
(4, 'api', 'creditcard'),
(8, 'api', 'payment'),
(7, 'api', 'paymentorder'),
(5, 'api', 'userbankaccount'),
(6, 'api', 'usercreditcard'),
(11, 'auth', 'group'),
(10, 'auth', 'permission'),
(14, 'authtoken', 'token'),
(1, 'base', 'objectsequence'),
(12, 'contenttypes', 'contenttype'),
(2, 'security', 'user'),
(13, 'sessions', 'session');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `django_migrations`
--

CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL,
  `app` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `applied` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `django_migrations`
--

INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES
(1, 'contenttypes', '0001_initial', '2017-06-14 21:41:03.917994'),
(2, 'contenttypes', '0002_remove_content_type_name', '2017-06-14 21:41:03.992673'),
(3, 'auth', '0001_initial', '2017-06-14 21:41:04.134140'),
(4, 'auth', '0002_alter_permission_name_max_length', '2017-06-14 21:41:04.174615'),
(5, 'auth', '0003_alter_user_email_max_length', '2017-06-14 21:41:04.200282'),
(6, 'auth', '0004_alter_user_username_opts', '2017-06-14 21:41:04.221889'),
(7, 'auth', '0005_alter_user_last_login_null', '2017-06-14 21:41:04.241786'),
(8, 'auth', '0006_require_contenttypes_0002', '2017-06-14 21:41:04.246344'),
(9, 'security', '0001_initial', '2017-06-14 21:41:04.457637'),
(10, 'admin', '0001_initial', '2017-06-14 21:41:04.544040'),
(11, 'api', '0001_initial', '2017-06-14 21:41:04.647371'),
(12, 'api', '0002_auto_20161002_1645', '2017-06-14 21:41:04.707968'),
(13, 'api', '0003_auto_20161002_1652', '2017-06-14 21:41:05.235570'),
(14, 'api', '0004_userbankaccount', '2017-06-14 21:41:05.455811'),
(15, 'api', '0005_auto_20161002_2035', '2017-06-14 21:41:05.803444'),
(16, 'api', '0006_auto_20161002_2336', '2017-06-14 21:41:06.321859'),
(17, 'api', '0007_auto_20161004_2210', '2017-06-14 21:41:11.480451'),
(18, 'api', '0008_auto_20161010_0131', '2017-06-14 21:41:11.734728'),
(19, 'api', '0009_remove_userbankaccount_is_commerce', '2017-06-14 21:41:11.964218'),
(20, 'api', '0010_auto_20161029_1721', '2017-06-14 21:41:12.360634'),
(21, 'api', '0011_auto_20161029_1754', '2017-06-14 21:41:12.455055'),
(22, 'api', '0012_auto_20161029_1754', '2017-06-14 21:41:12.563823'),
(23, 'api', '0013_auto_20170212_1628', '2017-06-14 21:41:13.257409'),
(24, 'api', '0014_paymentorder_uuid', '2017-06-14 21:41:13.376491'),
(25, 'api', '0015_paymentorder_description', '2017-06-14 21:41:13.490074'),
(26, 'api', '0016_auto_20170228_2054', '2017-06-14 21:41:13.701917'),
(27, 'api', '0017_auto_20170301_0010', '2017-06-14 21:41:13.934165'),
(28, 'api', '0018_paymentorder_invoice_number', '2017-06-14 21:41:14.044017'),
(29, 'authtoken', '0001_initial', '2017-06-14 21:41:14.121460'),
(30, 'authtoken', '0002_auto_20160226_1747', '2017-06-14 21:41:14.416890'),
(31, 'base', '0001_initial', '2017-06-14 21:41:14.536509'),
(32, 'security', '0002_auto_20161002_1652', '2017-06-14 21:41:14.996783'),
(33, 'security', '0003_auto_20161002_2007', '2017-06-14 21:41:16.571967'),
(34, 'security', '0004_auto_20161004_2210', '2017-06-14 21:41:17.124718'),
(35, 'security', '0005_auto_20161010_0109', '2017-06-14 21:41:17.196602'),
(36, 'security', '0006_auto_20161010_0125', '2017-06-14 21:41:17.710238'),
(37, 'security', '0007_auto_20161010_0132', '2017-06-14 21:41:17.809235'),
(38, 'security', '0008_auto_20161010_1848', '2017-06-14 21:41:17.871600'),
(39, 'security', '0009_user_when_active', '2017-06-14 21:41:17.969508'),
(40, 'security', '0010_auto_20161016_2212', '2017-06-14 21:41:18.516692'),
(41, 'security', '0011_auto_20161021_1534', '2017-06-14 21:41:18.593656'),
(42, 'security', '0012_auto_20161022_1559', '2017-06-14 21:41:18.699324'),
(43, 'security', '0013_user_new_email', '2017-06-14 21:41:18.807609'),
(44, 'security', '0014_auto_20161022_1921', '2017-06-14 21:41:19.155161'),
(45, 'security', '0015_auto_20161023_0215', '2017-06-14 21:41:19.445069'),
(46, 'security', '0016_auto_20161029_1756', '2017-06-14 21:41:19.578051'),
(47, 'security', '0017_auto_20161029_1756', '2017-06-14 21:41:19.782007'),
(48, 'security', '0018_auto_20161029_1758', '2017-06-14 21:41:19.885792'),
(49, 'security', '0019_auto_20161029_1758', '2017-06-14 21:41:20.002572'),
(50, 'sessions', '0001_initial', '2017-06-14 21:41:20.054059'),
(51, 'api', '0019_auto_20170927_0200', '2017-09-27 02:15:15.453805'),
(52, 'base', '0002_auto_20170927_0200', '2017-09-27 02:15:15.585872'),
(53, 'security', '0020_auto_20170927_0200', '2017-09-27 02:15:16.007839'),
(54, 'api', '0020_paymentorder_free_days', '2017-09-27 02:45:07.925838'),
(55, 'api', '0021_auto_20171004_0026', '2017-10-05 22:55:58.954976'),
(56, 'api', '0022_auto_20171004_0219', '2017-10-05 22:55:59.883390'),
(57, 'api', '0023_auto_20171004_0238', '2017-10-05 22:56:00.068053'),
(58, 'api', '0024_auto_20171004_0330', '2017-10-05 22:56:00.398093'),
(59, 'api', '0025_auto_20171004_1917', '2017-10-05 22:56:00.578378'),
(60, 'api', '0026_auto_20171015_0224', '2017-10-15 02:27:28.286311'),
(61, 'security', '0021_auto_20171015_0224', '2017-10-15 02:28:55.910399'),
(62, 'api', '0027_auto_20180114_1814', '2018-01-14 22:16:58.572519'),
(63, 'api', '0028_auto_20180114_2017', '2018-01-14 22:16:58.712994'),
(64, 'api', '0029_auto_20180114_2129', '2018-01-14 22:16:59.034669');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `django_session`
--

CREATE TABLE `django_session` (
  `session_key` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `session_data` longtext COLLATE utf8_unicode_ci NOT NULL,
  `expire_date` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `django_session`
--

INSERT INTO `django_session` (`session_key`, `session_data`, `expire_date`) VALUES
('5ayiu0ukm6feqjyxajv0wrlrk3zthgco', 'ZTNmMTkzZjk5MWExYTZmMWU0ZGMxM2NmZTIyMjJhYzJmNDE5MDhmNDp7Il9hdXRoX3VzZXJfaGFzaCI6IjBiOWRmYTA3ZWVmYWJkNzhkM2VmMTVhMmZlMDZhN2JjZjg4MDViNTQiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiIxIn0=', '2018-01-28 22:19:50.172266'),
('fdxr4h786uf9xcrdum0nuqvigx3vqhxv', 'ZDZjYTkyZjJkZWMzNzI5NDJlMzUzNWE5N2NlYjIwZjNkYjM5OGVkMTp7fQ==', '2017-11-23 15:10:57.464145'),
('mv1i6923c7gcdy3341edi3vjtc85ga7k', 'NjBkYjY0MjQxOWU0MTgyYTY0NjUxMTQzNDU0YWQ4MTZhNmRkZTRkYzp7Il9hdXRoX3VzZXJfaGFzaCI6IjJlNzhhZDQ1ZTA2ZTI5MzRiNTQwZGZmYjNkODUzMjMzMGM2YjQ5Y2IiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOiI1In0=', '2018-02-01 23:12:41.497492'),
('qgvv0vqppt8ms2a7m1l2ja90oueasya6', 'ZDZjYTkyZjJkZWMzNzI5NDJlMzUzNWE5N2NlYjIwZjNkYjM5OGVkMTp7fQ==', '2018-01-28 22:42:06.404606'),
('yxxq1rgrus9vugob7v357fas5ph2rzer', 'ZDZjYTkyZjJkZWMzNzI5NDJlMzUzNWE5N2NlYjIwZjNkYjM5OGVkMTp7fQ==', '2017-11-23 15:13:57.136278');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `payment`
--

CREATE TABLE `payment` (
  `id` int(11) NOT NULL,
  `date` datetime(6) NOT NULL,
  `code` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `status` char(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'ACCEPTED',
  `amount` double NOT NULL,
  `payer_id` int(11) NOT NULL,
  `payment_order_id` int(11) NOT NULL,
  `when_created` datetime(6) NOT NULL,
  `who_created_id` int(11) DEFAULT NULL,
  `when_last_edit` datetime(6) NOT NULL,
  `who_last_edit_id` int(11) DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  `when_deleted` datetime(6) DEFAULT NULL,
  `who_deleted_id` int(11) DEFAULT NULL,
  `when_under_verification` datetime(6) DEFAULT NULL,
  `who_under_verification_id` int(11) DEFAULT NULL,
  `when_accepted` datetime(6) DEFAULT NULL,
  `who_accepted_id` int(11) DEFAULT NULL,
  `when_rejected` datetime(6) DEFAULT NULL,
  `who_rejected_id` int(11) DEFAULT NULL,
  `when_cancelled` datetime(6) DEFAULT NULL,
  `who_cancelled_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `payment`
--

INSERT INTO `payment` (`id`, `date`, `code`, `status`, `amount`, `payer_id`, `payment_order_id`, `when_created`, `who_created_id`, `when_last_edit`, `who_last_edit_id`, `is_deleted`, `when_deleted`, `who_deleted_id`, `when_under_verification`, `who_under_verification_id`, `when_accepted`, `who_accepted_id`, `when_rejected`, `who_rejected_id`, `when_cancelled`, `who_cancelled_id`) VALUES
(1, '2019-09-05 13:24:13.904334', 'PY-19-000010', 'PAID', 3000, 60, 1, '2019-09-05 13:24:13.903778', 60, '2019-09-05 13:24:13.903972', 60, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

--
-- Disparadores `payment`
--
DELIMITER $$
CREATE TRIGGER `payment_insert` AFTER INSERT ON `payment` FOR EACH ROW BEGIN
		# Update payment order
		UPDATE payment_order po
		   SET po.status = NEW.status
		 WHERE po.id = NEW.payment_order_id;
        
	END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `payment_update` AFTER UPDATE ON `payment` FOR EACH ROW BEGIN
		# Update payment order
		UPDATE payment_order po
		   SET po.status = NEW.status
		 WHERE po.id = NEW.payment_order_id;
        
	END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `payment_credit_cards`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `payment_credit_cards` (
`id` int(11)
,`payment_id` int(11)
,`credit_card_id` int(11)
,`token` varchar(100)
,`token_agro` varchar(10)
,`owner_name` varchar(150)
,`bin` int(6)
,`last_four_digits` int(4)
,`expiration_month` int(2)
,`expiration_year` int(4)
,`amount` double
);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `payment_method`
--

CREATE TABLE `payment_method` (
  `id` int(11) NOT NULL,
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `payment_method_id` int(11) NOT NULL,
  `bank_id` int(11) NOT NULL,
  `card_type` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `rate_fee` float NOT NULL,
  `mask` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `asset_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `payment_method`
--

INSERT INTO `payment_method` (`id`, `name`, `payment_method_id`, `bank_id`, `card_type`, `rate_fee`, `mask`, `asset_id`) VALUES
(1, 'AgroNacion', 91, 1, 'agro', 0.025, '1234', 1),
(2, 'Galicia Rural', 98, 2, 'agro', 0.02, '4765', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `payment_order`
--

CREATE TABLE `payment_order` (
  `id` int(11) NOT NULL,
  `uuid` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `date` datetime(6) NOT NULL,
  `status` char(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'PENDING',
  `beneficiary_id` int(11) NOT NULL,
  `payer_email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `payer_id` int(11) DEFAULT NULL,
  `user_bank_account_id` int(11) DEFAULT NULL,
  `credit_card_id` int(11) NOT NULL,
  `reference_number` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `invoice_number` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `term` int(10) UNSIGNED DEFAULT NULL,
  `amount` double NOT NULL,
  `amount_to_transfer` double DEFAULT NULL,
  `available_amount` double DEFAULT NULL,
  `feeds` json DEFAULT NULL,
  `when_created` datetime(6) NOT NULL,
  `who_created_id` int(11) DEFAULT NULL,
  `when_last_edit` datetime(6) NOT NULL,
  `who_last_edit_id` int(11) DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  `when_deleted` datetime(6) DEFAULT NULL,
  `who_deleted_id` int(11) DEFAULT NULL,
  `when_available` datetime(6) DEFAULT NULL,
  `who_available_id` int(11) DEFAULT NULL,
  `when_cancelled` datetime(6) DEFAULT NULL,
  `who_cancelled_id` int(11) DEFAULT NULL,
  `when_paid` datetime(6) DEFAULT NULL,
  `when_transferred` datetime(6) DEFAULT NULL,
  `who_transferred_id` int(11) DEFAULT NULL,
  `when_transferring` datetime(6) DEFAULT NULL,
  `who_transferring_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `payment_order`
--

INSERT INTO `payment_order` (`id`, `uuid`, `code`, `date`, `status`, `beneficiary_id`, `payer_email`, `payer_id`, `user_bank_account_id`, `credit_card_id`, `reference_number`, `invoice_number`, `description`, `term`, `amount`, `amount_to_transfer`, `available_amount`, `feeds`, `when_created`, `who_created_id`, `when_last_edit`, `who_last_edit_id`, `is_deleted`, `when_deleted`, `who_deleted_id`, `when_available`, `who_available_id`, `when_cancelled`, `who_cancelled_id`, `when_paid`, `when_transferred`, `who_transferred_id`, `when_transferring`, `who_transferring_id`) VALUES
(1, '031732f3249548af89c661d491fc7c3b', 'PO-19-000019', '2019-09-05 13:17:09.543746', 'PAID', 59, 'agropago.test01@gmail.com', 60, NULL, 2, '', '123456-01', 'bla bla bla', 30, 3000, NULL, NULL, '{\"cft\": 0, \"arancel\": 0.02, \"ivaArancel\": 0.21, \"ivaBankRate\": 0.21, \"totalAmount\": 2891.1, \"apCommission\": 0.01, \"arancelTotal\": 60, \"IIBBRetention\": 0, \"earningRetetion\": 0, \"ivaAPCommission\": 0.21, \"ivaArancelTotal\": 12.6, \"ivaBankRateTotal\": 0, \"apCommissionTotal\": 30, \"IIBBRetentionTotal\": 0, \"earningRetetionTotal\": 0, \"ivaAPCommissionTotal\": 6.3, \"proporcionalBankrate\": 0, \"proporcionalBankrateTotal\": 0}', '2019-09-05 13:17:09.542325', 59, '2019-09-05 13:17:09.542846', 59, 0, NULL, NULL, NULL, NULL, NULL, NULL, '2019-09-05 00:00:00.000000', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rate`
--

CREATE TABLE `rate` (
  `id` int(11) NOT NULL,
  `days_labels` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `rate_label` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `perc` float NOT NULL,
  `days` int(11) NOT NULL,
  `paymentmethod_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `rate`
--

INSERT INTO `rate` (`id`, `days_labels`, `rate_label`, `perc`, `days`, `paymentmethod_id`) VALUES
(1, 'Hasta 30', '29.90% Tasa TNA.', 0, 30, 1),
(2, 'Hasta 60', '29.90% Tasa TNA.', 0.299, 60, 1),
(3, 'Hasta 90', '29.90% Tasa TNA.', 0.299, 90, 1),
(4, 'Hasta 120', '29.90% Tasa TNA.', 0.299, 120, 1),
(5, 'Hasta 150', '29.90% Tasa TNA.', 0.299, 150, 1),
(6, 'Hasta 180', '29.90% Tasa TNA.', 0.299, 180, 1),
(7, 'Hasta 30', '29.90% Tasa TNA.', 0, 30, 2),
(8, 'Hasta 60', '29.90% Tasa TNA.', 0.299, 60, 2),
(9, 'Hasta 90', '29.90% Tasa TNA.', 0.299, 90, 2),
(10, 'Hasta 120', '29.90% Tasa TNA.', 0.299, 120, 2),
(11, 'Hasta 150', '29.90% Tasa TNA.', 0.299, 150, 2),
(12, 'Hasta 180', '29.90% Tasa TNA.', 0.299, 180, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `security_user`
--

CREATE TABLE `security_user` (
  `id` int(11) NOT NULL,
  `password` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `when_created` datetime(6) NOT NULL,
  `when_last_edit` datetime(6) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  `when_deleted` datetime(6) DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `who_created_id` int(11) DEFAULT NULL,
  `who_deleted_id` int(11) DEFAULT NULL,
  `who_last_edit_id` int(11) DEFAULT NULL,
  `birthdate` date DEFAULT NULL,
  `cuit` varchar(11) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dni` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `valid_email` tinyint(1) NOT NULL,
  `when_inactive` datetime(6) DEFAULT NULL,
  `when_valid_email` datetime(6) DEFAULT NULL,
  `who_inactive_id` int(11) DEFAULT NULL,
  `when_active` datetime(6) DEFAULT NULL,
  `legal_name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `new_email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `terms_accepted` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `security_user`
--

INSERT INTO `security_user` (`id`, `password`, `last_login`, `is_superuser`, `when_created`, `when_last_edit`, `is_deleted`, `when_deleted`, `email`, `first_name`, `last_name`, `is_active`, `who_created_id`, `who_deleted_id`, `who_last_edit_id`, `birthdate`, `cuit`, `dni`, `valid_email`, `when_inactive`, `when_valid_email`, `who_inactive_id`, `when_active`, `legal_name`, `new_email`, `phone`, `terms_accepted`) VALUES
(1, 'pbkdf2_sha256$20000$5WpUx25IoWEu$XXHbrncJGQ/jfy94fTuwz/rGUGORTRYxX3CERbsYJ5M=', '2019-03-19 14:41:03.722455', 1, '2017-06-14 22:57:23.664232', '2017-06-14 22:57:23.664535', 0, NULL, 'fgarcia@trimstudios.com.ar', 'Federico Javier\r\n', 'Garcia Dura', 1, 1, NULL, 1, '1982-10-08', '24296080574', '29608057', 1, NULL, '2017-06-16 21:02:40.000000', NULL, NULL, 'Trim Studios SRL', '', '351-5906060', 1),
(5, 'pbkdf2_sha256$20000$rti79srRydNv$OVTQmIO5d/u600ES+lwwqrpApoP9fBq0q+EwO6z63IY=', '2018-07-25 00:44:37.227561', 0, '2017-09-07 21:15:52.967713', '2017-09-18 21:38:24.380856', 0, NULL, 'pbk.pablo.a@gmail.com', 'Pablo', 'Acuña', 1, 1, NULL, 5, '1990-02-08', '20347714152', '34771415', 1, NULL, '2017-09-07 21:17:36.918968', NULL, '2017-09-07 21:17:36.918961', 'Pablanka SRL', 'pablo_ad@live.com.ar', '3512100142', 1),
(13, 'pbkdf2_sha256$20000$SNPV2DBEi29m$bc0ZmrrIjli94X2+bzXwWwPBZo3u7YIRW4pR6Tg35n4=', '2019-08-15 13:56:19.760809', 0, '2017-09-24 16:28:33.111169', '2019-01-23 20:54:33.220755', 0, NULL, 'matias@gcagro.com.ar', 'Matías', 'Méndez', 1, 1, NULL, 13, '1981-01-09', '30712088741', '28489917', 1, NULL, '2017-09-24 16:30:24.044237', NULL, '2017-09-24 16:30:24.044223', 'GCagro SRL', '', '03445481721', 1),
(14, 'pbkdf2_sha256$20000$9rouuq8Q0QJp$JiEC1b69mABdtO4j2w4uyWgaqeJXkEZvuN/3YwuWVvI=', '2017-09-27 12:05:56.461376', 0, '2017-09-27 12:05:04.649939', '2017-09-27 12:05:34.239708', 0, NULL, 'julian.dominguez@mailinator.com', 'Julian', 'Dominguez', 1, 1, NULL, 1, '1975-09-05', '3071129693', '32568499', 1, NULL, '2017-09-27 12:05:34.238809', NULL, '2017-09-27 12:05:34.238802', 'Dominguez y Asociados SRL', '', '46845595', 1),
(15, 'pbkdf2_sha256$20000$dgioXMTnQ0so$Ab+8FEjO6eADWOJFO092LNstbYwoN2DFFnXTvfAQdjU=', '2018-06-28 18:40:30.037846', 0, '2017-09-28 18:23:59.083545', '2017-10-03 14:52:38.279877', 0, NULL, 'ventas@agropoints.com', 'Patricio', 'Bacigalupo', 1, 1, NULL, 15, '1982-02-08', '30712721908', '29423654', 1, NULL, '2017-09-28 18:24:53.461656', NULL, '2017-09-28 18:24:53.461649', 'Agropoints SRL', '', '3515214221', 1),
(16, 'pbkdf2_sha256$20000$GAb9xB3wae8q$sBTB7sJ+iS3f+f9qCCJZdd1UZoeHOgizAXdUxY/PUAA=', '2019-08-29 18:58:18.221966', 1, '2017-09-28 19:44:33.256458', '2019-08-29 18:57:33.180654', 0, NULL, 'nacho22martin@gmail.com', 'Ignacio', 'Martin', 1, 1, NULL, 16, '1983-04-22', '30714937525', '30125591', 1, NULL, '2017-09-28 19:45:34.565668', NULL, '2017-09-28 19:45:34.565662', 'Inglobe SRL', '', '3513284036', 1),
(18, 'pbkdf2_sha256$20000$urVLrlUQg0UR$wa8nT8IMEZz9m3dEwB22597/oL8rhgFy55p5D1qVp4g=', NULL, 0, '2017-10-03 14:30:11.350848', '2017-10-03 14:30:11.351306', 0, NULL, 'nutrigrand@avegrand.com', 'Ana Clara', 'Lumbia', 0, 1, NULL, 1, '1983-08-21', '30710257295', '30313321', 0, NULL, NULL, NULL, NULL, 'Avegrand SA', '', '0344515479381', 1),
(20, 'pbkdf2_sha256$20000$FMaP09ZxEkXS$CEUYeobUtz5DyZ6BpdAlctI8c+OpRrOguWqgd8U4i14=', '2017-10-12 19:22:59.099608', 0, '2017-10-12 19:06:02.005896', '2017-10-12 19:11:27.143740', 0, NULL, 'agro-ventas@hotmail.com', 'Anibal', 'Vuagniaux', 1, 1, NULL, 1, '1978-03-15', '20262533868', '26253386', 1, NULL, '2017-10-12 19:11:27.142937', NULL, '2017-10-12 19:11:27.142931', 'Vuagniaux Anibal', '', '3445653348', 1),
(21, 'pbkdf2_sha256$20000$5ecNkFLMe8n8$g1KegeMCXB7kc5NCdJEgCMc+IVA8yIoCV0BpWJndsyY=', '2017-10-26 19:16:51.523227', 0, '2017-10-26 16:20:03.962857', '2017-10-26 18:40:39.453098', 0, NULL, 'lsantafe@profarm.com.ar', 'Leandro', 'Santafe', 1, 1, NULL, 1, '1989-10-11', '30710277474', '34663463', 1, NULL, '2017-10-26 18:40:39.450230', NULL, '2017-10-26 18:40:39.450222', 'M.C.M SRL', '', '3472559249', 1),
(22, 'pbkdf2_sha256$20000$oBINfuyV1KvT$zLUXBfJNgaSlx5/RLReK0x6p1r0jNCF2Zw5i3xrDToQ=', '2018-01-26 15:15:24.726586', 0, '2017-10-31 13:10:21.506115', '2018-01-26 14:15:18.505651', 0, NULL, 'cobranza@alltecsa.com.ar', 'Zuleika', 'Gorostegui', 1, 1, NULL, 22, '1989-07-23', '33704571599', '34397136', 1, NULL, '2017-10-31 13:10:50.070235', NULL, '2017-10-31 13:10:50.070192', 'ALLTEC SA', '', '3624-040277', 1),
(23, 'pbkdf2_sha256$20000$W6XNYYTzSB6s$sz54ImVDuR8Qh8jLycuLOYxJgb3wsEh3XjJhFeh2WZE=', '2019-05-20 20:58:10.335167', 0, '2017-11-02 13:10:11.980712', '2017-11-02 13:10:51.699551', 0, NULL, 'felipe.sanchezmalo@agroucacha.com', 'David Guido', 'Flores', 1, 1, NULL, 1, '1950-06-23', '30638888811', '8401139', 1, NULL, '2017-11-02 13:10:51.697687', NULL, '2017-11-02 13:10:51.697683', 'AGRO UCACHA SRL', '', '0358-4630492', 1),
(24, 'pbkdf2_sha256$20000$qf2wP412I3wF$o9lGq80mJsEwWTBA/Xv8Iut4AznYtfZ2je2etPAJxKQ=', '2017-11-04 12:21:04.381057', 0, '2017-11-04 00:05:07.990392', '2017-11-04 12:25:07.332410', 0, NULL, 'betoheinse@gmail.com', 'Oscar Alberto', 'Heinse', 1, 1, NULL, 24, '1952-03-02', '20102184107', '10218410', 1, NULL, '2017-11-04 09:30:13.478618', NULL, '2017-11-04 09:30:13.478615', 'Heinse Oscar Alberto', '', '0344515475637', 1),
(26, 'pbkdf2_sha256$20000$IIHYQ8bmcd3F$yEDKzotOUO4Ez6VoI8X1HMSMW720pQGrOaixJt1UeNA=', NULL, 0, '2017-11-09 18:23:46.258844', '2017-11-09 18:25:12.674271', 0, NULL, 'jualvarez@gmail.com', 'Juan', 'Alvarez', 1, 1, NULL, 1, '1983-08-01', '20303289942', '30328994', 1, NULL, '2017-11-09 18:25:12.673284', NULL, '2017-11-09 18:25:12.673276', 'Pruebilla', '', '+5493512049760', 1),
(27, 'pbkdf2_sha256$20000$gW7g61ME7M3n$dgdSEVH4t4vHiBq0jmeVtxRpkvYc3R0cfNdMkBwDtpc=', '2017-11-16 15:35:44.796789', 0, '2017-11-16 15:33:42.490121', '2017-11-16 15:35:16.710370', 0, NULL, 'ventas@sintesisbiologica.com.ar', 'JIMENA', 'SABOR', 1, 1, NULL, 1, '1968-12-23', '27210618584', '21061858', 1, NULL, '2017-11-16 15:35:16.705239', NULL, '2017-11-16 15:35:16.705225', 'SABOR JIMENA', '', '3512336641', 1),
(28, 'pbkdf2_sha256$20000$44MMwVI95vzX$4gjA8HfTgCYJV/65LKBiIh1D0hhvV7dFUg+keG3qA+E=', '2017-12-27 13:02:00.743484', 0, '2017-12-06 23:11:53.306646', '2017-12-06 23:21:07.344448', 0, NULL, 'matiasdmendez@gmail.com', 'Matías', 'Méndez', 1, 1, NULL, 1, '1981-01-09', '30715237039', '28489917', 1, NULL, '2017-12-06 23:21:07.343891', NULL, '2017-12-06 23:21:07.343887', 'Fideicomiso GCagro', '', '0344515475637', 1),
(29, 'pbkdf2_sha256$20000$hjykRLbWZzPH$n3dQx37y2eyp0TMRdQ23MJP+3P44hWP2gVTCF2MDTg4=', '2017-12-12 15:07:04.179779', 0, '2017-12-11 19:26:43.293774', '2017-12-11 19:27:31.630473', 0, NULL, 'cmlpolverini@gmail.com', 'Carina Maria de Lujan', 'Polverini', 1, 1, NULL, 1, '1970-04-06', '27215475013', '21547501', 1, NULL, '2017-12-11 19:27:31.629801', NULL, '2017-12-11 19:27:31.629797', 'Polverini Carina Maria Del Lujan', '', '0347615694716', 1),
(30, 'pbkdf2_sha256$20000$FktdMlJxHcfy$PebOBQko12E31zwfw9JmCYSggT9rJ7N8jz90mGhFOA0=', '2017-12-12 11:40:50.280435', 0, '2017-12-11 19:37:24.725981', '2017-12-11 19:37:37.104542', 0, NULL, 'prasetto@gmail.com', 'Pedro Alfredo', 'Rasetto', 1, 1, NULL, 1, '1970-05-03', '23215475069', '21547506', 1, NULL, '2017-12-11 19:37:37.103737', NULL, '2017-12-11 19:37:37.103733', 'Rasetto Pedro Alfredo', '', '0347615694716', 1),
(31, 'pbkdf2_sha256$20000$3TjWcShqoGtg$bDp+431C2GCXia1z887y5m/JxM7T9WSkBjB+WTiWPnE=', NULL, 0, '2018-01-17 11:40:23.275752', '2018-01-17 11:40:23.276497', 0, NULL, 'sfonseca@ecoruralsa.com', 'Santiago', 'Fonseca', 0, 1, NULL, 1, '1978-03-01', '30708351179', '26538659', 0, NULL, NULL, NULL, NULL, 'Agropecuaria Eco Rural S.A.', '', '3415805429', 1),
(32, 'pbkdf2_sha256$20000$VRGr2R1gfO4d$l7OUxm/T0ZtuCZtaEAcap06/lCVaZWbfz/hVa96RB30=', '2019-02-20 19:59:46.606000', 0, '2018-01-30 17:54:05.941677', '2018-01-30 17:55:25.295901', 0, NULL, 'maquinariasagroviales@gmail.com', 'Fernando', 'Listello', 1, 1, NULL, 1, '1987-12-02', '20330109581', '33010958', 1, NULL, '2018-01-30 17:55:25.295193', NULL, '2018-01-30 17:55:25.295189', 'Maquinarias Agroviales GEFERLIST', '', '0351156200935', 1),
(33, 'pbkdf2_sha256$20000$Jhved0ikRddm$hQ/wyGCCKX3vEkdikg6Uuu5Dd6bu8wqjFX7zcmpXIHM=', '2018-04-20 22:30:35.004437', 0, '2018-04-20 22:29:38.155871', '2018-04-20 22:30:27.151383', 0, NULL, 'mackinlay.santiago@gmail.com', 'santiago', 'mackinlay', 1, 1, NULL, 1, '1987-02-12', '20326938441', '32693844', 1, NULL, '2018-04-20 22:30:27.150263', NULL, '2018-04-20 22:30:27.150259', 'Santiago Mackinlay', '', '03388 15673607', 1),
(34, 'pbkdf2_sha256$20000$riwckANRTw8u$LknjpahAqstLg4OIoF1IIgT/gDd83DnjxeLLy5XBaFo=', '2018-05-18 12:04:14.972640', 0, '2018-05-18 11:59:36.989691', '2018-05-18 12:00:38.997515', 0, NULL, 'juanmartin@vaqapp.com', 'Juan Martin', 'Pagella', 1, 1, NULL, 1, '1987-12-29', '30715747665', '33096967', 1, NULL, '2018-05-18 12:00:38.996858', NULL, '2018-05-18 12:00:38.996853', 'VAQAPP SAS', '', '2364337950', 1),
(35, 'pbkdf2_sha256$20000$Rl5Y3iFdXQCS$+WedquwKbXPsEJLtsiBjDDUih4XNy9b6k5Z7OX1lz1E=', '2019-03-14 23:36:08.529125', 0, '2018-05-28 18:35:26.715448', '2018-05-28 18:36:55.425025', 0, NULL, 'podokocompra@gmail.com', 'Jesus', 'Del Valle', 1, 1, NULL, 1, '1949-05-03', '33083058728', '8305872', 1, NULL, '2018-05-28 18:36:55.424653', NULL, '2018-05-28 18:36:55.424650', 'AgroPo', '', '35489076', 1),
(36, 'pbkdf2_sha256$20000$6YbkeARZ6lp4$KfYAoILc7pdVDJsY3yT/Nngp6nzIZzLH/AaPSRw+/oQ=', '2018-06-22 18:11:05.890797', 0, '2018-06-22 17:54:10.838957', '2018-06-22 18:08:17.470306', 0, NULL, 'bardinjavier@gmail.com', 'Javier', 'Bardin', 1, 1, NULL, 1, '1965-08-18', '30710070136', '17211431', 1, NULL, '2018-06-22 18:08:17.469949', NULL, '2018-06-22 18:08:17.469946', 'Los Amigos de J y A S.A.', '', '2284529279', 1),
(37, 'pbkdf2_sha256$20000$LuRVzS7fwc22$S6+b8xQVJcaYnVahhIYsguAzdLkXkhGVcq8NklQfUJk=', '2018-10-30 17:10:20.582519', 0, '2018-07-04 14:52:26.527839', '2018-07-04 14:52:56.069163', 0, NULL, 'ggimenez@olpays.com', 'GERMAN', 'GIMENEZ', 1, 1, NULL, 1, '1981-05-06', '30715502050', '28627551', 1, NULL, '2018-07-04 14:52:56.068795', NULL, '2018-07-04 14:52:56.068792', 'OLP SOLUTIONS SA', '', '2615501137', 1),
(38, 'pbkdf2_sha256$20000$ehBkkDngM4A6$M9tXhEpdEX2GH0mOuvj9dt3+AJ/vuP25HS4r+To016U=', '2018-07-19 15:53:59.683739', 0, '2018-07-19 15:52:14.773585', '2018-07-19 15:52:35.489264', 0, NULL, 'javiercomandos11@gmail.com', 'Javier', 'De la hoz', 1, 1, NULL, 1, '1991-03-15', '20369149742', '33456743', 1, NULL, '2018-07-19 15:52:35.487757', NULL, '2018-07-19 15:52:35.487753', 'Agrospec sa', '', '45239685', 1),
(39, 'pbkdf2_sha256$20000$iLzj2mvGDb7b$3AwDDaS+5UuLIfbTTUm20+1/wy5VgEwYMN34dVEZeF4=', '2018-07-31 03:54:11.176775', 0, '2018-07-31 03:53:19.108591', '2018-07-31 03:53:55.056468', 0, NULL, 'grupoipmax@gmail.com', 'nair', 'funes', 1, 1, NULL, 1, '1995-10-02', '27390792471', '39079247', 1, NULL, '2018-07-31 03:53:55.054376', NULL, '2018-07-31 03:53:55.054373', 'unicred', '', '3517911910', 1),
(40, 'pbkdf2_sha256$20000$YeMWlyB9fp8z$FNPRiXVpmPn8myEH232qAIfw71zXTfG/71ZPkc4oerQ=', '2018-12-28 20:04:04.185532', 0, '2018-08-11 02:09:19.636827', '2018-08-11 02:16:03.434303', 0, NULL, 'agustingrimaut@gmail.com', 'Agustin Matias', 'Grimaut', 1, 1, NULL, 40, '1989-05-12', '20343175524', '34317552', 1, NULL, '2018-08-11 02:11:20.536804', NULL, '2018-08-11 02:11:20.536798', 'Agustin Grimaut', '', '3513040236', 1),
(41, 'pbkdf2_sha256$20000$YmOWbnfPnx8C$c13DCq6ukPivSY6ytAuF42Q+Rb/fY9GgJHkdHKVmQMw=', '2018-08-17 22:39:30.819291', 0, '2018-08-17 22:38:10.101554', '2018-08-17 22:38:46.469028', 0, NULL, 'alfredolgarcia@gmail.com', 'Alfredo', 'Garcia', 1, 1, NULL, 1, '1976-10-22', '20255574885', '25557488', 1, NULL, '2018-08-17 22:38:46.466906', NULL, '2018-08-17 22:38:46.466903', 'Alfredo garcia', '', '5491165042200', 1),
(42, 'pbkdf2_sha256$20000$OePe9Ulutf8I$5N3ojVYl/zjjWEKqh2cVk5oYayu0rmixrWXOtS+sgVA=', '2019-04-26 17:43:02.324948', 0, '2018-08-30 01:37:53.588376', '2019-01-09 23:13:08.651537', 0, NULL, 'luz.eramallo@gmail.com', 'Luz', 'Ramallo', 1, 1, NULL, 42, '1987-03-24', '27329680938', '32968093', 1, NULL, '2018-08-30 01:38:38.997438', NULL, '2018-08-30 01:38:38.997434', 'LR', '', '5407037', 1),
(43, 'pbkdf2_sha256$20000$dAGam0eqJk3A$rmYjJMqSSApOBOKrKVp1nXpExw8wO9WQKxiIP/AfI3Q=', '2018-09-26 14:43:34.968346', 0, '2018-09-24 22:28:55.982080', '2018-09-24 22:31:21.326055', 0, NULL, 'gaston@esalort.com', 'Gaston', 'Salort', 1, 1, NULL, 1, '1989-04-05', '20344407399', '34440739', 1, NULL, '2018-09-24 22:31:21.325639', NULL, '2018-09-24 22:31:21.325636', 'E', '', '3513075448', 1),
(44, 'pbkdf2_sha256$20000$nGYk35k8rlQw$JwITsG+AzfomV1T1F5M/Ler2q4UVcSLaZqeq3Zphj6Y=', '2019-03-14 04:15:56.191875', 0, '2018-09-25 11:43:19.630066', '2018-09-25 11:46:03.186938', 0, NULL, 'imartin@agropago.com', 'Ignacio', 'Martin', 1, 1, NULL, 1, '1983-04-22', '30716142422', '30125591', 1, NULL, '2018-09-25 11:46:03.186551', NULL, '2018-09-25 11:46:03.186548', 'Agrodigital SAS', '', '153284036', 1),
(45, 'pbkdf2_sha256$20000$MOwUovzJxzy9$ioSzwfSS8ad8Nr8j0GrT/2TWozMA+AelCTGDTJOKAHI=', NULL, 0, '2018-10-04 02:32:31.893927', '2018-10-04 02:32:31.894637', 0, NULL, 'test@test.com', 'test', 'test', 0, 1, NULL, 1, '2018-10-03', '33708031599', 'test', 0, NULL, NULL, NULL, NULL, 'test', '', 'test', 1),
(46, 'pbkdf2_sha256$20000$5sQvYgfTWWyR$7glGnL298F1YESc7drNK87G97E8cVDmM8PGRMsoC9xA=', '2018-10-09 04:09:27.286089', 0, '2018-10-09 04:02:37.370167', '2018-10-09 04:02:57.924797', 0, NULL, 'info@bestlinens.com.ar', 'Liliana', 'Barbarito', 1, 1, NULL, 1, '1947-12-19', '30712166521', '5704005', 1, NULL, '2018-10-09 04:02:57.924317', NULL, '2018-10-09 04:02:57.924313', 'INCANTO SRL', '', '1131109383', 1),
(47, 'pbkdf2_sha256$20000$yluRZOATJG25$pqIOAs+umEfNl1AE/XoQWGXQACRh21HcNq+s08pQ0bw=', '2018-10-30 23:44:57.839757', 0, '2018-10-30 21:28:28.087402', '2018-10-30 21:31:26.529245', 0, NULL, 'pepeargento@yopmail.com', 'Pepe', 'Argento', 1, 1, NULL, 1, '2018-01-01', '23999999999', '99999999', 1, NULL, '2018-10-30 21:31:26.528859', NULL, '2018-10-30 21:31:26.528856', 'Argento S.A', '', '4240000', 1),
(48, 'pbkdf2_sha256$20000$laUTKVMIUKz2$5jKGTN0wCNIgIcbmSFGEWoz99aRJ9X4ulXh3vViIavU=', '2018-12-18 00:48:05.524504', 0, '2018-12-18 00:33:02.981932', '2018-12-18 00:40:34.251696', 0, NULL, 'perezjuanap@mailinator.com', 'Juan', 'Perez', 1, 1, NULL, 48, '1990-01-15', '20123456786', '12345678', 1, NULL, '2018-12-18 00:34:19.893239', NULL, '2018-12-18 00:34:19.893236', 'PEREZ JUAN', '', '3561710728', 1),
(49, 'pbkdf2_sha256$20000$IGUhA4hVUUfO$saYYj+Jg9Q9aJUpdc00GUxskngH0fJBsafz2KpzKG/Y=', '2019-01-10 23:57:06.665049', 0, '2019-01-10 23:50:09.699758', '2019-01-10 23:52:15.504148', 0, NULL, 'magdabas6@gmail.com', 'magdalena', 'bas', 1, 1, NULL, 1, '1983-07-02', '27303290708', '30329070', 1, NULL, '2019-01-10 23:52:15.503780', NULL, '2019-01-10 23:52:15.503777', 'Suma Recursos Humanos', '', '3515214221', 1),
(50, 'pbkdf2_sha256$20000$wgJscAiAWOQx$JtPBywLcbSLG+qZZh3ousG1pzxB+0uAcZtKOqSFyYrE=', '2019-08-19 18:37:21.210855', 1, '2019-02-14 04:10:15.563217', '2019-02-14 04:11:07.301536', 0, NULL, 'patobaci@gmail.com', 'Patricio', 'Bacigalupo', 1, 1, NULL, 1, '1982-02-08', '30716095009', '29423654', 1, NULL, '2019-02-14 04:11:07.301127', NULL, '2019-02-14 04:11:07.301124', 'AP Soluciones SAS', '', '3515214221', 1),
(51, 'pbkdf2_sha256$20000$k6wODTXS78A6$53FcNB+LFfD7oNkfd6ERDj7nvXNscV7dQY6OqhVQHj8=', '2019-07-15 13:33:31.840931', 0, '2019-02-20 17:19:11.510886', '2019-07-15 13:33:01.668476', 0, NULL, 'Alfredo.l.garcia@bancogalicia.com.ar', 'Alfredo', 'Garcia', 1, 1, NULL, 51, '1981-01-01', '20214215471', '41258465', 1, NULL, '2019-02-20 18:07:49.869135', NULL, '2019-02-20 18:07:49.869130', 'Banco Galicia', '', '+54 9 11 6504-2200', 1),
(52, 'pbkdf2_sha256$20000$GeJO5veRmhlD$yXX8ez1KCRD4BXeT99cDfzn6BF/aMXCSgtjpSrIn7OQ=', '2019-02-23 13:11:42.751088', 0, '2019-02-23 13:11:09.120485', '2019-02-23 13:11:34.384686', 0, NULL, 'agrovillagra@gmail.com', 'DARIO MARCOS', 'VILLAGRA', 1, 1, NULL, 1, '1980-10-30', '20282489423', '28248942', 1, NULL, '2019-02-23 13:11:34.384265', NULL, '2019-02-23 13:11:34.384262', 'VILLAGRA DARIO MARCOS', '', '03564569629', 1),
(53, 'pbkdf2_sha256$20000$cijvKNIwcRFY$1J5cy7Uj7hLopYYvfsSwXo7UKaHHr70mnyOTwW+RZcs=', '2019-02-25 19:36:12.882667', 0, '2019-02-25 19:35:00.222918', '2019-02-25 19:36:09.094414', 0, NULL, 'luxifago@gmail.com', 'Gerardo', 'Lagger', 1, 1, NULL, 1, '1989-05-16', '20342995706', '34299570', 1, NULL, '2019-02-25 19:36:09.093792', NULL, '2019-02-25 19:36:09.093786', 'Gerardo Lagger', '', '3416971616', 1),
(54, 'pbkdf2_sha256$20000$bqG5bD9P9K5F$zYSQrEkgqkA+r9U9fKAUVE5HXU45j65/xE61uF1FryY=', '2019-02-26 21:30:56.904749', 0, '2019-02-26 13:36:14.577482', '2019-02-26 13:36:38.788475', 0, NULL, 'uriburu@mailinator.com', 'Jose Ebaristo', 'Uriburu', 1, 1, NULL, 1, '1989-05-31', '30610798892', '28277311', 1, NULL, '2019-02-26 13:36:38.788087', NULL, '2019-02-26 13:36:38.788084', 'URIBURU SA', '', '02473454719', 1),
(55, 'pbkdf2_sha256$20000$befDVuL1TSm3$uvWPjhGPenVXMWD5IEazCKvEDGYfkhqoUkNh1PeCRYo=', '2019-05-16 20:07:39.578531', 0, '2019-03-01 12:49:43.088716', '2019-05-16 20:06:53.611709', 0, NULL, 'smganadera_sa@live.com.ar', 'Sergio Alberto', 'Mauhum', 1, 1, NULL, 55, '1965-01-19', '30711142076', '17145666', 1, NULL, '2019-03-01 13:25:19.570870', NULL, '2019-03-01 13:25:19.570867', 'SM Ganaderasa', '', '0353-155667139', 1),
(56, 'pbkdf2_sha256$20000$UpRhuvvGjXdM$GBwqbz7Ot/pK47tQlXBNSS/mwH7JLqBswQ31pEZeOwQ=', '2019-03-01 16:58:01.204716', 0, '2019-03-01 16:54:48.337891', '2019-03-01 16:57:15.865748', 0, NULL, 'carolina2050@hotmail.com', 'carolina', 'ruiz', 1, 1, NULL, 1, '1981-10-19', '27283970405', '28397040', 1, NULL, '2019-03-01 16:57:15.865346', NULL, '2019-03-01 16:57:15.865343', 'empresaria', '', '3874887573', 1),
(57, 'pbkdf2_sha256$20000$Hqx8XXtx91lq$mHb2/N3BlkbrrE3La2XFeuuW5FWu9aejo86K3WL227g=', '2019-03-01 17:56:16.557495', 0, '2019-03-01 17:48:11.298535', '2019-03-01 17:55:53.879086', 0, NULL, 'mica@kilimoagtech.com', 'Micaela', 'Bertino', 1, 1, NULL, 1, '1988-11-09', '30715107941', '33916768', 1, NULL, '2019-03-01 17:55:53.878636', NULL, '2019-03-01 17:55:53.878633', 'KILIMO S.A.', '', '0351152055812', 1),
(58, 'pbkdf2_sha256$20000$2gDhXV8qI7eX$3XC1/7WfqwYYUQrSyfsKGvDgAwAMsiEKKjd0Jp4ngbU=', '2019-03-05 02:30:37.960521', 0, '2019-03-05 01:28:04.337195', '2019-03-05 01:31:34.856109', 0, NULL, 'mra@durtom.com', 'Mauricio', 'Arizaga', 1, 1, NULL, 1, '1953-03-23', '20107737228', '10773722', 1, NULL, '2019-03-05 01:31:34.855682', NULL, '2019-03-05 01:31:34.855678', 'Durtom', '', '+5493516865446', 1),
(59, 'pbkdf2_sha256$20000$aoJ45PSI91ua$AdO+CgefhABS4uNlBC5ELh6kWc6MpMoAuStckJh1l80=', '2019-03-20 11:22:21.099771', 0, '2019-03-19 13:12:25.742366', '2019-03-19 13:13:22.740484', 0, NULL, 'raul.osvaldo.robledo@gmail.com', 'Raul', 'Robledo', 1, 1, NULL, 1, '1971-11-09', '20222473226', '22247322', 1, NULL, '2019-03-19 13:13:22.740209', NULL, '2019-03-19 13:13:22.740207', 'Monotributista', '', '3549558493', 1),
(60, 'pbkdf2_sha256$20000$xQVjGguxHRw1$I5eoKdMnbFz24OoHl06d9DnTuWXW4d0ImqK5An5TiuI=', '2019-08-05 19:48:29.098081', 0, '2019-03-20 03:36:07.978931', '2019-03-20 03:43:58.655107', 0, NULL, 'agropago.test01@gmail.com', 'test01', 'agropago', 1, 1, NULL, 1, '1971-11-09', '20111111112', '22247322', 1, NULL, '2019-03-20 03:43:58.654822', NULL, '2019-03-20 03:43:58.654820', 'Raul', '', '1615616161', 1),
(61, 'pbkdf2_sha256$20000$49KEF577BBH6$4ySgY4gMGs7AWj8u3ncfUcs6A4dCvT6k8TBaPY8K/EQ=', '2019-05-28 15:22:27.015151', 0, '2019-03-20 11:52:01.043761', '2019-03-20 11:52:58.363709', 0, NULL, 'agropago.test02@gmail.com', 'test02', 'agropago', 1, 1, NULL, 1, '1978-01-01', '20222222223', '22222222', 1, NULL, '2019-03-20 11:52:58.363466', NULL, '2019-03-20 11:52:58.363464', 'test02', '', '456456465465', 1),
(62, 'pbkdf2_sha256$20000$PqBgGbm0xXi1$SWmy4zeF/hR8TI0+66E+Phos3QTSyVn8YWEjqFOAODQ=', '2019-04-17 11:49:50.866090', 1, '2019-03-20 12:00:05.260558', '2019-03-20 12:05:39.956182', 0, NULL, 'agropago.adm@gmail.com', 'admin', 'agropago', 1, 1, NULL, 1, '1978-01-01', '20333333334', '33333333', 1, NULL, '2019-03-20 12:05:39.955909', NULL, '2019-03-20 12:05:39.955907', 'admin', '', '456456456465464', 1),
(63, 'pbkdf2_sha256$20000$sEYiNHbad4PC$SNevJNlH3JRF5xrIf/FUMmqiOtIxyGfwjNsVqie5/ks=', '2019-03-20 12:47:41.468999', 0, '2019-03-20 12:45:12.583736', '2019-03-20 12:47:09.006617', 0, NULL, 'candelero@copca.com.ar', 'Luis Gabriel', 'Candelero', 1, 1, NULL, 1, '1966-06-03', '20176456567', '17645656', 1, NULL, '2019-03-20 12:47:09.006269', NULL, '2019-03-20 12:47:09.006267', 'Candelero, Luis Gabriel', '', '3467-446488', 1),
(64, 'pbkdf2_sha256$20000$M84NSjmZDl2P$38XFvlSf2zDi0i5+4WwJiRTxmvftzyH/A+P1W7fIKEw=', '2019-03-20 19:22:14.684992', 0, '2019-03-20 19:19:27.905980', '2019-03-20 19:20:06.123433', 0, NULL, 'rdenicola@colomboymagliano.com.ar', 'Roberto', 'De Nicola', 1, 1, NULL, 1, '1980-08-12', '30520760535', '28337261', 1, NULL, '2019-03-20 19:20:06.123080', NULL, '2019-03-20 19:20:06.123078', 'Colombo y Magliano sa', '', '43349511', 1),
(65, 'pbkdf2_sha256$20000$MwFNq6hmNadZ$eN5vohhLudlSvE0Px8faenLsm3ZJq308fEuKHeuDtXc=', '2019-03-20 19:27:08.773008', 0, '2019-03-20 19:25:37.117576', '2019-03-20 19:26:17.588390', 0, NULL, 'RALLARIA@COLOMBOYMAGLIANO.COM.AR', 'Ramiro', 'Allaria', 1, 1, NULL, 1, '1984-07-10', '27310619863', '31061986', 1, NULL, '2019-03-20 19:26:17.588063', NULL, '2019-03-20 19:26:17.588061', 'Ramiro Allaria', '', '43349511', 1),
(66, 'pbkdf2_sha256$20000$B8chdHayYUG1$4ZTTpBdl8z5PeCp0BnTNF7OTUSimi/YPJOFBBxYrrfY=', NULL, 0, '2019-03-21 11:54:09.534565', '2019-03-21 11:54:09.534626', 0, NULL, 'gabriel_villarruel@hotmail.com', 'Gabriel', 'Villarruel', 0, 1, NULL, 1, '1972-11-25', '30712397930', '92521981', 0, NULL, NULL, NULL, NULL, 'NUEVA SIEMBRA AGROVIAL SRL', '', '03434582455', 1),
(67, 'pbkdf2_sha256$20000$z8SXwu5DX0Ct$/u6MY5COfxqfxgTtV4PE+JoWbE7ZeWfs2iB4QgqNLRw=', '2019-04-03 19:30:07.355227', 0, '2019-03-29 21:24:56.118208', '2019-04-01 19:25:09.309308', 0, NULL, 'salvapicat@hotmail.com', 'Salvador', 'Picat', 1, 1, NULL, 1, '1987-09-03', '30708365803', '33315364', 1, NULL, '2019-04-01 19:25:09.308475', NULL, '2019-04-01 19:25:09.308473', 'La Gallega', '', '3525643614', 1),
(68, 'pbkdf2_sha256$20000$dB1BHm3RKG56$8mMMuXRhZd65A8Z8CbePCneK2MkQKt63HkWZkYaevIw=', NULL, 0, '2019-04-27 22:41:01.505458', '2019-04-27 22:41:01.505515', 0, NULL, 'hegobatlilo1@hotmail.com', 'cirilo baltazar', 'Zaracho', 0, 1, NULL, 1, '1966-01-03', '20174954772', '17495477', 0, NULL, NULL, NULL, NULL, 'Zaracho cirilo baltazar', '', '3773434154', 1),
(69, 'pbkdf2_sha256$20000$M4fxCUxln5PR$gsQyfJbl1qm4wXqt/O2jMTN5rEBB4RDdmhxCgO9hJLs=', NULL, 0, '2019-05-02 13:10:24.103773', '2019-05-02 13:10:24.103829', 0, NULL, 'lprovensal@ruralco.com.ar', 'Lisandro', 'Provensal', 0, 1, NULL, 1, '1976-05-17', '30711873682', '24902143', 0, NULL, NULL, NULL, NULL, 'Ruralco Soluciones s.a.', '', '0341-5271387', 1),
(70, 'pbkdf2_sha256$20000$GY78e7Bv0log$TmAGNDQEkIF22X3av7vgCBwhTa6Br2H7urh3Ypcw8Qg=', NULL, 0, '2019-05-02 18:57:26.203572', '2019-05-02 19:00:03.093309', 0, NULL, 'vicentemartelli@yahoo.com.ar', 'vicente', 'martelli', 1, 1, NULL, 1, '1941-02-12', '20061266268', '6126626', 1, NULL, '2019-05-02 19:00:03.092470', NULL, '2019-05-02 19:00:03.092468', 'vicente martelli', '', '03465 15651922', 1),
(71, 'pbkdf2_sha256$20000$UW1bMtyHm51c$V+R2K6vDma+5zeYtV90okMzkoGgI3/4SJONjO4k6uIc=', '2019-05-10 21:50:51.730022', 0, '2019-05-10 21:49:44.216848', '2019-05-10 21:50:43.515714', 0, NULL, 'finanzas@laemancipacion.com.ar', 'Gaston', 'Iuorno', 1, 1, NULL, 1, '1979-03-23', '33503225099', '27195711', 1, NULL, '2019-05-10 21:50:43.515127', NULL, '2019-05-10 21:50:43.515125', 'LA EMANCIPACION', '', '02924-420303', 1),
(72, 'pbkdf2_sha256$20000$sEYsDlY55ONJ$gK+FdPJuppH/fhnitCp/BHwTV8b+ytu8qh64uZ33FwI=', NULL, 0, '2019-05-15 15:18:53.342987', '2019-05-15 15:18:53.343114', 0, NULL, 'agroasd@gmail.com', 'Juan Pedro', 'Vujassin', 0, 1, NULL, 1, '1987-01-13', '30715236768', '32891477', 0, NULL, NULL, NULL, NULL, 'Agropecuaria Sucesion de Donatela SRL', '', '38212455', 1),
(73, 'pbkdf2_sha256$20000$8Z0DVytAwGr0$5ZbOebI9B5OlXuAHEkdGRAm6qk8Wqd6ckfpuT/DshWM=', '2019-05-15 17:18:42.553449', 0, '2019-05-15 17:18:03.534435', '2019-05-15 17:18:27.270149', 0, NULL, 'nicoacebo@live.com.ar', 'nico', 'acebo', 1, 1, NULL, 1, '1988-04-04', '20334775691', '33477569', 1, NULL, '2019-05-15 17:18:27.269720', NULL, '2019-05-15 17:18:27.269717', '-', '', '-', 1),
(74, 'pbkdf2_sha256$20000$TAaNz1kRroIm$rLM8UGCz7J4aykIDJf4WRy98H79DC2MB9HqANpr348Y=', '2019-05-15 20:07:15.685816', 0, '2019-05-15 20:05:31.854041', '2019-05-15 20:06:56.778405', 0, NULL, 'francoluchino@hotmail.com', 'Franco', 'Luchino', 1, 1, NULL, 1, '1990-04-22', '24351342605', '35134260', 1, NULL, '2019-05-15 20:06:56.777546', NULL, '2019-05-15 20:06:56.777544', 'LUCHINO FRANCO', '', '03584119876', 1),
(75, 'pbkdf2_sha256$20000$ve30UovlLpef$X2BjNKlTmefHe164D7P1dMYnFcTjHEcCLVO2+GqnRvA=', NULL, 0, '2019-05-20 12:06:54.635752', '2019-05-20 12:06:54.635815', 0, NULL, 'fernando@nextagro.co', 'Fernando', 'Vazquez', 0, 1, NULL, 1, '1972-12-28', '30709582565', '23174151', 0, NULL, NULL, NULL, NULL, 'Agropecuaria La Cimarrona SRL', '', '+543515633124', 1),
(76, 'pbkdf2_sha256$20000$Sbg4uihnrVFD$pBjr5qHb7nTJUd2qKM9FnHJVSjmJPz5SbzO8MIe9daY=', '2019-05-20 18:20:24.471535', 0, '2019-05-20 18:19:13.786398', '2019-05-20 18:19:59.152715', 0, NULL, 'ardillon2003@gmail.com', 'Alejandro', 'Busico', 1, 1, NULL, 1, '1980-08-16', '23275502529', '24550252', 1, NULL, '2019-05-20 18:19:59.152426', NULL, '2019-05-20 18:19:59.152423', 'Busico SRL', '', '5493516508444', 1),
(77, 'pbkdf2_sha256$20000$UjN8vXSmvEv3$Gu9Gv+tGhcftue+CQWXORrPv5illrKRvAIerJlVSNsI=', '2019-05-28 11:04:09.233444', 0, '2019-05-28 10:12:08.400759', '2019-05-28 11:03:24.699756', 0, NULL, 'pablolaus@scdplanet.com.ar', 'Pablo', 'Laus', 1, 1, NULL, 1, '1973-04-27', '30714218405', '23091339', 1, NULL, '2019-05-28 11:03:24.699337', NULL, '2019-05-28 11:03:24.699334', 'Agropecuaria Lacor SA', '', '1151828485', 1),
(78, 'pbkdf2_sha256$20000$u5snloMyPmgv$/Cpgf6TPp7xkE1n2WFxUYHRTmCBo+fp3knfvUG5knj8=', '2019-05-28 11:28:18.043329', 0, '2019-05-28 11:24:55.696874', '2019-05-28 11:27:55.303159', 0, NULL, 'apcantoni@hotmail.com', 'Alberto', 'Cantoni', 1, 1, NULL, 1, '1973-06-29', '30677520562', '23538203', 1, NULL, '2019-05-28 11:27:55.302611', NULL, '2019-05-28 11:27:55.302609', 'Juan Carlos Cantoni e Hijos', '', '0346015690655', 1),
(79, 'pbkdf2_sha256$20000$T3702RNM8avp$cUoqPgWCQBaa5V1GzgqwqC9jKMYQPVvp5tII08i0HdA=', '2019-05-28 11:39:30.228337', 0, '2019-05-28 11:38:39.400546', '2019-05-28 11:39:14.389596', 0, NULL, 'mutualascensia@gmail.com', 'Jorge', 'Rodríguez', 1, 1, NULL, 1, '1980-01-02', '30715166727', '27787905', 1, NULL, '2019-05-28 11:39:14.389073', NULL, '2019-05-28 11:39:14.389071', 'asociación mutual Ascensia', '', '03489420811', 1),
(80, 'pbkdf2_sha256$20000$2kJI9Ah3O8aA$HZVyU4av225x04IUS8v4+g7SnMP/Hzy4bWpdlSJmaog=', '2019-05-28 11:53:52.973936', 0, '2019-05-28 11:52:32.166593', '2019-05-28 11:53:28.813617', 0, NULL, 'eherz@decampoacampo.com', 'Emilio', 'Herz', 1, 1, NULL, 1, '1984-04-28', '30526554562', '30978127', 1, NULL, '2019-05-28 11:53:28.812720', NULL, '2019-05-28 11:53:28.812718', 'Pedro Genta y Cia Sa', '', '1132094242', 1),
(81, 'pbkdf2_sha256$20000$XkFJ023NtKGA$IkNduyCPRes9ycS62aAvyGtFV7eVSZ6UfVOEybO9/V4=', '2019-05-28 11:54:56.240144', 0, '2019-05-28 11:53:50.938115', '2019-05-28 11:54:36.161188', 0, NULL, 'loscaldenes2010@gmail.com', 'Miguel angel', 'Cavanna', 1, 1, NULL, 1, '1966-10-07', '20179665035', '17966503', 1, NULL, '2019-05-28 11:54:36.160924', NULL, '2019-05-28 11:54:36.160922', 'Miguel angel cavanna', '', '1153892351', 1),
(82, 'pbkdf2_sha256$20000$j7LGLLmuxo7K$kxvs1yrPDfTIQXXSSIhppU7kI1Sje3X8TsUzEaEJBrQ=', '2019-05-28 11:57:27.394435', 0, '2019-05-28 11:56:05.645540', '2019-05-28 11:57:19.074582', 0, NULL, 'manuel.molejon@gmail.com', 'Manuel', 'Molejon', 1, 1, NULL, 1, '1986-05-16', '30638455508', '32132420', 1, NULL, '2019-05-28 11:57:19.074272', NULL, '2019-05-28 11:57:19.074269', 'VETERINARA SANTA ELENA SRL', '', '1154090033', 1),
(83, 'pbkdf2_sha256$20000$VGCpyK6t6pHk$CtaakMLy/E+X3Y1qrHOHQw2iUKPyoXzHEYFYW7HYNwc=', '2019-05-28 12:52:44.245803', 0, '2019-05-28 12:51:05.570175', '2019-05-28 12:52:24.015253', 0, NULL, 'bursjoaquin@gmail.com', 'Joaquin', 'Burs', 1, 1, NULL, 1, '1992-05-26', '30708776722', '36990124', 1, NULL, '2019-05-28 12:52:24.014741', NULL, '2019-05-28 12:52:24.014739', 'ERRAZURI S.A.', '', '2494699947', 1),
(84, 'pbkdf2_sha256$20000$vI8YrBxRsKBZ$Wx/SpKHFvwo/lcyD3qmm+m9At2HwnwSd553LgaSiwc4=', '2019-05-28 13:54:14.595281', 0, '2019-05-28 13:52:18.586522', '2019-05-28 13:52:53.453836', 0, NULL, 'luisana.pebacini@gmail.com', 'Luisana', 'Pebacini', 1, 1, NULL, 1, '1988-12-31', '27344080866', '34408086', 1, NULL, '2019-05-28 13:52:53.453294', NULL, '2019-05-28 13:52:53.453292', 'Luisana Pebacini', '', '3416224673', 1),
(85, 'pbkdf2_sha256$20000$FITq2zvAvo4j$tvEdKnF1nOZH+Z5BIOX1bxg7Ghuf7DtIl0TZVus5Xv4=', '2019-05-28 14:28:47.240708', 0, '2019-05-28 14:26:05.657676', '2019-05-28 14:28:32.724700', 0, NULL, 'sbustelo@actih.com.ar', 's', 'b', 1, 1, NULL, 1, '1975-03-05', '30714358312', '27225989', 1, NULL, '2019-05-28 14:28:32.723777', NULL, '2019-05-28 14:28:32.723775', 'caa gui', '', '43752395', 1),
(86, 'pbkdf2_sha256$20000$Kxskb9R5gNzZ$eOatch75dipZtiHbGJbpOuOS+x22n2qxr63EwGG6ses=', '2019-05-28 15:15:28.343648', 0, '2019-05-28 15:13:40.064799', '2019-05-28 15:14:51.735075', 0, NULL, 'facundoerviti@gmail.com', 'facundo', 'Erviti', 1, 1, NULL, 1, '1964-02-05', '20171010056', '17101005', 1, NULL, '2019-05-28 15:14:51.734144', NULL, '2019-05-28 15:14:51.734141', 'Erviti Facundo', '', '+5491130047647', 1),
(87, 'pbkdf2_sha256$20000$97ceDqhbF4AT$huJRY/JnrsuqhOHK0I7+Rp4726CDnePl03xzxYCxqK8=', '2019-05-28 15:46:25.183716', 0, '2019-05-28 15:40:43.592874', '2019-05-28 15:41:08.556666', 0, NULL, 'fvarela@ynercia.com.ar', 'Federico', 'Varela', 1, 1, NULL, 1, '1986-08-07', '20325232642', '32523264', 1, NULL, '2019-05-28 15:41:08.555832', NULL, '2019-05-28 15:41:08.555830', 'Ynercia', '', '1167034895', 1),
(88, 'pbkdf2_sha256$20000$XU1jFlC9RoIH$LTolQvMlDB3vDW69bGFQRzXDa/1ZMa0C2plDQwI+rb8=', '2019-05-28 16:53:59.974908', 0, '2019-05-28 16:52:35.289861', '2019-05-28 16:53:23.248937', 0, NULL, 'spuente@pueblemaquinarias.com.ar', 'Sebastian', 'Puente', 1, 1, NULL, 1, '1990-04-18', '30708385960', '34285208', 1, NULL, '2019-05-28 16:53:23.248088', NULL, '2019-05-28 16:53:23.248086', 'PUEBLE S.A.', '', '3815880374', 1),
(89, 'pbkdf2_sha256$20000$3VNqyWaDR4eS$iFXgxaNtIl8LyqFhOnKQstx24vsY/aMXhIFQx9Bpy+A=', '2019-05-28 17:24:03.386671', 0, '2019-05-28 17:22:37.532373', '2019-05-28 17:23:48.234651', 0, NULL, 'manuel_chiappe@hotmail.com', 'manuel', 'chiappe', 1, 1, NULL, 1, '1978-09-15', '30604952774', '26895402', 1, NULL, '2019-05-28 17:23:48.234333', NULL, '2019-05-28 17:23:48.234331', 'RHB', '', '+5491171611435', 1),
(90, 'pbkdf2_sha256$20000$W5i8l4DTMtvs$hjvaS1Rx2z2/CyKhupaAKa2GtZMhjO6CYja3nyfqFDw=', '2019-05-28 17:29:24.373723', 0, '2019-05-28 17:26:27.536486', '2019-05-28 17:28:50.582845', 0, NULL, 'belen@caracaba.com', 'Ana Belen', 'garcia', 1, 1, NULL, 1, '1987-11-30', '27333652639', '33365263', 1, NULL, '2019-05-28 17:28:50.582552', NULL, '2019-05-28 17:28:50.582550', 'Ana Belen Garcis', '', '0352515647053', 1),
(91, 'pbkdf2_sha256$20000$e6IJ9FZNaqbN$ODaspwtwoMLSX2B8G3DwfomHnvkfo4leygPgbOBt8TQ=', '2019-05-28 20:22:32.884375', 0, '2019-05-28 20:21:44.472951', '2019-05-28 20:22:26.680676', 0, NULL, 'lang@sismagro.com', 'Lothaire', 'Lang', 1, 1, NULL, 1, '1987-04-17', '30715980157', '95602756', 1, NULL, '2019-05-28 20:22:26.679847', NULL, '2019-05-28 20:22:26.679845', 'Sismagro SAS', '', '123', 1),
(92, 'pbkdf2_sha256$20000$BHSeJlSdfE8W$fkVtdjK6igia/FLAP0pdP25luakhPr+tD3n1bxhaJUo=', NULL, 0, '2019-05-29 00:40:37.528095', '2019-05-29 00:40:37.528179', 0, NULL, 'martinotero@hotmail.com.ar', 'Martin', 'Otero', 0, 1, NULL, 1, '1978-11-12', '20268311875', '26831187', 0, NULL, NULL, NULL, NULL, 'Martín Román Otero', '', '0344615592128', 1),
(93, 'pbkdf2_sha256$20000$dIRVXt9WTXST$7CrttFrUdPguCVzqsoqzk3k+uzZuNWz5oTxxPZ4D7rQ=', '2019-05-29 01:15:06.583659', 0, '2019-05-29 01:13:02.755433', '2019-05-29 01:14:06.003563', 0, NULL, 'luislanus@gmail.com', 'Luis', 'Lanus', 1, 1, NULL, 1, '1967-11-25', '20182561577', '18256157', 1, NULL, '2019-05-29 01:14:06.003276', NULL, '2019-05-29 01:14:06.003274', 'Luis Lanus', '', '01153690947', 1),
(94, 'pbkdf2_sha256$20000$vl8pNiFbVihZ$IGgmSN+cLs6PYXRwvF8FX2QnANA7xxSxcSGlIow7TNw=', '2019-05-29 02:54:56.410731', 0, '2019-05-29 02:53:57.253024', '2019-05-29 02:54:49.958527', 0, NULL, 'huaglensa@gmail.com', 'Luis', 'Castilla', 1, 1, NULL, 1, '1990-09-17', '30707179216', '35337063', 1, NULL, '2019-05-29 02:54:49.957682', NULL, '2019-05-29 02:54:49.957680', 'Huaglen S.A', '', '01148079540', 1),
(95, 'pbkdf2_sha256$20000$er2quJB0vrKV$OLuDU6YnkLLSEh2Rh9DLWMcbTS8X6UDZ+cz8YSbrtbM=', '2019-05-29 05:36:49.482814', 0, '2019-05-29 05:34:41.460908', '2019-05-29 05:40:13.017765', 0, NULL, 'carlos@mdqtrailers.com.ar', 'Carlos Aldo', 'Giusti', 1, 1, NULL, 95, '1957-05-07', '20127821586', '12782158', 1, NULL, '2019-05-29 05:36:06.116279', NULL, '2019-05-29 05:36:06.116277', 'MDQtrailers', '', '02234828338', 1),
(96, 'pbkdf2_sha256$20000$afevJBu5yBGW$YjI0qe75O/xflIb2eQRsaeTbnW8gxrS6VlkN6EJxlCY=', '2019-05-29 22:06:41.752034', 0, '2019-05-29 07:25:46.107406', '2019-05-29 07:26:29.316995', 0, NULL, 'comercialrom@gmail.com', 'Andres', 'Sposetti', 1, 1, NULL, 1, '1971-09-11', '30597582028', '22430521', 1, NULL, '2019-05-29 07:26:29.316437', NULL, '2019-05-29 07:26:29.316435', 'COMERCIAL ROM SA', '', '+5491157423346', 1),
(97, 'pbkdf2_sha256$20000$Gfy6RjH2zFEd$AFcv3WrSl8xzvw1PlgLwJoS7EndyB7DkMVD39qOSmk0=', '2019-05-29 11:31:04.069453', 0, '2019-05-29 11:29:40.517296', '2019-05-29 11:30:32.404737', 0, NULL, 'miltonrodriguez88@gmail.com', 'Milton', 'Rodriguez Oggero', 1, 1, NULL, 1, '1988-03-19', '20334277918', '33427791', 1, NULL, '2019-05-29 11:30:32.404448', NULL, '2019-05-29 11:30:32.404445', 'Consumidor Final', '', '3492 508861', 1),
(98, 'pbkdf2_sha256$20000$kFVu1RLODqA1$s8kwV8thuLnEnoT7mDuKWdM4MGLwQbPPUguVZCyEvQU=', '2019-06-03 22:34:39.074031', 0, '2019-05-29 14:20:19.172514', '2019-05-29 14:20:40.300179', 0, NULL, 'administracion@euskalagro.net', 'ROBERTO MIGUEL', 'SOUBELET', 1, 1, NULL, 1, '1978-04-16', '20264921636', '26492163', 1, NULL, '2019-05-29 14:20:40.299633', NULL, '2019-05-29 14:20:40.299631', 'EUSKAL AGRO', '', '02342571005', 1),
(99, 'pbkdf2_sha256$20000$rzjL7jOSdUyV$hoDB7fzpbRkASMOoShGQc0rHsczFlte1XDAHpGnrsEA=', '2019-05-30 00:49:31.769855', 0, '2019-05-30 00:40:47.514890', '2019-05-30 00:48:57.240205', 0, NULL, 'eamedina@medinasa.com.ar', 'Elvio', 'Medina', 1, 1, NULL, 1, '1984-11-24', '30716122510', '31209713', 1, NULL, '2019-05-30 00:48:57.239907', NULL, '2019-05-30 00:48:57.239905', 'MMA AGRO S.A.S.', '', '02355422325', 1),
(100, 'pbkdf2_sha256$20000$o2hTjhGUeUaj$yfXSwaj0pF3FHKFxhp39i/TR3V+aNBbdjj0EpG04w88=', NULL, 0, '2019-05-30 03:17:54.564195', '2019-05-30 03:17:54.564249', 0, NULL, 'dfsdfdfs@adfsdf.com', 'sdasd', 'asdas', 0, 1, NULL, 1, '2019-05-08', '20317359692', '23423432', 0, NULL, NULL, NULL, NULL, 'asdasd', '', '34234', 1),
(101, 'pbkdf2_sha256$20000$vCOzZJPnfYxm$8Awm7ZSyxs0cgfTz6hFcsstIWAksvJ0e4iVymV6qW3k=', '2019-06-19 14:06:46.011341', 0, '2019-05-30 11:28:06.781966', '2019-05-30 11:28:54.295617', 0, NULL, 'fitoreumann@hotmail.com', 'Adolfo', 'Reumann', 1, 1, NULL, 1, '1979-07-01', '30709825328', '27386178', 1, NULL, '2019-05-30 11:28:54.294803', NULL, '2019-05-30 11:28:54.294801', 'Pampa Norte S.A', '', '2302663015', 1),
(102, 'pbkdf2_sha256$20000$vPGx4WaWHOOr$zb1WOH7qp7LkY3/ctoi/jA2/fNnsIzwwC0oGn4PM29o=', '2019-05-31 13:30:30.913413', 0, '2019-05-31 13:29:13.617357', '2019-05-31 13:30:10.958506', 0, NULL, 'ignacio_bruno@hotmail.com', 'Ignacio', 'Bruno', 1, 1, NULL, 1, '1992-02-14', '20366802550', '36680255', 1, NULL, '2019-05-31 13:30:10.957679', NULL, '2019-05-31 13:30:10.957677', 'IGNACIO BRUNO', '', '3564682690', 1),
(103, 'pbkdf2_sha256$20000$fL6NMkqounoM$/4YVmcJHLAfMmIs1RaTHhMp+1bUc6YqgCwkkEZr3O8g=', '2019-05-31 15:52:51.163690', 0, '2019-05-31 15:51:05.067491', '2019-05-31 15:52:12.928131', 0, NULL, 'mdelafuente@campocrop.com.ar', 'manuel', 'de la fuente', 1, 1, NULL, 1, '1982-12-17', '30709360759', '29959312', 1, NULL, '2019-05-31 15:52:12.927272', NULL, '2019-05-31 15:52:12.927270', 'Campo Crop SA', '', '1151976655', 1),
(104, 'pbkdf2_sha256$20000$XTXzKwI5rUs7$7KDJ5jqkNvedtfvC559fyXULAnbfyUsTDKK1jbX9xT0=', '2019-06-01 12:16:05.174484', 0, '2019-06-01 12:13:36.225945', '2019-06-01 12:14:34.697163', 0, NULL, 'nicolas.sanchez@sanchezagronegocios.com.ar', 'NICOLAS', 'SANCHEZ', 1, 1, NULL, 1, '2019-01-09', '30708997354', '34052077', 1, NULL, '2019-06-01 12:14:34.696823', NULL, '2019-06-01 12:14:34.696821', 'SANCHEZ AGRONEGOCIOS SA', '', '2657248675', 1),
(105, 'pbkdf2_sha256$20000$N1btZ2MVd7ZJ$Er3t9QkVR2Si/LhaJ1thwO2wcN++n3SwPQWx+H5VRA4=', '2019-06-01 23:14:42.870000', 0, '2019-06-01 23:08:42.660181', '2019-06-01 23:14:34.733016', 0, NULL, 'stockpf@gmail.com', 'Carlos', 'Nielsen', 1, 1, NULL, 1, '1981-02-24', '30708271868', '28682669', 1, NULL, '2019-06-01 23:14:34.732488', NULL, '2019-06-01 23:14:34.732486', 'Elias semillas e insumos srl', '', '+5492983550339', 1),
(106, 'pbkdf2_sha256$20000$R7A1gukefEl3$/BUbaShE24Ga8Dh6p+prb1UaMqWgswoQlXoFevf/90w=', '2019-07-04 20:44:59.969525', 0, '2019-06-03 14:31:49.153259', '2019-07-04 20:44:41.620989', 0, NULL, 'cordajuan@gmail.com', 'Juan Eduardo', 'Corda', 1, 1, NULL, 106, '1985-12-10', '30623236338', '31958444', 1, NULL, '2019-06-03 14:32:59.217797', NULL, '2019-06-03 14:32:59.217794', 'LA CAMINERA S.A.', '', '0223-154478268', 1),
(107, 'pbkdf2_sha256$20000$S6aAlI931cbM$Nm++l0P33P5c7lYKWAxX8ezW2SAUR+oIIX/WDW+NVjE=', '2019-06-04 12:25:56.236294', 0, '2019-06-03 20:29:46.520563', '2019-06-04 12:25:44.016915', 0, NULL, 'l.dorbessan@f2lfertilizantes.com', 'lisandro', 'dorbessan', 1, 1, NULL, 1, '1967-12-17', '30710783027', '18627256', 1, NULL, '2019-06-04 12:25:44.016572', NULL, '2019-06-04 12:25:44.016570', 'F2L Fertilizantes SRL', '', '0111567679976', 1),
(108, 'pbkdf2_sha256$20000$ILDIW2rnMDys$QpGyyn0WWi60+cXzOxe2rINvdU2C3T6K23S54s4sL0Q=', '2019-06-03 22:49:33.952940', 0, '2019-06-03 21:52:09.323633', '2019-06-03 22:49:11.082444', 0, NULL, 'pampaagronomia@hotmail.com', 'Ruben Marcelo', 'Marcos', 1, 1, NULL, 1, '1966-05-03', '23174707499', '35235922', 1, NULL, '2019-06-03 22:49:11.082138', NULL, '2019-06-03 22:49:11.082135', 'Marcos Ruben Marcelo', '', '2302631762', 1),
(109, 'pbkdf2_sha256$20000$HDZ16Fah0l0M$ii4MZkVLXf8BFs09nicfqSTf3AsLaVus00BOTU25SzM=', '2019-07-16 14:41:49.040261', 0, '2019-06-06 19:52:38.536724', '2019-06-06 19:53:44.685324', 0, NULL, 'mauro.santiago@leveal.com', 'MAURO', 'SANTIAGO', 1, 1, NULL, 1, '1983-04-05', '30664985469', '29694624', 1, NULL, '2019-06-06 19:53:44.684869', NULL, '2019-06-06 19:53:44.684867', 'LEVEAL S.A', '', '02273-442427', 1),
(110, 'pbkdf2_sha256$20000$4TvBOyJmx5BE$rniALz+FTVtZKtBYTVWmBmsV7H5CYl2WbjV7VnE3CQc=', '2019-06-10 21:38:21.666826', 0, '2019-06-10 21:36:47.448923', '2019-06-10 21:38:00.841605', 0, NULL, 'inipopinipop@gmail.com', 'Eugenio', 'Balna', 1, 1, NULL, 1, '1980-09-19', '30710969619', '25600101', 1, NULL, '2019-06-10 21:38:00.841258', NULL, '2019-06-10 21:38:00.841256', 'Monotrinutista', '', '1542587945', 1),
(111, 'pbkdf2_sha256$20000$DvZBQ4XUIKTd$rNUngVnmffDKXSLdCZamrV+9O+RVmVb9GJAps+2gM8g=', '2019-06-22 20:01:26.041729', 0, '2019-06-12 12:24:24.898314', '2019-06-12 12:24:47.511424', 0, NULL, 'gmercau1966@gmail.com', 'Gustavo', 'Mercau', 1, 1, NULL, 1, '1966-05-25', '30707904220', '17942251', 1, NULL, '2019-06-12 12:24:47.509888', NULL, '2019-06-12 12:24:47.509886', 'Have a Good day Marketing S.A', '', '0111556661442', 1),
(112, 'pbkdf2_sha256$20000$ARhPyQD3SRPS$OPFO9dPRSJ92V66lhDvDD3pSNfD1HAmO0A6F42Rrwmo=', '2019-06-19 19:47:27.856017', 0, '2019-06-19 19:46:35.024349', '2019-06-19 19:47:02.516838', 0, NULL, 'julia.oneto@villanueva.com.ar', 'Julia', 'Oneto', 1, 1, NULL, 1, '1988-06-21', '30557913463', '33387982', 1, NULL, '2019-06-19 19:47:02.515977', NULL, '2019-06-19 19:47:02.515975', 'Villa Nueva S.A', '', '3536571018', 1),
(113, 'pbkdf2_sha256$20000$nISWgvyULzjk$3LxQ/lYCcRb+ItK7qzrBweNXhaPtOMh3O0AniPoXXrM=', '2019-08-26 18:43:42.926692', 0, '2019-06-24 14:35:14.082518', '2019-07-15 14:16:29.629460', 0, NULL, 'joseluis.iudica@arpov.org.ar', 'José Luis', 'Iudica', 1, 1, NULL, 113, '1961-01-17', '33645022659', '14446298', 1, NULL, '2019-06-24 14:35:57.517088', NULL, '2019-06-24 14:35:57.517086', 'Asociación Argentina de Protección de las Obtenciones Vegetales (ArPOV) A.C.', '', '+5491169541269', 1),
(114, 'pbkdf2_sha256$20000$EzNU49f3ndOq$xNugDMXUVgd+EL9w6wE27iPUtW5w2vLW2R+zvl3NmaM=', '2019-06-28 15:18:00.143271', 0, '2019-06-28 15:10:07.130344', '2019-06-28 15:11:05.571681', 0, NULL, 'javier.niedfeld@altagenetics.com', 'Javier', 'Niedfeld', 1, 1, NULL, 1, '1973-09-11', '30621745693', '23497395', 1, NULL, '2019-06-28 15:11:05.571418', NULL, '2019-06-28 15:11:05.571416', 'CIALE SOCIEDAD ANONIMA', '', '02474 15554637', 1),
(115, 'pbkdf2_sha256$20000$EhLQ8DM6VtXn$fUkwuRiXLt7LzcFFhPICgllHbU2U5P6fIHPJFVRa14Q=', '2019-07-23 13:03:47.888700', 0, '2019-07-03 13:47:46.581687', '2019-07-03 13:48:38.043396', 0, NULL, 'enieto@bancor.com.ar', 'Eleonora', 'Nieto', 1, 1, NULL, 1, '1972-09-14', '27227930182', '22793018', 1, NULL, '2019-07-03 13:48:38.042297', NULL, '2019-07-03 13:48:38.042294', 'Bancor', '', '3514205695', 1),
(116, 'pbkdf2_sha256$20000$UjyHiHBJR6yN$+95OitfLClLKZ9Fe0iT7F6vKNRNlaBCW3XmE5Qb+MI8=', '2019-07-10 18:06:09.611572', 0, '2019-07-10 18:04:55.441714', '2019-07-10 18:05:53.313245', 0, NULL, 'mariaines@ebayacasal.com.ar', 'Maria Ines', 'Baya Casal', 1, 1, NULL, 1, '2019-04-16', '30607413254', '36872976', 1, NULL, '2019-07-10 18:05:53.312985', NULL, '2019-07-10 18:05:53.312983', 'Enrique M. Baya Casal S.A.', '', '45478200', 1),
(117, 'pbkdf2_sha256$20000$ZI9lLFyZEaAM$37XnQAXRRwiKU/vSL5tUX8SL1H8/k5u2LrdSzny40rM=', '2019-07-12 15:42:52.549355', 0, '2019-07-11 19:53:14.697285', '2019-07-11 19:53:39.581821', 0, NULL, 'nacebo@lartirigoyen.com', 'Ignacio', 'Lartirigoyen', 1, 1, NULL, 1, '1954-08-26', '30613985995', '11266067', 1, NULL, '2019-07-11 19:53:39.580947', NULL, '2019-07-11 19:53:39.580945', 'LARTIRIGOYEN Y CIA. S.A.', '', '02954491481', 1),
(118, 'pbkdf2_sha256$20000$qBpITTTPoPho$AR0sXyl66oKY3V7DWIzg8pSuNaFfjTov3hVHIbilv7Q=', '2019-07-22 20:07:57.615291', 0, '2019-07-18 21:51:58.825484', '2019-07-18 21:52:26.701285', 0, NULL, 'alex_fourcade95@hotmail.com', 'Alexia', 'Fourcade', 1, 1, NULL, 1, '1995-05-29', '27390685861', '39068586', 1, NULL, '2019-07-18 21:52:26.700436', NULL, '2019-07-18 21:52:26.700434', 'xxxx', '', '1132135560', 1),
(119, 'pbkdf2_sha256$20000$zanWJ9DTHhNf$22vvjcNE4HlMqEYfvdQjV44DfBQtlNxoFMVRv8Pzz3o=', '2019-07-19 18:18:31.950462', 0, '2019-07-19 18:16:58.769095', '2019-07-19 18:22:57.954140', 0, NULL, 'agricola.azul@speedy.com.ar', 'Sebastian', 'Costa', 1, 1, NULL, 119, '1972-06-20', '30665180952', '22738791', 1, NULL, '2019-07-19 18:18:20.614002', NULL, '2019-07-19 18:18:20.614000', 'Agricola Azul SA', '', '02281432780', 1),
(120, 'pbkdf2_sha256$20000$E4ztvJtb6beP$BD3Qytb+nDtv75IIGRXMweBNyF8O7bZg12oO4RbLzoM=', '2019-07-22 21:01:32.139584', 0, '2019-07-22 20:47:07.568618', '2019-07-22 21:01:25.882261', 0, NULL, 'amperbat@arnet.com.ar', 'JUAN', 'AGUILAR', 1, 1, NULL, 1, '1951-10-11', '20085558073', '08555807', 1, NULL, '2019-07-22 21:01:25.882007', NULL, '2019-07-22 21:01:25.882005', 'AGUILAR JUAN JOSE', '', '3874515065', 1),
(121, 'pbkdf2_sha256$20000$9a1iABQNT59X$yGX4pnzITQ6O/y133IeqHqNvU58ZyUiENUIZJoJgRkk=', '2019-08-07 14:56:22.133591', 0, '2019-08-07 14:53:08.368899', '2019-08-07 14:55:47.256128', 0, NULL, 'arielavila0786@gmail.com', 'Ariel', 'Avila', 1, 1, NULL, 1, '1970-03-29', '20215317863', '21531786', 1, NULL, '2019-08-07 14:55:47.255901', NULL, '2019-08-07 14:55:47.255899', 'Ariel Avila', '', '3412023323', 1),
(122, 'pbkdf2_sha256$20000$UQP0lVo8KnkL$GRScs9GRk0DcG/PYM88NQAWlDfoY+DfsjdcspSaHL4o=', '2019-08-13 17:08:30.508279', 0, '2019-08-13 17:07:46.960886', '2019-08-13 17:08:09.575716', 0, NULL, 'carloskelly22@hotmail.com', 'Carlos', 'Kelly', 1, 1, NULL, 1, '1984-03-23', '30527954955', '30396677', 1, NULL, '2019-08-13 17:08:09.575156', NULL, '2019-08-13 17:08:09.575154', 'Cañada Honda S.A', '', '2355510303', 1),
(123, 'pbkdf2_sha256$20000$DDPlHW4SkPI8$Zughly28C3wlAyAbDxXb9uSYqs9LCF3geKsuBe6vjAw=', '2019-08-14 15:38:05.130433', 0, '2019-08-14 15:37:25.632107', '2019-08-14 15:37:58.931723', 0, NULL, 'belen@colomboycolombo.com.ar', 'Belen', 'Lehsmann', 1, 1, NULL, 1, '1989-09-04', '30654138725', '34651375', 1, NULL, '2019-08-14 15:37:58.931186', NULL, '2019-08-14 15:37:58.931184', 'Colombo y Colombo SA', '', '1125583105', 1),
(124, 'pbkdf2_sha256$20000$YD2EKHgloiMC$UlpBAX5ZBL1iiHN6KjszBsMlvCV4rp1y42rLsam+Rh4=', '2019-08-14 20:37:44.773024', 0, '2019-08-14 20:32:53.566719', '2019-08-14 20:33:11.009642', 0, NULL, 'stephmass2000@gmail.com', 'veronica', 'andrews', 1, 1, NULL, 1, '2019-08-09', '27296567324', '27296567', 1, NULL, '2019-08-14 20:33:11.008771', NULL, '2019-08-14 20:33:11.008769', 'steph', '', '9046015373', 1),
(125, 'pbkdf2_sha256$20000$ZpbRB48Kokhm$/b0rDYi/OMTNM9P/OTZeIdWkJ0Tz8mviWgX7nmNALak=', '2019-08-27 15:22:06.424031', 0, '2019-08-27 15:21:46.364269', '2019-08-27 15:22:00.545820', 0, NULL, 'draspo@paseshow.com.ar', 'Diego', 'Raspo', 1, 1, NULL, 1, '1972-04-21', '30715344153', '22698063', 1, NULL, '2019-08-27 15:22:00.545212', NULL, '2019-08-27 15:22:00.545210', 'itbusiness', '', '005493516198788', 1),
(126, 'pbkdf2_sha256$20000$g833UADKNabB$lva2+x/mZzt1AyCVe/a57aT/IaiJI1VFLT++M7FXiAc=', '2019-08-29 14:54:34.026615', 0, '2019-08-28 16:04:30.620977', '2019-08-28 16:06:01.913819', 0, NULL, 'gabytag@gmail.com', 'Gabriela', 'Barrale', 1, 1, NULL, 1, '1972-02-01', '27225638417', '22563841', 1, NULL, '2019-08-28 16:06:01.913518', NULL, '2019-08-28 16:06:01.913516', 'Consignaciones Cordoba S.A.', '', '3512839679', 1),
(127, 'pbkdf2_sha256$20000$B0KcRILdt9tw$Mk4x8Tz3V1hrymZimEYZwqBGwNYRpcs4LiAyDfyckLM=', NULL, 0, '2019-09-03 22:52:50.458311', '2019-09-03 23:03:37.946886', 0, NULL, 'ignacio@datar-id.com.ar', 'Ignacio', 'Martin', 1, 1, NULL, 1, '1983-04-22', '20301255919', '30125591', 1, NULL, '2019-09-03 23:03:37.946207', NULL, '2019-09-03 23:03:37.946203', 'Ignacio Martin', '', '3513284036', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `security_user_groups`
--

CREATE TABLE `security_user_groups` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `security_user_user_permissions`
--

CREATE TABLE `security_user_user_permissions` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `transactions`
-- (Véase abajo para la vista actual)
--
CREATE TABLE `transactions` (
`id` int(11)
,`operation_type` varchar(13)
,`deb_cred` varchar(1)
,`uuid` char(32)
,`code` varchar(50)
,`user_id` int(11)
,`user` varchar(302)
,`date` datetime(6)
,`description` longtext
,`status` char(15)
,`target_id` int(11)
,`target` varchar(302)
,`amount` double
);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `transaction_credit_card`
--

CREATE TABLE `transaction_credit_card` (
  `id` int(11) NOT NULL,
  `credit_card_id` int(11) NOT NULL,
  `token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `token_agro` varchar(10) COLLATE utf8_unicode_ci DEFAULT NULL,
  `owner_name` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `bin` int(6) DEFAULT NULL,
  `last_four_digits` int(4) DEFAULT NULL,
  `expiration_month` int(2) DEFAULT NULL,
  `expiration_year` int(4) DEFAULT NULL,
  `amount` double NOT NULL,
  `amount_to_conciliate` double NOT NULL DEFAULT '0',
  `amount_conciliated` double NOT NULL DEFAULT '0',
  `process_date` date DEFAULT NULL,
  `aut` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cupon` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `observations` longtext COLLATE utf8_unicode_ci,
  `status` char(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'PENDING',
  `conciliation_date` datetime(6) DEFAULT NULL,
  `transaction_code` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `when_created` datetime(6) NOT NULL,
  `who_created_id` int(11) DEFAULT NULL,
  `when_last_edit` datetime(6) NOT NULL,
  `who_last_edit_id` int(11) DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  `when_deleted` datetime(6) DEFAULT NULL,
  `who_deleted_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `transaction_credit_card`
--

INSERT INTO `transaction_credit_card` (`id`, `credit_card_id`, `token`, `token_agro`, `owner_name`, `bin`, `last_four_digits`, `expiration_month`, `expiration_year`, `amount`, `amount_to_conciliate`, `amount_conciliated`, `process_date`, `aut`, `cupon`, `observations`, `status`, `conciliation_date`, `transaction_code`, `when_created`, `who_created_id`, `when_last_edit`, `who_last_edit_id`, `is_deleted`, `when_deleted`, `who_deleted_id`) VALUES
(2, 2, '7d5ad361-b28e-4ca2-a41e-26af13484b29', '123456', 'raul', 476590, 1873, 10, 25, 3000, 2854.8, 2854.8, NULL, '102421', '8083600', '', 'CONCILIATED', '2019-09-05 00:00:00.000000', NULL, '2019-09-05 13:24:13.968524', NULL, '2019-09-05 13:24:20.161838', NULL, 0, NULL, NULL);

--
-- Disparadores `transaction_credit_card`
--
DELIMITER $$
CREATE TRIGGER `transaction_credit_card_update` AFTER UPDATE ON `transaction_credit_card` FOR EACH ROW BEGIN
		IF NEW.status = 'CONCILIATED' THEN
			SELECT payment_id
			  INTO @PAYMENT_ID
			  FROM transaction_values
			 WHERE id = NEW.id;
		
			# Update transaction_credit_card
			UPDATE wallet_balance wb
				join transaction_values ov
				  on ov.id = wb.value_id
			   SET wb.status = 'AVAILABLE'
			 WHERE ov.payment_id = @PAYMENT_ID;
             
			# Update payment order
			UPDATE payment_order po
				join payment p
				  on po.id = p.payment_order_id
			   SET po.when_paid = NEW.conciliation_date,
                   po.status = 'PAID'
			 WHERE p.id = @PAYMENT_ID;

			# Update payment order
			UPDATE payment p
			   SET p.status = 'PAID'
			 WHERE p.id = @PAYMENT_ID;

		END IF;
         
	END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `transaction_values`
--

CREATE TABLE `transaction_values` (
  `id` int(11) NOT NULL,
  `operation_type` char(20) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `afected_id` int(11) NOT NULL,
  `status` char(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'PENDING',
  `value_type` char(15) COLLATE utf8_unicode_ci NOT NULL,
  `currency` char(4) COLLATE utf8_unicode_ci NOT NULL,
  `amount` double NOT NULL,
  `payment_id` int(11) DEFAULT NULL,
  `transfer_id` int(11) DEFAULT NULL,
  `checkout_id` int(11) DEFAULT NULL,
  `when_created` datetime(6) NOT NULL,
  `who_created_id` int(11) DEFAULT NULL,
  `when_last_edit` datetime(6) NOT NULL,
  `who_last_edit_id` int(11) DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  `when_deleted` datetime(6) DEFAULT NULL,
  `who_deleted_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `transaction_values`
--

INSERT INTO `transaction_values` (`id`, `operation_type`, `user_id`, `afected_id`, `status`, `value_type`, `currency`, `amount`, `payment_id`, `transfer_id`, `checkout_id`, `when_created`, `who_created_id`, `when_last_edit`, `who_last_edit_id`, `is_deleted`, `when_deleted`, `who_deleted_id`) VALUES
(1, 'FEE', 59, 60, 'PENDING', 'VIRTUAL_CASH', 'ARS', 108.9, 1, NULL, NULL, '2019-09-05 13:24:13.927438', 60, '2019-09-05 13:24:13.927762', 60, 0, NULL, NULL),
(2, 'PAYMENT', 60, 59, 'PENDING', 'CREDIT_CARD', 'ARS', 3000, 1, NULL, NULL, '2019-09-05 13:24:13.945817', 60, '2019-09-05 13:24:13.945989', 60, 0, NULL, NULL),
(3, 'CHECKOUT', 59, 59, 'PENDING', 'VIRTUAL_CASH', 'ARS', 2000, NULL, NULL, 1, '2019-09-05 13:28:57.463346', 59, '2019-09-05 13:28:57.463517', 59, 0, NULL, NULL);

--
-- Disparadores `transaction_values`
--
DELIMITER $$
CREATE TRIGGER `transaction_values_wallet` AFTER INSERT ON `transaction_values` FOR EACH ROW BEGIN
		# Insert record on wallet in case it doesn't exist
		INSERT INTO wallet (user_id, value_type, currency)
		SELECT NEW.user_id, vt.value_type, vt.currency
          FROM value_types vt
		 WHERE NOT EXISTS (select * 
							 from wallet w
							where w.user_id = NEW.user_id
							  and w.value_type = vt.value_type
							  and w.currency = vt.currency);
                              
		INSERT INTO wallet (user_id, value_type, currency)
		SELECT NEW.afected_id, vt.value_type, vt.currency
          FROM value_types vt
		 WHERE NOT EXISTS (select * 
							 from wallet w
							where w.user_id = NEW.afected_id
							  and w.value_type = vt.value_type
							  and w.currency = vt.currency);
                              
		SET @OPERATION_DESC = CASE
								WHEN NEW.operation_type = 'PAYMENT'
									THEN 'Pago'
								WHEN NEW.operation_type = 'TRANSFER'
									THEN 'Transferencia'
								WHEN NEW.operation_type = 'CHECKOUT'
									THEN 'Retiro'
								ELSE ''
			 			     END;
                                    

		IF NEW.value_type = 'CREDIT_CARD' AND NEW.operation_type IN ('PAYMENT', 'TRANSFER') THEN
			INSERT INTO wallet_balance (user_id, value_type, currency, date, description, cred_deb, status, amount, value_id)
            VALUES (NEW.user_id, "VIRTUAL_CASH", NEW.currency, NEW.when_created, "Ingreso dinero", "C", 'IN_PROCESS', NEW.amount, NEW.id);

			INSERT INTO wallet_balance (user_id, value_type, currency, date, description, cred_deb, status, amount, value_id)
            VALUES (NEW.user_id, "VIRTUAL_CASH", NEW.currency, NEW.when_created, CONCAT(@OPERATION_DESC, " a"), "D", 'IN_PROCESS', NEW.amount, NEW.id);

			INSERT INTO wallet_balance (user_id, value_type, currency, date, description, cred_deb, status, amount, value_id)
            VALUES (NEW.afected_id, "VIRTUAL_CASH", NEW.currency, NEW.when_created, CONCAT(@OPERATION_DESC, " de"), "C", 'IN_PROCESS', NEW.amount, NEW.id);
            
        END IF;
        
    	IF NEW.value_type = 'VIRTUAL_CASH' AND NEW.operation_type IN ('PAYMENT', 'TRANSFER') THEN
			INSERT INTO wallet_balance (user_id, value_type, currency, date, description, cred_deb, status, amount, value_id)
            VALUES (NEW.user_id, "VIRTUAL_CASH", NEW.currency, NEW.when_created, CONCAT(@OPERATION_DESC, " a"), "D", 'AVAILABLE', NEW.amount, NEW.id);
            
			INSERT INTO wallet_balance (user_id, value_type, currency, date, description, cred_deb, status, amount, value_id)
			VALUES (NEW.afected_id, "VIRTUAL_CASH", NEW.currency, NEW.when_created, CONCAT(@OPERATION_DESC, " de"), "C", 'AVAILABLE', NEW.amount, NEW.id);
        END IF;

    	IF NEW.value_type = 'VIRTUAL_CASH' AND NEW.operation_type IN ('CHECKOUT') THEN
			INSERT INTO wallet_balance (user_id, value_type, currency, date, description, cred_deb, status, amount, value_id)
            VALUES (NEW.user_id, "VIRTUAL_CASH", NEW.currency, NEW.when_created, @OPERATION_DESC, "D", 'AVAILABLE', NEW.amount, NEW.id);
        END IF;

		IF NEW.operation_type = 'FEE' THEN
			# Insert record on wallet in case it doesn't exist
			INSERT INTO wallet_balance (user_id, value_type, currency, date, description, cred_deb, status, amount, value_id)
            VALUES (NEW.user_id, "VIRTUAL_CASH", NEW.currency, NEW.when_created, "Comisiones", "D", 'IN_PROCESS', NEW.amount, NEW.id);
        END IF;
        
	END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `transfer`
--

CREATE TABLE `transfer` (
  `id` int(11) NOT NULL,
  `uuid` char(32) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `date` datetime(6) NOT NULL,
  `status` char(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'TRANSFERRED',
  `sender_id` int(11) NOT NULL,
  `receiver_email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `receiver_id` int(11) DEFAULT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `amount` double NOT NULL,
  `amount_to_transfer` double DEFAULT NULL,
  `feeds` json DEFAULT NULL,
  `validation_code` int(6) NOT NULL,
  `when_created` datetime(6) NOT NULL,
  `who_created_id` int(11) DEFAULT NULL,
  `when_last_edit` datetime(6) NOT NULL,
  `who_last_edit_id` int(11) DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  `when_deleted` datetime(6) DEFAULT NULL,
  `who_deleted_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user_bank_account`
--

CREATE TABLE `user_bank_account` (
  `id` int(11) NOT NULL,
  `bank_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `account_number` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `cbu` varchar(22) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `valid_account` tinyint(1) NOT NULL,
  `when_valid_account` datetime(6) DEFAULT NULL,
  `who_valid_account_id` int(11) DEFAULT NULL,
  `when_created` datetime(6) NOT NULL,
  `who_created_id` int(11) DEFAULT NULL,
  `when_last_edit` datetime(6) NOT NULL,
  `who_last_edit_id` int(11) DEFAULT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  `when_deleted` datetime(6) DEFAULT NULL,
  `who_deleted_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `user_bank_account`
--

INSERT INTO `user_bank_account` (`id`, `bank_id`, `user_id`, `account_number`, `cbu`, `alias`, `valid_account`, `when_valid_account`, `who_valid_account_id`, `when_created`, `who_created_id`, `when_last_edit`, `who_last_edit_id`, `is_deleted`, `when_deleted`, `who_deleted_id`) VALUES
(1, 15, 61, '2282882', '1234567890123456789012', 'asnda.casda.c', 1, NULL, NULL, '2019-09-03 14:31:54.510273', 61, '2019-09-03 14:31:54.510336', 61, 0, NULL, NULL),
(2, 15, 127, '1231231', '123123123123', 'asd.as.sa.', 1, NULL, NULL, '2019-09-03 23:43:23.062548', 127, '2019-09-03 23:43:23.062613', 127, 0, NULL, NULL),
(3, 1, 59, '123456789', '0170231820000000010500', 'alias01', 1, NULL, NULL, '2019-09-05 13:27:40.159842', 59, '2019-09-05 13:27:40.159998', 59, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `validation_codes`
--

CREATE TABLE `validation_codes` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `code` int(6) NOT NULL,
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `validation_codes`
--

INSERT INTO `validation_codes` (`id`, `user_id`, `code`, `created`) VALUES
(3, 127, 3293, '2019-09-04 00:24:43'),
(4, 59, 9587, '2019-09-05 13:28:35');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `value_types`
--

CREATE TABLE `value_types` (
  `value_type` char(15) COLLATE utf8_unicode_ci NOT NULL,
  `currency` char(4) COLLATE utf8_unicode_ci NOT NULL,
  `hidden` tinyint(1) NOT NULL DEFAULT '1',
  `description` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `value_types`
--

INSERT INTO `value_types` (`value_type`, `currency`, `hidden`, `description`) VALUES
('CREDIT_CARD', 'ARS', 1, 'TARJETA DE CREDITO EN PESOS'),
('VIRTUAL_CASH', 'ARS', 0, 'VIRTUAL CASH EN PESOS');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wallet`
--

CREATE TABLE `wallet` (
  `user_id` int(11) NOT NULL,
  `value_type` char(15) COLLATE utf8_unicode_ci NOT NULL,
  `currency` char(4) COLLATE utf8_unicode_ci NOT NULL,
  `amount_inprocess` double NOT NULL DEFAULT '0',
  `amount_available` double NOT NULL DEFAULT '0',
  `amount_canceled` double NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `wallet`
--

INSERT INTO `wallet` (`user_id`, `value_type`, `currency`, `amount_inprocess`, `amount_available`, `amount_canceled`) VALUES
(59, 'CREDIT_CARD', 'ARS', 0, 0, 0),
(59, 'VIRTUAL_CASH', 'ARS', 0, 891.0999999999999, 0),
(60, 'CREDIT_CARD', 'ARS', 0, 0, 0),
(60, 'VIRTUAL_CASH', 'ARS', 0, 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `wallet_balance`
--

CREATE TABLE `wallet_balance` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `value_type` char(15) COLLATE utf8_unicode_ci NOT NULL,
  `currency` char(4) COLLATE utf8_unicode_ci NOT NULL,
  `date` datetime(6) NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `cred_deb` char(1) COLLATE utf8_unicode_ci NOT NULL,
  `value_id` int(11) DEFAULT NULL,
  `amount` double NOT NULL,
  `status` char(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'IN_PROCESS'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `wallet_balance`
--

INSERT INTO `wallet_balance` (`id`, `user_id`, `value_type`, `currency`, `date`, `description`, `cred_deb`, `value_id`, `amount`, `status`) VALUES
(1, 59, 'VIRTUAL_CASH', 'ARS', '2019-09-05 13:24:13.927438', 'Comisiones', 'D', 1, 108.9, 'AVAILABLE'),
(2, 60, 'VIRTUAL_CASH', 'ARS', '2019-09-05 13:24:13.945817', 'Ingreso dinero', 'C', 2, 3000, 'AVAILABLE'),
(3, 60, 'VIRTUAL_CASH', 'ARS', '2019-09-05 13:24:13.945817', 'Pago a', 'D', 2, 3000, 'AVAILABLE'),
(4, 59, 'VIRTUAL_CASH', 'ARS', '2019-09-05 13:24:13.945817', 'Pago de', 'C', 2, 3000, 'AVAILABLE'),
(5, 59, 'VIRTUAL_CASH', 'ARS', '2019-09-05 13:28:57.463346', 'Retiro', 'D', 3, 2000, 'AVAILABLE');

--
-- Disparadores `wallet_balance`
--
DELIMITER $$
CREATE TRIGGER `wallet_balance_wallet_insert` AFTER INSERT ON `wallet_balance` FOR EACH ROW BEGIN
		# Update wallet 
        UPDATE wallet
           SET amount_inprocess = CASE
									WHEN NEW.status = 'IN_PROCESS' AND NEW.cred_deb = 'C' THEN amount_inprocess + NEW.amount
									WHEN NEW.status = 'IN_PROCESS' AND NEW.cred_deb = 'D' THEN amount_inprocess - NEW.amount
									ELSE amount_inprocess
								  END,
			   amount_available = CASE
									WHEN NEW.status = 'AVAILABLE' AND NEW.cred_deb = 'C' THEN amount_available + NEW.amount
									WHEN NEW.status = 'AVAILABLE' AND NEW.cred_deb = 'D' THEN amount_available - NEW.amount
									ELSE amount_available
								  END,
			   amount_canceled = CASE
									WHEN NEW.status = 'CANCELED' AND NEW.cred_deb = 'C' THEN amount_canceled + NEW.amount
									WHEN NEW.status = 'CANCELED' AND NEW.cred_deb = 'D' THEN amount_canceled - NEW.amount
									ELSE amount_canceled
								  END
		 WHERE user_id = NEW.user_id
           AND value_type = NEW.value_type
           AND currency = NEW.currency;
	
	END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `wallet_balance_wallet_update` AFTER UPDATE ON `wallet_balance` FOR EACH ROW BEGIN
		# Update wallet 
        UPDATE wallet
           SET amount_inprocess = CASE
									WHEN OLD.status = 'IN_PROCESS' AND NEW.status IN ('AVAILABLE', 'CANCELED') AND NEW.cred_deb = 'C' THEN amount_inprocess - NEW.amount
									WHEN OLD.status = 'IN_PROCESS' AND NEW.status IN ('AVAILABLE', 'CANCELED') AND NEW.cred_deb = 'D' THEN amount_inprocess + NEW.amount
									WHEN NEW.status = 'IN_PROCESS' AND OLD.status IN ('AVAILABLE', 'CANCELED') AND NEW.cred_deb = 'C' THEN amount_inprocess + NEW.amount
									WHEN NEW.status = 'IN_PROCESS' AND OLD.status IN ('AVAILABLE', 'CANCELED') AND NEW.cred_deb = 'D' THEN amount_inprocess - NEW.amount
									ELSE amount_inprocess
								  END,
			   amount_available = CASE
									WHEN NEW.status = 'AVAILABLE' AND OLD.status IN ('IN_PROCESS', 'CANCELED') AND NEW.cred_deb = 'C' THEN amount_available + NEW.amount
									WHEN NEW.status = 'AVAILABLE' AND OLD.status IN ('IN_PROCESS', 'CANCELED') AND NEW.cred_deb = 'D' THEN amount_available - NEW.amount
									WHEN OLD.status = 'AVAILABLE' AND NEW.status IN ('IN_PROCESS', 'CANCELED') AND NEW.cred_deb = 'C' THEN amount_available - NEW.amount
									WHEN OLD.status = 'AVAILABLE' AND NEW.status IN ('IN_PROCESS', 'CANCELED') AND NEW.cred_deb = 'D' THEN amount_available + NEW.amount
									ELSE amount_available
								  END,
			   amount_canceled = CASE
									WHEN NEW.status = 'CANCELED' AND OLD.status IN ('IN_PROCESS', 'AVAILABLE') AND NEW.cred_deb = 'C' THEN amount_canceled + NEW.amount
									WHEN NEW.status = 'CANCELED' AND OLD.status IN ('IN_PROCESS', 'AVAILABLE') AND NEW.cred_deb = 'D' THEN amount_canceled - NEW.amount
									WHEN OLD.status = 'CANCELED' AND NEW.status IN ('IN_PROCESS', 'AVAILABLE') AND NEW.cred_deb = 'C' THEN amount_canceled - NEW.amount
									WHEN OLD.status = 'CANCELED' AND NEW.status IN ('IN_PROCESS', 'AVAILABLE') AND NEW.cred_deb = 'D' THEN amount_canceled + NEW.amount
									ELSE amount_canceled
								  END
		 WHERE user_id = NEW.user_id
           AND value_type = NEW.value_type
           AND currency = NEW.currency;
	
	END
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `wallet_balance_wallet_validate_update` BEFORE UPDATE ON `wallet_balance` FOR EACH ROW BEGIN
		# validate amount in order to prevent to be updated
        if NEW.amount != OLD.amount then
			SIGNAL SQLSTATE '05000' SET MESSAGE_TEXT = 'Error: la cantidad no puede ser modificada!';
		end if;
	
	END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura para la vista `payment_credit_cards`
--
DROP TABLE IF EXISTS `payment_credit_cards`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `payment_credit_cards`  AS  select `ov`.`id` AS `id`,`ov`.`payment_id` AS `payment_id`,`cc`.`credit_card_id` AS `credit_card_id`,`cc`.`token` AS `token`,`cc`.`token_agro` AS `token_agro`,`cc`.`owner_name` AS `owner_name`,`cc`.`bin` AS `bin`,`cc`.`last_four_digits` AS `last_four_digits`,`cc`.`expiration_month` AS `expiration_month`,`cc`.`expiration_year` AS `expiration_year`,`cc`.`amount` AS `amount` from (`transaction_credit_card` `cc` join `transaction_values` `ov` on((`cc`.`id` = `ov`.`id`))) ;

-- --------------------------------------------------------

--
-- Estructura para la vista `transactions`
--
DROP TABLE IF EXISTS `transactions`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`%` SQL SECURITY DEFINER VIEW `transactions`  AS  select `po`.`id` AS `id`,'PAYMENT_ORDER' AS `operation_type`,'C' AS `deb_cred`,`po`.`uuid` AS `uuid`,`po`.`code` AS `code`,`po`.`beneficiary_id` AS `user_id`,concat(ifnull(`b`.`first_name`,''),', ',ifnull(`b`.`last_name`,'')) AS `user`,`po`.`date` AS `date`,`po`.`description` AS `description`,`po`.`status` AS `status`,`po`.`payer_id` AS `target_id`,concat(ifnull(`pa`.`first_name`,''),', ',ifnull(`pa`.`last_name`,'')) AS `target`,`po`.`amount` AS `amount` from ((`payment_order` `po` join `security_user` `b` on((`b`.`id` = `po`.`beneficiary_id`))) join `security_user` `pa` on((`pa`.`id` = `po`.`payer_id`))) union select `po`.`id` AS `id`,'PAYMENT_ORDER' AS `operation_type`,'D' AS `deb_cred`,`po`.`uuid` AS `uuid`,`po`.`code` AS `code`,`po`.`payer_id` AS `user_id`,concat(ifnull(`p`.`first_name`,''),', ',ifnull(`p`.`last_name`,'')) AS `user`,`po`.`date` AS `date`,`po`.`description` AS `description`,`po`.`status` AS `status`,`po`.`beneficiary_id` AS `target_id`,concat(ifnull(`b`.`first_name`,''),', ',ifnull(`b`.`last_name`,'')) AS `target`,`po`.`amount` AS `amount` from ((`payment_order` `po` join `security_user` `b` on((`b`.`id` = `po`.`beneficiary_id`))) join `security_user` `p` on((`p`.`id` = `po`.`payer_id`))) union select `t`.`id` AS `id`,'TRANSFER' AS `operation_type`,'D' AS `deb_cred`,`t`.`uuid` AS `uuid`,`t`.`code` AS `code`,`t`.`sender_id` AS `user_id`,concat(ifnull(`s`.`first_name`,''),', ',ifnull(`s`.`last_name`,'')) AS `user`,`t`.`date` AS `date`,`t`.`description` AS `description`,`t`.`status` AS `status`,`t`.`receiver_id` AS `target_id`,concat(ifnull(`d`.`first_name`,''),', ',ifnull(`d`.`last_name`,'')) AS `target`,`t`.`amount` AS `amount` from ((`transfer` `t` join `security_user` `s` on((`s`.`id` = `t`.`sender_id`))) join `security_user` `d` on((`d`.`id` = `t`.`receiver_id`))) union select `t`.`id` AS `id`,'TRANSFER' AS `operation_type`,'C' AS `deb_cred`,`t`.`uuid` AS `uuid`,`t`.`code` AS `code`,`t`.`receiver_id` AS `user_id`,concat(ifnull(`d`.`first_name`,''),', ',ifnull(`d`.`last_name`,'')) AS `user`,`t`.`date` AS `date`,`t`.`description` AS `description`,`t`.`status` AS `status`,`t`.`sender_id` AS `afected_id`,concat(ifnull(`s`.`first_name`,''),', ',ifnull(`s`.`last_name`,'')) AS `afected`,`t`.`amount` AS `amount` from ((`transfer` `t` join `security_user` `s` on((`s`.`id` = `t`.`sender_id`))) join `security_user` `d` on((`d`.`id` = `t`.`receiver_id`))) union select `t`.`id` AS `id`,'CHECKOUT' AS `operation_type`,'D' AS `deb_cred`,`t`.`uuid` AS `uuid`,`t`.`code` AS `code`,`t`.`user_id` AS `user_id`,concat(ifnull(`s`.`first_name`,''),', ',ifnull(`s`.`last_name`,'')) AS `user`,`t`.`date` AS `date`,'Retiro Dinero' AS `description`,`t`.`status` AS `status`,`c`.`id` AS `target_id`,concat('cbu: ',ifnull(`c`.`cbu`,''),', banco:',ifnull(`b`.`name`,'')) AS `target`,`t`.`amount` AS `amount` from (((`checkout` `t` join `security_user` `s` on((`s`.`id` = `t`.`user_id`))) join `user_bank_account` `c` on((`c`.`id` = `t`.`user_bank_account_id`))) join `bank` `b` on((`b`.`id` = `c`.`bank_id`))) ;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `asset`
--
ALTER TABLE `asset`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `authtoken_token`
--
ALTER TABLE `authtoken_token`
  ADD PRIMARY KEY (`key`),
  ADD UNIQUE KEY `user_id` (`user_id`);

--
-- Indices de la tabla `auth_group`
--
ALTER TABLE `auth_group`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indices de la tabla `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `group_id` (`group_id`,`permission_id`),
  ADD KEY `auth_group__permission_id_1f49ccbbdc69d2fc_fk_auth_permission_id` (`permission_id`);

--
-- Indices de la tabla `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `content_type_id` (`content_type_id`,`codename`);

--
-- Indices de la tabla `bank`
--
ALTER TABLE `bank`
  ADD PRIMARY KEY (`id`),
  ADD KEY `who_created_id` (`who_created_id`),
  ADD KEY `who_deleted_id` (`who_deleted_id`),
  ADD KEY `who_last_edit_id` (`who_last_edit_id`);

--
-- Indices de la tabla `base_objectsequence`
--
ALTER TABLE `base_objectsequence`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `base_objectsequence_subclass_50d06d6ac20b1414_uniq` (`subclass`,`content_type_id`),
  ADD KEY `base__content_type_id_3e39e538101ff18f_fk_django_content_type_id` (`content_type_id`);

--
-- Indices de la tabla `checkout`
--
ALTER TABLE `checkout`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `code` (`code`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `who_created_id` (`who_created_id`),
  ADD KEY `who_last_edit_id` (`who_last_edit_id`),
  ADD KEY `who_deleted_id` (`who_deleted_id`),
  ADD KEY `user_bank_account_id` (`user_bank_account_id`);

--
-- Indices de la tabla `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `djang_content_type_id_697914295151027a_fk_django_content_type_id` (`content_type_id`),
  ADD KEY `django_admin_log_user_id_52fdd58701c5f563_fk_security_user_id` (`user_id`);

--
-- Indices de la tabla `django_content_type`
--
ALTER TABLE `django_content_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `django_content_type_app_label_45f3b1d93ec8c61c_uniq` (`app_label`,`model`);

--
-- Indices de la tabla `django_migrations`
--
ALTER TABLE `django_migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `django_session`
--
ALTER TABLE `django_session`
  ADD PRIMARY KEY (`session_key`),
  ADD KEY `django_session_de54fa62` (`expire_date`);

--
-- Indices de la tabla `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `code` (`code`),
  ADD KEY `payment_order_id` (`payment_order_id`),
  ADD KEY `payer_id` (`payer_id`),
  ADD KEY `who_cancelled_id` (`who_cancelled_id`),
  ADD KEY `who_last_edit_id` (`who_last_edit_id`),
  ADD KEY `who_accepted_id` (`who_accepted_id`),
  ADD KEY `who_created_id` (`who_created_id`),
  ADD KEY `who_deleted_id` (`who_deleted_id`),
  ADD KEY `who_rejected_id` (`who_rejected_id`),
  ADD KEY `who_under_verification_id` (`who_under_verification_id`);

--
-- Indices de la tabla `payment_method`
--
ALTER TABLE `payment_method`
  ADD PRIMARY KEY (`id`),
  ADD KEY `asset_id` (`asset_id`);

--
-- Indices de la tabla `payment_order`
--
ALTER TABLE `payment_order`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `code` (`code`),
  ADD KEY `who_transferred_id` (`who_transferred_id`),
  ADD KEY `who_transferring_id` (`who_transferring_id`),
  ADD KEY `who_available_id` (`who_available_id`),
  ADD KEY `who_cancelled_id` (`who_cancelled_id`),
  ADD KEY `who_last_edit_id` (`who_last_edit_id`),
  ADD KEY `credit_card_id` (`credit_card_id`),
  ADD KEY `who_created_id` (`who_created_id`),
  ADD KEY `who_deleted_id` (`who_deleted_id`),
  ADD KEY `beneficiary_id` (`beneficiary_id`),
  ADD KEY `payer_id` (`payer_id`),
  ADD KEY `user_bank_account_id` (`user_bank_account_id`);

--
-- Indices de la tabla `rate`
--
ALTER TABLE `rate`
  ADD PRIMARY KEY (`id`),
  ADD KEY `paymentmethod_id` (`paymentmethod_id`);

--
-- Indices de la tabla `security_user`
--
ALTER TABLE `security_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `security_user_cuit_21f1721fe03237fb_uniq` (`cuit`),
  ADD KEY `security_user_3db7701a` (`who_inactive_id`),
  ADD KEY `security_user_who_created_id_43334463411c1d5_fk_security_user_id` (`who_created_id`),
  ADD KEY `security_use_who_deleted_id_783524641b7a708e_fk_security_user_id` (`who_deleted_id`),
  ADD KEY `security_us_who_last_edit_id_b9569b25dba4c34_fk_security_user_id` (`who_last_edit_id`);

--
-- Indices de la tabla `security_user_groups`
--
ALTER TABLE `security_user_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_id` (`user_id`,`group_id`),
  ADD KEY `security_user_groups_group_id_20fbda0cdc70637b_fk_auth_group_id` (`group_id`);

--
-- Indices de la tabla `security_user_user_permissions`
--
ALTER TABLE `security_user_user_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_id` (`user_id`,`permission_id`),
  ADD KEY `security_us_permission_id_5e09889514738f3c_fk_auth_permission_id` (`permission_id`);

--
-- Indices de la tabla `transaction_credit_card`
--
ALTER TABLE `transaction_credit_card`
  ADD PRIMARY KEY (`id`),
  ADD KEY `who_created_id` (`who_created_id`),
  ADD KEY `who_last_edit_id` (`who_last_edit_id`),
  ADD KEY `who_deleted_id` (`who_deleted_id`),
  ADD KEY `credit_card_id` (`credit_card_id`);

--
-- Indices de la tabla `transaction_values`
--
ALTER TABLE `transaction_values`
  ADD PRIMARY KEY (`id`),
  ADD KEY `value_type` (`value_type`,`currency`),
  ADD KEY `payment_id` (`payment_id`),
  ADD KEY `transfer_id` (`transfer_id`),
  ADD KEY `checkout_id` (`checkout_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `who_created_id` (`who_created_id`),
  ADD KEY `who_last_edit_id` (`who_last_edit_id`),
  ADD KEY `who_deleted_id` (`who_deleted_id`);

--
-- Indices de la tabla `transfer`
--
ALTER TABLE `transfer`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `code` (`code`),
  ADD KEY `sender_id` (`sender_id`),
  ADD KEY `receiver_id` (`receiver_id`),
  ADD KEY `who_created_id` (`who_created_id`),
  ADD KEY `who_last_edit_id` (`who_last_edit_id`),
  ADD KEY `who_deleted_id` (`who_deleted_id`);

--
-- Indices de la tabla `user_bank_account`
--
ALTER TABLE `user_bank_account`
  ADD PRIMARY KEY (`id`),
  ADD KEY `who_valid_account_id` (`who_valid_account_id`),
  ADD KEY `who_last_edit_id` (`who_last_edit_id`),
  ADD KEY `who_created_id` (`who_created_id`),
  ADD KEY `who_deleted_id` (`who_deleted_id`),
  ADD KEY `bank_id` (`bank_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indices de la tabla `validation_codes`
--
ALTER TABLE `validation_codes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_id_code` (`user_id`,`code`);

--
-- Indices de la tabla `value_types`
--
ALTER TABLE `value_types`
  ADD PRIMARY KEY (`value_type`,`currency`);

--
-- Indices de la tabla `wallet`
--
ALTER TABLE `wallet`
  ADD PRIMARY KEY (`user_id`,`value_type`,`currency`),
  ADD KEY `value_type` (`value_type`,`currency`);

--
-- Indices de la tabla `wallet_balance`
--
ALTER TABLE `wallet_balance`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `user_id_2` (`user_id`,`value_type`,`currency`),
  ADD KEY `value_id` (`value_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `asset`
--
ALTER TABLE `asset`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `auth_group`
--
ALTER TABLE `auth_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `auth_permission`
--
ALTER TABLE `auth_permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT de la tabla `bank`
--
ALTER TABLE `bank`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT de la tabla `base_objectsequence`
--
ALTER TABLE `base_objectsequence`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `checkout`
--
ALTER TABLE `checkout`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `django_admin_log`
--
ALTER TABLE `django_admin_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `django_content_type`
--
ALTER TABLE `django_content_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `django_migrations`
--
ALTER TABLE `django_migrations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT de la tabla `payment`
--
ALTER TABLE `payment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `payment_method`
--
ALTER TABLE `payment_method`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `payment_order`
--
ALTER TABLE `payment_order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `rate`
--
ALTER TABLE `rate`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `security_user`
--
ALTER TABLE `security_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=128;

--
-- AUTO_INCREMENT de la tabla `security_user_groups`
--
ALTER TABLE `security_user_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `security_user_user_permissions`
--
ALTER TABLE `security_user_user_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `transaction_values`
--
ALTER TABLE `transaction_values`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `transfer`
--
ALTER TABLE `transfer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `user_bank_account`
--
ALTER TABLE `user_bank_account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `validation_codes`
--
ALTER TABLE `validation_codes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `wallet_balance`
--
ALTER TABLE `wallet_balance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `authtoken_token`
--
ALTER TABLE `authtoken_token`
  ADD CONSTRAINT `authtoken_token_user_id_1d10c57f535fb363_fk_security_user_id` FOREIGN KEY (`user_id`) REFERENCES `security_user` (`id`);

--
-- Filtros para la tabla `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD CONSTRAINT `auth_group__permission_id_1f49ccbbdc69d2fc_fk_auth_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  ADD CONSTRAINT `auth_group_permission_group_id_689710a9a73b7457_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`);

--
-- Filtros para la tabla `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD CONSTRAINT `auth__content_type_id_508cf46651277a81_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`);

--
-- Filtros para la tabla `bank`
--
ALTER TABLE `bank`
  ADD CONSTRAINT `bank_ibfk_1` FOREIGN KEY (`who_created_id`) REFERENCES `security_user` (`id`),
  ADD CONSTRAINT `bank_ibfk_2` FOREIGN KEY (`who_deleted_id`) REFERENCES `security_user` (`id`),
  ADD CONSTRAINT `bank_ibfk_3` FOREIGN KEY (`who_last_edit_id`) REFERENCES `security_user` (`id`);

--
-- Filtros para la tabla `base_objectsequence`
--
ALTER TABLE `base_objectsequence`
  ADD CONSTRAINT `base__content_type_id_3e39e538101ff18f_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`);

--
-- Filtros para la tabla `checkout`
--
ALTER TABLE `checkout`
  ADD CONSTRAINT `checkout_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `security_user` (`id`),
  ADD CONSTRAINT `checkout_ibfk_2` FOREIGN KEY (`who_created_id`) REFERENCES `security_user` (`id`),
  ADD CONSTRAINT `checkout_ibfk_3` FOREIGN KEY (`who_last_edit_id`) REFERENCES `security_user` (`id`),
  ADD CONSTRAINT `checkout_ibfk_4` FOREIGN KEY (`who_deleted_id`) REFERENCES `security_user` (`id`),
  ADD CONSTRAINT `checkout_ibfk_5` FOREIGN KEY (`user_bank_account_id`) REFERENCES `user_bank_account` (`id`);

--
-- Filtros para la tabla `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD CONSTRAINT `djang_content_type_id_697914295151027a_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  ADD CONSTRAINT `django_admin_log_user_id_52fdd58701c5f563_fk_security_user_id` FOREIGN KEY (`user_id`) REFERENCES `security_user` (`id`);

--
-- Filtros para la tabla `payment`
--
ALTER TABLE `payment`
  ADD CONSTRAINT `payment_ibfk_1` FOREIGN KEY (`payment_order_id`) REFERENCES `payment_order` (`id`),
  ADD CONSTRAINT `payment_ibfk_2` FOREIGN KEY (`payer_id`) REFERENCES `security_user` (`id`),
  ADD CONSTRAINT `payment_ibfk_3` FOREIGN KEY (`who_cancelled_id`) REFERENCES `security_user` (`id`),
  ADD CONSTRAINT `payment_ibfk_4` FOREIGN KEY (`who_last_edit_id`) REFERENCES `security_user` (`id`),
  ADD CONSTRAINT `payment_ibfk_5` FOREIGN KEY (`who_accepted_id`) REFERENCES `security_user` (`id`),
  ADD CONSTRAINT `payment_ibfk_6` FOREIGN KEY (`who_created_id`) REFERENCES `security_user` (`id`),
  ADD CONSTRAINT `payment_ibfk_7` FOREIGN KEY (`who_deleted_id`) REFERENCES `security_user` (`id`),
  ADD CONSTRAINT `payment_ibfk_8` FOREIGN KEY (`who_rejected_id`) REFERENCES `security_user` (`id`),
  ADD CONSTRAINT `payment_ibfk_9` FOREIGN KEY (`who_under_verification_id`) REFERENCES `security_user` (`id`);

--
-- Filtros para la tabla `payment_method`
--
ALTER TABLE `payment_method`
  ADD CONSTRAINT `payment_method_ibfk_1` FOREIGN KEY (`asset_id`) REFERENCES `asset` (`id`);

--
-- Filtros para la tabla `payment_order`
--
ALTER TABLE `payment_order`
  ADD CONSTRAINT `payment_order_ibfk_1` FOREIGN KEY (`who_transferred_id`) REFERENCES `security_user` (`id`),
  ADD CONSTRAINT `payment_order_ibfk_10` FOREIGN KEY (`payer_id`) REFERENCES `security_user` (`id`),
  ADD CONSTRAINT `payment_order_ibfk_11` FOREIGN KEY (`user_bank_account_id`) REFERENCES `user_bank_account` (`id`),
  ADD CONSTRAINT `payment_order_ibfk_2` FOREIGN KEY (`who_transferring_id`) REFERENCES `security_user` (`id`),
  ADD CONSTRAINT `payment_order_ibfk_3` FOREIGN KEY (`who_available_id`) REFERENCES `security_user` (`id`),
  ADD CONSTRAINT `payment_order_ibfk_4` FOREIGN KEY (`who_cancelled_id`) REFERENCES `security_user` (`id`),
  ADD CONSTRAINT `payment_order_ibfk_5` FOREIGN KEY (`who_last_edit_id`) REFERENCES `security_user` (`id`),
  ADD CONSTRAINT `payment_order_ibfk_6` FOREIGN KEY (`credit_card_id`) REFERENCES `payment_method` (`id`),
  ADD CONSTRAINT `payment_order_ibfk_7` FOREIGN KEY (`who_created_id`) REFERENCES `security_user` (`id`),
  ADD CONSTRAINT `payment_order_ibfk_8` FOREIGN KEY (`who_deleted_id`) REFERENCES `security_user` (`id`),
  ADD CONSTRAINT `payment_order_ibfk_9` FOREIGN KEY (`beneficiary_id`) REFERENCES `security_user` (`id`);

--
-- Filtros para la tabla `rate`
--
ALTER TABLE `rate`
  ADD CONSTRAINT `rate_ibfk_1` FOREIGN KEY (`paymentmethod_id`) REFERENCES `payment_method` (`id`);

--
-- Filtros para la tabla `security_user`
--
ALTER TABLE `security_user`
  ADD CONSTRAINT `security_us_who_inactive_id_774af1953c8183f3_fk_security_user_id` FOREIGN KEY (`who_inactive_id`) REFERENCES `security_user` (`id`),
  ADD CONSTRAINT `security_us_who_last_edit_id_b9569b25dba4c34_fk_security_user_id` FOREIGN KEY (`who_last_edit_id`) REFERENCES `security_user` (`id`),
  ADD CONSTRAINT `security_use_who_deleted_id_783524641b7a708e_fk_security_user_id` FOREIGN KEY (`who_deleted_id`) REFERENCES `security_user` (`id`),
  ADD CONSTRAINT `security_user_who_created_id_43334463411c1d5_fk_security_user_id` FOREIGN KEY (`who_created_id`) REFERENCES `security_user` (`id`);

--
-- Filtros para la tabla `security_user_groups`
--
ALTER TABLE `security_user_groups`
  ADD CONSTRAINT `security_user_group_user_id_307d80982434304c_fk_security_user_id` FOREIGN KEY (`user_id`) REFERENCES `security_user` (`id`),
  ADD CONSTRAINT `security_user_groups_group_id_20fbda0cdc70637b_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`);

--
-- Filtros para la tabla `security_user_user_permissions`
--
ALTER TABLE `security_user_user_permissions`
  ADD CONSTRAINT `security_us_permission_id_5e09889514738f3c_fk_auth_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  ADD CONSTRAINT `security_user_user__user_id_280369e2bf3786a2_fk_security_user_id` FOREIGN KEY (`user_id`) REFERENCES `security_user` (`id`);

--
-- Filtros para la tabla `transaction_credit_card`
--
ALTER TABLE `transaction_credit_card`
  ADD CONSTRAINT `transaction_credit_card_ibfk_1` FOREIGN KEY (`id`) REFERENCES `transaction_values` (`id`),
  ADD CONSTRAINT `transaction_credit_card_ibfk_2` FOREIGN KEY (`who_created_id`) REFERENCES `security_user` (`id`),
  ADD CONSTRAINT `transaction_credit_card_ibfk_3` FOREIGN KEY (`who_last_edit_id`) REFERENCES `security_user` (`id`),
  ADD CONSTRAINT `transaction_credit_card_ibfk_4` FOREIGN KEY (`who_deleted_id`) REFERENCES `security_user` (`id`),
  ADD CONSTRAINT `transaction_credit_card_ibfk_5` FOREIGN KEY (`credit_card_id`) REFERENCES `payment_method` (`id`);

--
-- Filtros para la tabla `transaction_values`
--
ALTER TABLE `transaction_values`
  ADD CONSTRAINT `transaction_values_ibfk_1` FOREIGN KEY (`value_type`,`currency`) REFERENCES `value_types` (`value_type`, `currency`),
  ADD CONSTRAINT `transaction_values_ibfk_2` FOREIGN KEY (`payment_id`) REFERENCES `payment` (`id`),
  ADD CONSTRAINT `transaction_values_ibfk_3` FOREIGN KEY (`transfer_id`) REFERENCES `transfer` (`id`),
  ADD CONSTRAINT `transaction_values_ibfk_4` FOREIGN KEY (`checkout_id`) REFERENCES `checkout` (`id`),
  ADD CONSTRAINT `transaction_values_ibfk_5` FOREIGN KEY (`user_id`) REFERENCES `security_user` (`id`),
  ADD CONSTRAINT `transaction_values_ibfk_6` FOREIGN KEY (`who_created_id`) REFERENCES `security_user` (`id`),
  ADD CONSTRAINT `transaction_values_ibfk_7` FOREIGN KEY (`who_last_edit_id`) REFERENCES `security_user` (`id`),
  ADD CONSTRAINT `transaction_values_ibfk_8` FOREIGN KEY (`who_deleted_id`) REFERENCES `security_user` (`id`);

--
-- Filtros para la tabla `transfer`
--
ALTER TABLE `transfer`
  ADD CONSTRAINT `transfer_ibfk_1` FOREIGN KEY (`sender_id`) REFERENCES `security_user` (`id`),
  ADD CONSTRAINT `transfer_ibfk_2` FOREIGN KEY (`receiver_id`) REFERENCES `security_user` (`id`),
  ADD CONSTRAINT `transfer_ibfk_3` FOREIGN KEY (`who_created_id`) REFERENCES `security_user` (`id`),
  ADD CONSTRAINT `transfer_ibfk_4` FOREIGN KEY (`who_last_edit_id`) REFERENCES `security_user` (`id`),
  ADD CONSTRAINT `transfer_ibfk_5` FOREIGN KEY (`who_deleted_id`) REFERENCES `security_user` (`id`);

--
-- Filtros para la tabla `user_bank_account`
--
ALTER TABLE `user_bank_account`
  ADD CONSTRAINT `user_bank_account_ibfk_1` FOREIGN KEY (`who_valid_account_id`) REFERENCES `security_user` (`id`),
  ADD CONSTRAINT `user_bank_account_ibfk_2` FOREIGN KEY (`who_last_edit_id`) REFERENCES `security_user` (`id`),
  ADD CONSTRAINT `user_bank_account_ibfk_3` FOREIGN KEY (`who_created_id`) REFERENCES `security_user` (`id`),
  ADD CONSTRAINT `user_bank_account_ibfk_4` FOREIGN KEY (`who_deleted_id`) REFERENCES `security_user` (`id`),
  ADD CONSTRAINT `user_bank_account_ibfk_5` FOREIGN KEY (`bank_id`) REFERENCES `bank` (`id`),
  ADD CONSTRAINT `user_bank_account_ibfk_6` FOREIGN KEY (`user_id`) REFERENCES `security_user` (`id`);

--
-- Filtros para la tabla `validation_codes`
--
ALTER TABLE `validation_codes`
  ADD CONSTRAINT `validation_codes__user_fk_security_user_id` FOREIGN KEY (`user_id`) REFERENCES `security_user` (`id`);

--
-- Filtros para la tabla `wallet`
--
ALTER TABLE `wallet`
  ADD CONSTRAINT `wallet_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `security_user` (`id`),
  ADD CONSTRAINT `wallet_ibfk_2` FOREIGN KEY (`value_type`,`currency`) REFERENCES `value_types` (`value_type`, `currency`);

--
-- Filtros para la tabla `wallet_balance`
--
ALTER TABLE `wallet_balance`
  ADD CONSTRAINT `wallet_balance_ibfk_1` FOREIGN KEY (`user_id`,`value_type`,`currency`) REFERENCES `wallet` (`user_id`, `value_type`, `currency`),
  ADD CONSTRAINT `wallet_balance_ibfk_2` FOREIGN KEY (`value_id`) REFERENCES `transaction_values` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
