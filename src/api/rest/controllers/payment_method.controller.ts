import { CurrentUser, UserInfo } from './../../../services/auth/currentuser.decorator';
import { JwtAuthGuard } from './../../../services/auth/jwt-auth.guard';
import { Calc } from './../../../services/utils/calc';
import { PaymentMethodCalcInput } from './../../../model/agropago/payment_method/payment_method';
import { PaymentMethodService } from './../../../services/payment_method.service';
import { Controller, Request, Get, Post, Put, Body, Query, Delete, HttpCode, Param, Header, UseGuards } from '@nestjs/common';

import { LoggerService } from 'nest-logger';

@Controller()
export class PaymentMethodController {
  constructor(private readonly logger: LoggerService,
              private readonly paymentMethodService: PaymentMethodService,
              private readonly calc: Calc) {}

  @UseGuards(JwtAuthGuard)
  @Get('/payment_methods')
  async getPaymentMethod(@CurrentUser() currentUser: UserInfo, @Request() req, @Query('offset') offset = 0, @Query('limit') limit = 100) {
    const offsetParm = offset;
    const limitParm = limit;
    const items = await this.paymentMethodService.getPaymentMethods(offsetParm, limitParm);
    return {
      pagination: {
        offset: offsetParm,
        limit: limitParm,
        total: items.length,
      },
      data: items,
      links: {
        next: '',
        prev: '',
      },
    };
  }

  @UseGuards(JwtAuthGuard)
  @Get('/payment_methods/:id')
  async getPaymentMethodById(@CurrentUser() currentUser: UserInfo, @Param('id') id : number= -1) {
    return this.paymentMethodService.getPaymentMethodById(id);
  }

  @UseGuards(JwtAuthGuard)
  @Post('/payment_methods/calc')
  async calcFees(@Body() paymentMethodCalcInput: PaymentMethodCalcInput) {
    return this.calc.calculateFee(1, paymentMethodCalcInput.payment_method_id, paymentMethodCalcInput.options, paymentMethodCalcInput.amount);
  }

}
