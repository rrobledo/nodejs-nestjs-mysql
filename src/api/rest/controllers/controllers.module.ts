import { HealthController } from './health.controller';
import { OrdersController } from './orders.controller';
import { Module } from '@nestjs/common';
import { ItemsController } from 'api/rest/controllers/items.controller';
import { LoggerModule } from 'utils/logger.module';
import { ServicesModule } from 'services/services.module';
import { AuthController } from './auth.controller';
import { AuthModule } from 'services/auth/auth.module';
import { PaymentMethodController } from './payment_method.controller';
import { TransactionsController } from './transactions.controller';

@Module({
  imports: [LoggerModule, ServicesModule, AuthModule],
  controllers: [HealthController, AuthController, ItemsController, PaymentMethodController, OrdersController, TransactionsController],
  providers: [],
})
export class ControllersModule {}
