import { UserInfo, CurrentUser } from './../../../services/auth/currentuser.decorator';
import { JwtAuthGuard } from './../../../services/auth/jwt-auth.guard';
import { PaymentFullCreate } from './../../../model/agropago/transactions/payment';
import { Context } from './../../../model/agropago/configs/context';
import { TransactionsService } from './../../../services/transactions.service';
import { Controller, Get, Post, Put, Body, Query, Delete, HttpCode, Param, Header, UseGuards } from '@nestjs/common';

import { LoggerService } from 'nest-logger';
import { PaymentReducedCreate } from 'model/agropago/transactions/payment';

@Controller()
export class TransactionsController {
  constructor(private readonly logger: LoggerService,
              private readonly transactionsService: TransactionsService) {}

  @UseGuards(JwtAuthGuard)
  @Post('/transactions/payment_orders/:payment_order_id/payments')
  async createPaymentReduced(@CurrentUser() currentUser: UserInfo, @Param('payment_order_id') payment_order_id: number, @Body() payment: PaymentReducedCreate) {
    const context = new Context(currentUser.tenantId, currentUser.userId);

    const paymentSaved = await this.transactionsService.createPaymentReduced(payment_order_id, payment, context);
    this.logger.debug('new payment created');
    return paymentSaved;
  }

  @UseGuards(JwtAuthGuard)
  @Post('/transactions/payments')
  async createPaymentFull(@CurrentUser() currentUser: UserInfo, @Body() payment: PaymentFullCreate) {
    const context = new Context(currentUser.tenantId, currentUser.userId);

    const paymentSaved = await this.transactionsService.createPaymentFull(payment, context);
    this.logger.debug('new payment created');
    return paymentSaved;
  }

  @UseGuards(JwtAuthGuard)
  @Get('/transactions/payments/:id')
  async getItemById(@CurrentUser() currentUser: UserInfo, @Param('id') pId : number = -1) {
    const context = new Context(currentUser.tenantId, currentUser.userId);
    return this.transactionsService.getPaymentById(context, pId);
  }

  @UseGuards(JwtAuthGuard)
  @Get('/transactions/payments')
  async getPaymentsByFilter(@CurrentUser() currentUser: UserInfo, @Query('filter') filter = '{}', @Query('offset') offset = 0, @Query('limit') limit = 10) {
    const context = new Context(currentUser.tenantId, currentUser.userId);

    const offsetParm = offset;
    const limitParm = limit;
    const items = await this.transactionsService.getPaymentByFilter(context, filter, offsetParm, limitParm);
    return {
      pagination: {
        offset: offsetParm,
        limit: limitParm,
        total: items.length,
      },
      data: items,
      links: {
        next: '',
        prev: '',
      },
    };
  }

  @UseGuards(JwtAuthGuard)
  @Get('/transactions')
  async getTransactionsByFilter(@CurrentUser() currentUser: UserInfo, @Query('filter') filter = '{}', @Query('offset') offset = 0, @Query('limit') limit = 10) {
    const context = new Context(currentUser.tenantId, currentUser.userId);

    const offsetParm = offset;
    const limitParm = limit;
    const items = await this.transactionsService.getTransactionsByFilter(context, filter, offsetParm, limitParm);
    return {
      pagination: {
        offset: offsetParm,
        limit: limitParm,
        total: items.length,
      },
      data: items,
      links: {
        next: '',
        prev: '',
      },
    };
  }

}
