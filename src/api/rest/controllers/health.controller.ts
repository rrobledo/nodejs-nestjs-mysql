import { Controller, Get, Request, Post, UseGuards } from '@nestjs/common';
import { AuthService } from 'services/auth/auth.service';

@Controller()
export class HealthController {
  constructor(private readonly authService: AuthService) {}

  @Get('health')
  getHealth(@Request() req) {
    return {};
  }
}
