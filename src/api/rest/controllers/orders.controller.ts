import { UserInfo, CurrentUser } from './../../../services/auth/currentuser.decorator';
import { JwtAuthGuard } from './../../../services/auth/jwt-auth.guard';
import { Context } from './../../../model/agropago/configs/context';
import { PaymentOrderCreate } from 'model/agropago/orders/payment_order';
import { Controller, Get, Post, Put, Body, Query, Delete, HttpCode, Param, Header, UseGuards } from '@nestjs/common';

import { LoggerService } from 'nest-logger';
import { OrdersService } from 'services/orders.service';

@Controller()
export class OrdersController {
  constructor(private readonly logger: LoggerService,
              private readonly ordersService: OrdersService) {}

  @UseGuards(JwtAuthGuard)
  @Post('/orders/payment_orders')
  async create(@CurrentUser() currentUser: UserInfo, @Body() paymentOrder: PaymentOrderCreate) {
    const context = new Context(currentUser.tenantId, currentUser.userId);
    const paymentOrderSaved = await this.ordersService.createPaymentOrder(paymentOrder, context);
    this.logger.debug('new order created');
    return paymentOrderSaved;
  }

  @UseGuards(JwtAuthGuard)
  @Get('/orders/payment_orders/:id')
  async getItemById(@CurrentUser() currentUser: UserInfo, @Param('id') poId = '-1') {
    const context = new Context(currentUser.tenantId, currentUser.userId);
    return this.ordersService.getPaymentOrderById(context, poId);
  }

  @UseGuards(JwtAuthGuard)
  @Get('/orders/payment_orders')
  async getByFilter(@CurrentUser() currentUser: UserInfo, @Query('filter') filter = '{}', @Query('offset') offset = 0, @Query('limit') limit = 10) {
    const context = new Context(currentUser.tenantId, currentUser.userId);

    const offsetParm = offset;
    const limitParm = limit;
    const items = await this.ordersService.getPaymentOrdersByFilter(context, filter, offsetParm, limitParm);
    return {
      pagination: {
        offset: offsetParm,
        limit: limitParm,
        total: items.length,
      },
      data: items,
      links: {
        next: '',
        prev: '',
      },
    };
  }

  @UseGuards(JwtAuthGuard)
  @Put('/orders/payment_orders/:id/cancel')
  async cancel(@CurrentUser() currentUser: UserInfo, @Param('id') poId = '-1') {
    const context = new Context(currentUser.tenantId, currentUser.userId);
    this.ordersService.cancelOrder(context, poId);
    return this.ordersService.getPaymentOrderById(context, poId);
  }


}
