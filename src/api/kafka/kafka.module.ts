import { Module } from '@nestjs/common';
import { LoggerModule } from 'utils/logger.module';
import { ServicesModule } from 'services/services.module';

@Module({
  imports: [LoggerModule, ServicesModule],
  controllers: [],
  providers: [],
  exports: [],
})
export class ControllersModule {}
