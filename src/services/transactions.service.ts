import { TransactionCreditCard } from './../model/agropago/transactions/transaction_credit_card';
import { PigmaService } from './external/pigma';
import { TenantConfig } from './../model/agropago/configs/tenant_config';
import { AgroTransactions } from 'model/agropago/transactions/transactions';
import { OrdersService } from 'services/orders.service';
import { PaymentReduced, PaymentFullCreate } from './../model/agropago/transactions/payment';
import { Context } from './../model/agropago/configs/context';
import { OrdersRepository } from './../repositories/orders.repository';
import { TransactionsRepository } from './../repositories/transactions.repository';
import { Calc, PaymentFee } from './utils/calc';
import { serialize, deserialize } from "class-transformer";
import { LoggerService } from 'nest-logger';
import { ConfigService } from '@nestjs/config';
import { Injectable, NotFoundException, BadRequestException, ForbiddenException, InternalServerErrorException } from '@nestjs/common';
import { InjectRepository, getCustomRepositoryToken } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { PaymentMethod } from 'model/agropago/payment_method/payment_method';
import { PaymentReducedCreate, Payment, PaymentCreate } from 'model/agropago/transactions/payment';
import { PaymentOrder } from 'model/agropago/orders/payment_order';
import * as MongoParse from 'mongo-parse';

@Injectable()
export class TransactionsService {

  constructor(private readonly logger: LoggerService,
              private readonly config: ConfigService,
              private readonly calc: Calc,
              private serviceOrders: OrdersService,
              private repoTransactions: TransactionsRepository,
              private repoOrders: OrdersRepository,
              private pigmaService: PigmaService,
              @InjectRepository(TenantConfig) private repoTenantConfig: Repository<TenantConfig>,
              @InjectRepository(PaymentMethod) private repoPaymentMethod: Repository<PaymentMethod>,
              @InjectRepository(TransactionCreditCard) private repoCreditCard: Repository<TransactionCreditCard>) {
  }

  async createPaymentFull(paymentCreate: PaymentFullCreate, context: Context): Promise<PaymentReduced> {
    let payment: Payment = deserialize(Payment, serialize(paymentCreate));

    payment.payment_order = await this.serviceOrders.createPaymentOrderReduced(paymentCreate.payment_order, context);
    payment.payment_order_id = payment.payment_order.id;
    return this.createPayment(payment, context)
  }


  async createPaymentReduced(paymentOrderId: number, paymentCreate: PaymentReducedCreate, context: Context): Promise<PaymentReduced> {
    let payment: Payment = deserialize(Payment, serialize(paymentCreate));

    const paymentOrder = await this.validatePaymentOrder(paymentOrderId, context);
    await this.validatePaymentMethods(payment);

    payment.payment_order = paymentOrder;
    payment.payment_order_id = paymentOrder.id;
    return this.createPayment(payment, context)
  }

  async getPaymentByFilter(context: Context, filter: string, offset: number = 0, limit: number = 50): Promise<PaymentReduced[]> {
    // validate filter
    let filterJson = JSON.parse(filter);
    MongoParse.parse(filterJson);

    let newFilter = {
      "$and" : [
          {
            "$or": [
              { "__beneficiary_id" : context.user_id },
              { "__payer_id" : context.user_id },
            ]
          },
          filterJson
      ]
    }
    return this.repoTransactions.getPaymentsByFilter(JSON.stringify(newFilter), offset, limit)
  }

  async getPaymentById(context: Context, pId: number): Promise<PaymentReduced> {
    let filter = {
      "$and" : [
          {
            id: pId,
          },
          {
            "$or": [
              { "__beneficiary_id" : context.user_id },
              { "__payer_id" : context.user_id },
            ]
          },
      ]
    }
    const result = await this.repoTransactions.getPaymentsByFilter(JSON.stringify(filter), 0, 1);
    if (result.length <= 0 ) {
      throw new NotFoundException(`payment id: ${pId} not found.`);
    } else {
      return Promise.resolve(result[0]);
    }
  }
 
  async getTransactionsByFilter(context: Context, filter: string, offset: number = 0, limit: number = 50): Promise<AgroTransactions[]> {
    // validate filter
    let filterJson = JSON.parse(filter);
    MongoParse.parse(filterJson);

    let newFilter = {
      "$and" : [
          { "user_id" : context.user_id },
          filterJson
      ]
    }
    return this.repoTransactions.getTransactionsByFilter(JSON.stringify(newFilter), offset, limit)
  }

  private async createPayment(payment: Payment, context: Context): Promise<PaymentReduced> {
    // calculate fees
    const feesCalc: PaymentFee[] = await Promise.all(payment.values.map(async paymentMethod => {
                                    const fees: PaymentFee = await this.calc.calculateFee(context.tenant_id,
                                                                                          paymentMethod.payment_method_id,
                                                                                          paymentMethod.options,
                                                                                          paymentMethod.amount)
                                    paymentMethod.fees = fees;
                                    return Promise.resolve(fees);
                                  }));
    let fees = new PaymentFee();

    feesCalc.forEach(fee => {
      fees.operation_fee += fee.operation_fee;
      fees.operation_fee_iva += fee.operation_fee_iva;
      fees.payment_method_fee += fee.payment_method_fee;
      fees.payment_method_fee_iva += fee.payment_method_fee_iva;
      fees.agropago_fee += fee.agropago_fee;
      fees.agropago_fee_iva += fee.agropago_fee_iva;
      fees.total_fee += fee.total_fee;
      fees.total_fee_iva += fee.total_fee_iva;
      fees.total += fee.total;
    })
    payment.fees = fees;

    // acummulate payment total
    const total = payment.values.reduce((sum, current) => sum + current.amount, 0);
    payment.total = total;

    // save the payment
    const paymentSaved = await this.repoTransactions.addPayment(context, payment);
    
    // process transaction on external provider.
    await Promise.all(paymentSaved.values.map(async value => {
      if (value.payment_type === 'CREDIT_CARD') {
        const paymentMethodStored = await this.repoPaymentMethod.findOne(value.payment_method_id)
        const tenantConfig: TenantConfig = await this.repoTenantConfig.findOne(context.tenant_id);
        const creditCardValue = await this.repoCreditCard.findOne({where: {id : value.id}});
        try {
          const result = await this.pigmaService.payment(context, tenantConfig, payment, paymentMethodStored, creditCardValue);

          if (result.status === 'approved' ) {
            creditCardValue.status = 'ACCEPTED';
            creditCardValue.aut = result.authorization_code;
            creditCardValue.cupon = result.payment_id;
            await this.repoCreditCard.save(creditCardValue);
          } else if (result.status === 'review' ) {
            creditCardValue.cupon = result.payment_id;
            await this.repoCreditCard.save(creditCardValue);
          } else {
            await this.repoTransactions.deletePayment(payment.id);
            throw new ForbiddenException(`Credit card rejected: ${result.reason_error_description}`)
          }
        } catch(error) {
          await this.repoTransactions.deletePayment(payment.id);
          throw new ForbiddenException(error);
        }
      }
    }))

    return this.getPaymentById(context, paymentSaved.id);
  }

  private async validatePaymentOrder(paymentOrderId: number, context: Context): Promise<PaymentOrder> {
    // validate payment order already exists and its status=PENDING
    const payment_order = await this.repoOrders.getPaymentOrderById(paymentOrderId);
    if (payment_order) {
      if (payment_order.status != 'PENDING') {
        throw new BadRequestException(`payment order id: ${paymentOrderId} is not pending.`);
      }
    }
    else {
      throw new NotFoundException(`payment order id: ${paymentOrderId} not found.`);
    }
    return payment_order;
  }

  private async validatePaymentMethods(payment: Payment): Promise<any> {
    // validate if every accepted payment method already exists
    await Promise.all(payment.values.map(async paymentMethod => {
      const paymentMethodStored = await this.repoPaymentMethod.findOne(paymentMethod.payment_method_id)
      if (!paymentMethodStored) {
        throw new NotFoundException(`payment method id: ${paymentMethod.id} not found.`);
      }
    }));
  }  
}
