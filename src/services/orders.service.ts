import { PaymentOrderReducedCreate } from './../model/agropago/orders/payment_order';
import { Context } from './../model/agropago/configs/context';
import { OrdersRepository } from './../repositories/orders.repository';
import { Calc } from './utils/calc';
import { PaymentOrder, PaymentOrderCreate } from 'model/agropago/orders/payment_order';
import { serialize, deserialize } from "class-transformer";
import { LoggerService } from 'nest-logger';
import { ConfigService } from '@nestjs/config';
import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, Connection } from 'typeorm';
import { PaymentMethod } from 'model/agropago/payment_method/payment_method';
import { Users } from 'model/agropago/users/users';
import * as MongoParse from 'mongo-parse';

@Injectable()
export class OrdersService {

  constructor(private readonly logger: LoggerService,
              private readonly config: ConfigService,
              private readonly calc: Calc,
              private repoOrders: OrdersRepository,
              @InjectRepository(Users) private repoUsers: Repository<Users>,
              @InjectRepository(PaymentMethod) private repoPaymentMethod: Repository<PaymentMethod>) {
  }

  async getPaymentOrdersByFilter(context: Context, filter: string, offset: number = 0, limit: number = 50): Promise<PaymentOrder[]> {
    // validate filter
    let filterJson = JSON.parse(filter);
    MongoParse.parse(filterJson);

    let newFilter = {
      "$and" : [
        {
          "$or": [
            { beneficiary_id : context.user_id },
            { payer_id : context.user_id },
          ]
        },
        filterJson
      ]
    }
    return this.repoOrders.getPaymentOrderByFilter(JSON.stringify(newFilter), offset, limit)
  }

  async getPaymentOrderById(context: Context, poId: string): Promise<PaymentOrder> {
    let filter = {
      "$and" : [
          {
            id: poId,
          },
          {
            "$or": [
              { beneficiary_id : context.user_id },
              { payer_id : context.user_id },
            ]
          },
        ]
    }
    const result =  await this.repoOrders.getPaymentOrderByFilter(JSON.stringify(filter), 0, 1);
    if (result.length <= 0 ) {
      throw new NotFoundException(`payment order id: ${poId} not found.`);
    } else {
      return Promise.resolve(result[0]);
    }
  }

  async createPaymentOrder(paymentOrderCreate: PaymentOrderCreate, context: Context): Promise<PaymentOrder> {
    let paymentOrder: PaymentOrder = deserialize(PaymentOrder, serialize(paymentOrderCreate));

    await this.fillBeneficiaryAndPayer(paymentOrder, context);
    await this.validateAndCalculateFees(paymentOrder, context);

    // save the payment Order
    const paymentOrderSaved = await this.repoOrders.addPaymentOrder(paymentOrder);
    return Promise.resolve(paymentOrderSaved);
  }

  async createPaymentOrderReduced(paymentOrderCreate: PaymentOrderReducedCreate, context: Context): Promise<PaymentOrder> {
    let paymentOrder: PaymentOrder = deserialize(PaymentOrder, serialize(paymentOrderCreate));

    await this.fillBeneficiaryAndPayer(paymentOrder, context);

    // save the payment Order
    const paymentOrderSaved = await this.repoOrders.addPaymentOrder(paymentOrder);
    return Promise.resolve(paymentOrderSaved);
  }

  async cancelOrder(context: Context, poId: string): Promise<any> {
    let filter = {
      "$and" : [
          {
            id: poId,
          },
          {
            beneficiary_id: context.user_id,
          },
      ]
    }
    const result =  await this.repoOrders.getPaymentOrderByFilter(JSON.stringify(filter), 0, 1);
    if (result.length <= 0 ) {
      throw new NotFoundException(`payment order id: ${poId} not found.`);
    }
    await this.repoOrders.cancelPaymentOrder(result[0]);
    return Promise.resolve();
  }

  private async fillBeneficiaryAndPayer(paymentOrder: PaymentOrder, context: Context): Promise<PaymentOrder> {
    // add beneficiary
    const beneficiary = new Users();
    beneficiary.id = context.user_id
    paymentOrder.beneficiary = beneficiary;

    // find payer
    let payerStored = await this.repoUsers.findOne({
      where: { email : paymentOrder.payer_email }
    })
    if (!payerStored) {
      const payer = new Users();
      payer.tenant_id = context.default_tenant_id;
      payer.email = paymentOrder.payer_email;
      payer.is_active = false;
      payerStored = await this.repoUsers.save(payer);
    }
    paymentOrder.payer = payerStored;
    return paymentOrder;
  }

  private async validateAndCalculateFees(paymentOrder: PaymentOrder, context: Context): Promise<any> {
    // validate if every accepted payment method already exists
    // and calculate its fee
    await Promise.all(paymentOrder.accepted_payment_methods.map(async paymentMethod => {
      const fees = await this.calc.calculateFee(context.tenant_id, paymentMethod.id, paymentMethod.options, paymentOrder.amount)
      paymentMethod.fees = fees
      return Promise.resolve();
    }));
  }
}
