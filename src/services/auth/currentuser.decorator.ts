import { createParamDecorator } from '@nestjs/common';

export class UserInfo {
    userId : number;
    tenantId: number;
    role: string;
}

/**
 * retrieve the current user with a decorator
 * example of a controller method:
 * @Post()
 * someMethod(@CurrentUser() currentUser) {
 *   // do something with the user
 * }
 */
export const CurrentUser = createParamDecorator((unknown, req) : UserInfo => {
    let buff = Buffer.from(req.user.userId, 'base64');
    let text = buff.toString('ascii');
    let userInfo : UserInfo = JSON.parse(text);

    return userInfo
});
