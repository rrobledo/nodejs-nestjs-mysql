import { TransactionCreditCard } from './../../model/agropago/transactions/transaction_credit_card';
import { AxiosResponse } from 'axios';
import { Context } from './../../model/agropago/configs/context';
import { PaymentMethod } from '../../model/agropago/payment_method/payment_method';
import { TenantConfig } from '../../model/agropago/configs/tenant_config';
import { Injectable, BadRequestException, HttpService, InternalServerErrorException } from '@nestjs/common';
import { LoggerService } from 'nest-logger';
import { ConfigService } from '@nestjs/config';
import { Payment } from 'model/agropago/transactions/payment';
import * as moment from 'moment';

@Injectable()
export class PigmaService {
    constructor(private readonly logger: LoggerService,
        private readonly config: ConfigService,
        private httpService: HttpService) {
    }

    async payment(context: Context, tenantConfig: TenantConfig, payment: Payment, paymentMethod: PaymentMethod, creditCard: TransactionCreditCard) : Promise<any> {
        const operationId = `${payment.id.toString()}-${(new Date).getTime()}`;
        const installmentStartDate = moment().add(creditCard.term, 'days');
        const installmentAmount = creditCard.amount / creditCard.installment;
        let installments = [];

        for (let month = 0; month < creditCard.installment; month++) {
            const installmentDate = installmentStartDate.add(month, 'months');
            installments.push({
                id: month + 1,
                date: installmentDate.format('YYYY-MM-DD'),
                amount: installmentAmount * 100
            })
        }

        const paymentData = {
            "operation_id": operationId,
            "payment_method_id": paymentMethod.options.payment_method_id,
            "payment_detail": payment.payment_order.description,
            "email": "raul.osvaldo.robledo@gmail.com",
            "token": creditCard.token,
            "amount": creditCard.amount * 100,
            "installments": installments,
            "token_agro": creditCard.token_agro,
            "token_type": "T",
            "days_agreement": creditCard.term,
            "card_info": {
                "name": creditCard.owner_name,
                "bin": creditCard.bin,
                "last_four_digits": creditCard.last_four_digits,
                "expiration_month": creditCard.expiration_month,
                "expiration_year": creditCard.expiration_year
            }
        }

        const enterpriseUrl = `${this.config.get('enterprise.api_url')}/payments_agro`;
        const enterpriseApiKey = tenantConfig.payment_api_key;
        const headers = {
            "Content-Type": "application/json",
            "Authorization": `Bearer ${enterpriseApiKey}`
        }
        try {
            const result: AxiosResponse = await this.httpService.post(enterpriseUrl, paymentData, { headers: headers }).toPromise();
            if (result.status != 201) {
                this.logger.error(`Error creating a payment status_code: ${result.status}`);
                this.logger.error(`Error creating a payment content: ${JSON.stringify(result.data)}`);
                throw new BadRequestException('Ha ocurrido un error procesando el pago, Por favor intente nuevamente o contactenos a soporte@agropago.com')
            }
            return Promise.resolve(result.data);
        } catch(error) {
            if (error.response.status === 400) {
                this.logger.error(`Error creating a payment status_code: ${error.response.status}`);
                this.logger.error(`Error creating a payment content: ${JSON.stringify(error.response.data)}`);
                throw new BadRequestException(JSON.stringify(error.response.data.request_errors));
            } else {
                this.logger.error(`Error creating a payment status_code: ${error.response.status}`);
                this.logger.error(`Error creating a payment content: ${JSON.stringify(error.response.data)}`);
                throw new InternalServerErrorException('Ha ocurrido un error procesando el pago, Por favor intente nuevamente o contactenos a soporte@agropago.com')
            }
        }
    }
}
