import { PigmaService } from './pigma';
import { Module, HttpModule, HttpService } from '@nestjs/common';

import { LoggerModule } from 'utils/logger.module';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [
    HttpModule,
    LoggerModule,
    ConfigModule
  ],
  providers: [PigmaService],
  exports: [PigmaService, HttpModule],
})

export class ExternalModule {}
