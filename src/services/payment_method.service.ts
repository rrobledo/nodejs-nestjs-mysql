import { PaymentMethod } from './../model/agropago/payment_method/payment_method';
import { LoggerService } from 'nest-logger';
import { ConfigService } from '@nestjs/config';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class PaymentMethodService {

  constructor(private readonly logger: LoggerService,
              private readonly config: ConfigService,
              @InjectRepository(PaymentMethod) private repo: Repository<PaymentMethod>) {
  }

  async getPaymentMethods(offset: number = 0, limit: number = 50): Promise<PaymentMethod[]> {
    return await this.repo.find();
  }

  async getPaymentMethodById(payment_method_id: number): Promise<PaymentMethod> {
    return await this.repo.findOne(`${payment_method_id}`);
  }

}
