import { Calc } from './calc';
import { Module } from '@nestjs/common';

import { LoggerModule } from 'utils/logger.module';
import { ConfigModule } from '@nestjs/config';
import { RepositoriesModule } from 'repositories/repositories.module';

@Module({
  imports: [LoggerModule, ConfigModule, RepositoriesModule],
  providers: [Calc],
  exports: [Calc],
})

export class UtilsModule {}
