import { PaymentMethod } from './../../model/agropago/payment_method/payment_method';
import { TenantConfig } from './../../model/agropago/configs/tenant_config';
import { InjectRepository } from '@nestjs/typeorm';
import { Injectable, NotFoundException, InternalServerErrorException } from '@nestjs/common';
import { LoggerService } from 'nest-logger';
import { ConfigService } from '@nestjs/config';
import { Repository } from 'typeorm';

/*
TEA = [(1+TNA/PERIODO)*PERIODO – 1] => duracion en anios
TEM = [(1+TEA)*(1/12) – 1]
TED = (((1 + TEM) / 30) − 1)

term = (Monto * (TED x (1 + TED)*DIAS)) / ((1 + TED)*DIAS) – 1)
interes cuota = (Monto  * TEM * TEM * CUOTAS) / (TEM * CUOTAS – 1)
interes plazo = (Monto  * TED * TED * MESES) / (TED * MESES – 1)
*/

@Injectable()
export class Calc {
    private PAYMENT_METHOD_FN = {
        "CREDIT_CARD" : new PaymentMethodTypeCreditCard()
    }

    constructor(private readonly logger: LoggerService,
        private readonly config: ConfigService,
        @InjectRepository(TenantConfig) private repoTenantConfig: Repository<TenantConfig>,
        @InjectRepository(PaymentMethod) private repoPaymentMethod: Repository<PaymentMethod>) {
    }

    async calculateFee(tenantId: number, paymentMethodId: number, options: any, amount: number) : Promise<PaymentFee> {
        const paymentMethod = await this.repoPaymentMethod.findOne(paymentMethodId)
        if (!paymentMethod) {
          throw new NotFoundException(`payment method id: ${paymentMethodId} not found.`);
        }
        const paymentFee = new PaymentFee();

        try {
            const tenantConfig: TenantConfig = await this.repoTenantConfig.findOne(tenantId)
    
            // calc operation fee
            paymentFee.operation_fee = this.roundN(amount * paymentMethod.operation_rate, 2);
            paymentFee.operation_fee_iva = this.roundN(paymentFee.operation_fee * tenantConfig.operation_iva_rate, 2);
    
            // calc payment method fee
            const fee: PaymentMethodTypeFee = (this.PAYMENT_METHOD_FN[paymentMethod.payment_type]).calculateFee(tenantConfig, paymentMethod, options, amount);
            paymentFee.payment_method_fee = this.roundN(fee.fee, 2);
            paymentFee.payment_method_fee_iva = this.roundN(fee.fee_iva, 2);
            
            // calc agropago fee
            paymentFee.agropago_fee = this.roundN(amount * tenantConfig.agropago_rate, 2);
            paymentFee.agropago_fee_iva = this.roundN(paymentFee.agropago_fee * tenantConfig.agropago_iva_rate, 2);
    
            paymentFee.total_fee = this.roundN(paymentFee.operation_fee + paymentFee.payment_method_fee + paymentFee.agropago_fee, 2);
            paymentFee.total_fee_iva = this.roundN(paymentFee.operation_fee_iva + paymentFee.payment_method_fee_iva + paymentFee.agropago_fee_iva, 2);
            paymentFee.total = this.roundN(paymentFee.total_fee + paymentFee.total_fee_iva, 2);
        } catch(ex) {
            if (ex instanceof NotFoundException) {
                throw ex;
            } else {
                throw new InternalServerErrorException(ex);
            }
        }
        return Promise.resolve(paymentFee);
    }

    roundN(num, n) {
        return Number(parseFloat((Math.round(num * Math.pow(10, n)) / Math.pow(10,n)).toString()).toFixed(n));
    }

}

interface PaymentMethodType {
    calculateFee(tenantConfig: TenantConfig, paymentMethod: any, options: any, amount: number) : PaymentMethodTypeFee;
}

class PaymentMethodTypeCreditCard implements PaymentMethodType {

    calculateFee(tenantConfig: TenantConfig, paymentMethod: any, options: any, amount: number) : PaymentMethodTypeFee {
        const result = new PaymentMethodTypeFee()
        let installment_fee = 0.00;
        let term_fee = 0.00;
        let installment_fee_iva = 0.00;
        let term_fee_iva = 0.00;

        if (options.term >= 0) {
            const termResult = paymentMethod.options.terms
                            .filter(term => { return term.term === options.term });
            if (termResult.length > 0 ) {
                /*
                * Calculo fee para dias de plazos basado en TNA
                */
                const term = termResult[0];
                if (term.rate > 0) {
                    const PERIODO = Math.trunc((term.term - 1) / 365) + 1;
                    const TNA = term.rate;
                    const TEA = (1 + TNA / PERIODO) * PERIODO - 1;
                    const TEM = Math.pow(( 1 + TEA), (1 / 12)) - 1;
                    const TED = Math.pow(( 1 + TEM), (1 / 30)) - 1;
    
                    term_fee = amount  * TED * term.term;
                    term_fee_iva = term_fee * tenantConfig.term_iva_rate
                }
            } else {
                throw new NotFoundException(`options.term: ${options.term} not found.`);
            }
        }

        if (options.installment >= 0) {
            const installmentResult = paymentMethod.options.installments
                            .filter(installment => { return installment.installment === options.installment});
            if (installmentResult.length > 0) {
                /*
                * Calculo fee para cuotas basado en TNA
                */
                const installment = installmentResult[0];
                if (installment.rate > 0) {
                    const PERIODO = Math.trunc((installment.installment - 1) / 12) + 1;
                    const TNA = installment.rate;
                    const TEA = (1 + TNA / PERIODO) * PERIODO - 1;
                   
                    installment_fee = amount * TEA;
                    installment_fee_iva = installment_fee * tenantConfig.installment_iva_rate
                }
            } else {
                throw new NotFoundException(`options.installment: ${options.installment} not found.`);
            }
        }

        result.fee = term_fee + installment_fee
        result.fee_iva = term_fee_iva + installment_fee_iva
        return result;
    }
    
}

export class PaymentFee  {
    operation_fee               : number = 0;
    operation_fee_iva           : number = 0;
    payment_method_fee          : number = 0;
    payment_method_fee_iva      : number = 0;
    agropago_fee                : number = 0;
    agropago_fee_iva            : number = 0;
    total_fee                   : number = 0;
    total_fee_iva               : number = 0;
    total                       : number = 0;
}

class PaymentMethodTypeFee  {
    fee        : number
    fee_iva    : number
}
