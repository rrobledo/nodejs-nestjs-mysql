import { PigmaService } from './external/pigma';
import { Calc } from './utils/calc';
import { ExternalModule } from './external/external.module';
import { UtilsModule } from './utils/utils.module';
import { OrdersService } from 'services/orders.service';
import { PaymentMethodService } from 'services/payment_method.service';
import { ItemsService } from './items.service';
import { Module } from '@nestjs/common';

import { LoggerModule } from 'utils/logger.module';
import { ConfigModule } from '@nestjs/config';
import { RepositoriesModule } from 'repositories/repositories.module';
import { AuthModule } from './auth/auth.module';
import { TransactionsService } from './transactions.service';

@Module({
  imports: [LoggerModule, ConfigModule, RepositoriesModule, AuthModule, UtilsModule, ExternalModule],
  providers: [ItemsService, PaymentMethodService, Calc, OrdersService, TransactionsService],
  exports: [ItemsService, PaymentMethodService, Calc, OrdersService, TransactionsService],
})

export class ServicesModule {}
