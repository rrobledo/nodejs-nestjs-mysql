import { PaymentOrder } from 'model/agropago/orders/payment_order';
import { IsDefined } from 'class-validator';
import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, JoinColumn } from 'typeorm';

@Entity({'name': 'payment_order_detail'})
export class PaymentOrderDetail {
    @PrimaryGeneratedColumn()
    @IsDefined()
    id: number;

    @Column()
    item_id: string;

    @Column()
    @IsDefined()
    description: string;

    @Column({"type": "float"})
    @IsDefined()
    amount: number;

    @IsDefined()
    @ManyToOne(type => PaymentOrder, paymentOrder => paymentOrder.items)
    @JoinColumn({ name: "payment_order_id", referencedColumnName: "id"})
    payment_order: PaymentOrder;

}

export class PaymentOrderDetailCreate {
    @IsDefined()
    item_id: string;

    @IsDefined()
    description: string;

    @IsDefined()
    amount: number;
}
