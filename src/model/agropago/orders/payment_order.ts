import { Users } from './../users/users';
import { PaymentOrderDetail, PaymentOrderDetailCreate } from './payment_order_detail';
import { IsDefined, ValidateNested } from 'class-validator';
import { Entity, Column, PrimaryGeneratedColumn, OneToMany, JoinColumn, ManyToOne } from 'typeorm';
import { Payment } from '../transactions/payment';
import { Type } from 'class-transformer';


@Entity({'name': 'payment_order'})
export class PaymentOrder {
    @PrimaryGeneratedColumn()
    @IsDefined()
    id: number;

    @PrimaryGeneratedColumn('uuid')
    @IsDefined()
    uuid: string;
    
    @Column('date')
    @IsDefined()
    date: Date;

    @Column()
    @IsDefined()
    status: string;

    @Column()
    @IsDefined()
    beneficiary_id: number;

    @ManyToOne(type => Users, user => user.id)
    @JoinColumn({ name: "beneficiary_id", referencedColumnName: "id"})
    beneficiary: Users;

    @Column()
    @IsDefined()
    payer_email: string;

    @Column()
    @IsDefined()
    payer_id: number;

    @ManyToOne(type => Users, user => user.id)
    @JoinColumn({ name: "payer_id", referencedColumnName: "id"})
    payer: Users;

    @Column('json')
    accepted_payment_methods: [
        {
            "id": number,
            "options": object,
            "fees": object
        }];

    @Column()
    @IsDefined()
    reference_number: string;

    @Column()
    @IsDefined()
    invoice_number: string;

    @Column()
    @IsDefined()
    description: string;

    @Column({"type": "float"})
    @IsDefined()
    amount: number;

    @Column({"type": "float"})
    amount_to_transfer: number;

    @Column({"type": "float"})
    available_amount: number;

    @OneToMany(type => PaymentOrderDetail, item => item.payment_order)
    items: PaymentOrderDetail[];

    @OneToMany(type => Payment, item => item.payment_order)
    payments: Payment[];
}

@Entity({'name': 'payment_order'})
export class PaymentOrderReduced {
    @PrimaryGeneratedColumn()
    @IsDefined()
    id: number;

    @PrimaryGeneratedColumn('uuid')
    @IsDefined()
    uuid: string;
    
    @Column('date')
    @IsDefined()
    date: Date;

    @Column()
    @IsDefined()
    status: string;

    @ManyToOne(type => Users, user => user.id)
    @JoinColumn({ name: "beneficiary_id", referencedColumnName: "id"})
    beneficiary: Users;

    @ManyToOne(type => Users, user => user.id)
    @JoinColumn({ name: "payer_id", referencedColumnName: "id"})
    payer: Users;

    @Column()
    @IsDefined()
    reference_number: string;

    @Column()
    @IsDefined()
    invoice_number: string;

    @Column()
    @IsDefined()
    description: string;

    @Column({"type": "float"})
    @IsDefined()
    amount: number;

    @OneToMany(type => PaymentOrderDetail, item => item.payment_order)
    items: PaymentOrderDetail[];

    @OneToMany(type => Payment, item => item.payment_order)
    payments: Payment[];
}

export class PaymentOrderCreate {
    @IsDefined()
    payer_email: string;

    @IsDefined()
    accepted_payment_methods: [{
            "id": number,
            "options": object
        }];

    @IsDefined()
    reference_number: string;

    @IsDefined()
    invoice_number: string;

    @IsDefined()
    description: string;

    @ValidateNested({ each: true })
    @Type(() => PaymentOrderDetailCreate)
    items: PaymentOrderDetailCreate[];

    @IsDefined()
    amount: number;
}

export class PaymentOrderReducedCreate {
    @IsDefined()
    payer_email: string;

    @IsDefined()
    reference_number: string;

    @IsDefined()
    invoice_number: string;

    @IsDefined()
    description: string;

    @ValidateNested({ each: true })
    @Type(() => PaymentOrderDetailCreate)
    items: PaymentOrderDetailCreate[];

    @IsDefined()
    amount: number;
}
