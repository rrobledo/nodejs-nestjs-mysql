import { IsDefined } from 'class-validator';
import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity({'name': 'payment_method'})
export class PaymentMethod {
    @IsDefined()
    @PrimaryGeneratedColumn()
    id: number;
  
    @Column()
    @IsDefined()
    bank_id: number;

    @Column()
    @IsDefined()
    name: string;

    @Column('json')
    @IsDefined()
    assets: {
        "logo": string,
        "cc_logo": string
    };

    @Column()
    @IsDefined()
    payment_type: string;

    @Column({"type": "float"})
    @IsDefined()
    operation_rate: number;

    @Column('json')
    @IsDefined()
    options: any

}

export class PaymentMethodCalcInput {
    @IsDefined()
    payment_method_id: number;

    @IsDefined()
    options: any

    @IsDefined()
    amount: number;
}
