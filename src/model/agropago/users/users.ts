import { IsDefined } from 'class-validator';
import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity({'name': 'users'})
export class Users {
    @IsDefined()
    @PrimaryGeneratedColumn()
    id: number;
  
    @Column()
    @IsDefined()
    tenant_id: number;

    @Column()
    @IsDefined()
    email: string;

    @Column()
    cuit: string;

    @Column()
    legal_name: string;

    @Column()
    is_active: boolean;

}

