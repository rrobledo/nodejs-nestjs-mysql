import { PaymentFee } from './../../../services/utils/calc';
import { Payment } from 'model/agropago/transactions/payment';
import { Users } from 'model/agropago/users/users';
import { PaymentOrder } from 'model/agropago/orders/payment_order';
import { IsDefined, ValidateNested } from 'class-validator';
import { Entity, Column, PrimaryGeneratedColumn, OneToMany, JoinColumn, ManyToOne } from 'typeorm';
import { Type } from 'class-transformer';


@Entity({'name': 'transaction_values'})
export class TransactionValues {
    @PrimaryGeneratedColumn()
    @IsDefined()
    id: number;

    @IsDefined()
    @Column('date')
    process_date: Date;
    
    @IsDefined()
    @Column()
    operation_type: string;

    @ManyToOne(type => Users, user => user.id)
    @JoinColumn({ name: "user_id", referencedColumnName: "id"})
    user: Users;

    @ManyToOne(type => Users, user => user.id)
    @JoinColumn({ name: "afected_id", referencedColumnName: "id"})
    afected: Users;

    @Column()
    @IsDefined()
    status: string;

    @IsDefined()
    @Column()
    payment_type: string;

    @IsDefined()
    @Column()
    payment_method_id: number;

    @IsDefined()
    @Column()
    currency: string;

    @Column({"type": "float"})
    @IsDefined()
    amount: number;

    @Column()
    payment_id: number;

    @IsDefined()
    @ManyToOne(type => Payment, payment => payment.values)
    @JoinColumn({ name: "payment_id", referencedColumnName: "id"})
    payment: Payment;

    @IsDefined()
    options: any;

    fees: PaymentFee;
}

export class TransactionValuesCreate {
    @IsDefined()
    payment_type: string;

    @IsDefined()
    payment_method_id: number;

    @IsDefined()
    amount: number;

    @IsDefined()
    options: any;

    fees: PaymentFee;
}