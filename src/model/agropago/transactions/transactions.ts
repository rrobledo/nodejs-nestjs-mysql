import { IsDefined } from 'class-validator';
import { Entity, Column, PrimaryColumn } from 'typeorm';


@Entity({'name': 'transactions'})
export class AgroTransactions {
    @PrimaryColumn()
    @IsDefined()
    id: number;

    @Column()
    @IsDefined()
    operation_type: string;
 
    @Column()
    @IsDefined()
    operation_mode: string;

    @Column()
    @IsDefined()
    deb_cred: string;

    @Column()
    @IsDefined()
    uuid: string;

    @Column()
    @IsDefined()
    code: string;

    @Column()
    @IsDefined()
    user_id: number;
    
    @Column()
    @IsDefined()
    user: string;

    @Column('date')
    @IsDefined()
    date: Date;

    @Column()
    @IsDefined()
    description: string;

    @Column()
    @IsDefined()
    ticket: string;

    @Column()
    @IsDefined()
    status: string;

    @Column()
    @IsDefined()
    target_id: number;
    
    @Column()
    @IsDefined()
    target: string;

    @Column({"name": "amount", "type": "float"})
    @IsDefined()
    amount: number;
}