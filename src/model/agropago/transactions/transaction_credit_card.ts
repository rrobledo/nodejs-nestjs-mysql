import { Payment } from 'model/agropago/transactions/payment';
import { Users } from 'model/agropago/users/users';
import { PaymentOrder } from 'model/agropago/orders/payment_order';
import { IsDefined, ValidateNested } from 'class-validator';
import { Entity, Column, PrimaryGeneratedColumn, OneToMany, JoinColumn, ManyToOne } from 'typeorm';
import { Type } from 'class-transformer';


@Entity({'name': 'transaction_credit_card'})
export class TransactionCreditCard {
    @PrimaryGeneratedColumn()
    @IsDefined()
    id: number;

    @Column()
    @IsDefined()
    payment_method_id: number;

    @Column()
    @IsDefined()
    token: string;

    @Column()
    @IsDefined()
    token_agro: string;

    @Column()
    @IsDefined()
    owner_name: string;
  
    @Column()
    @IsDefined()
    term: number;

    @Column()
    @IsDefined()
    installment: number;

    @Column()
    @IsDefined()
    bin: number;

    @Column()
    @IsDefined()
    last_four_digits: number;

    @Column()
    @IsDefined()
    expiration_month: number;

    @Column()
    @IsDefined()
    expiration_year: number;
  
    @Column({"type": "float"})
    @IsDefined()
    amount: number;

    @Column({"type": "float"})
    amount_to_conciliate: number;

    @Column({"type": "float"})
    amount_conciliated: number;

    @Column()
    aut: string;

    @Column()
    cupon: string;

    @Column()
    @IsDefined()
    observations: string;

    @Column()
    @IsDefined()
    status: string;

    @Column('date')
    process_date: Date;

    @Column('date')
    conciliation_date: Date;

    @Column('date')
    transaction_code: Date;
}

@Entity({'name': 'transaction_credit_card'})
export class TransactionCreditCardReduced {
    @PrimaryGeneratedColumn()
    @IsDefined()
    id: number;

    @Column()
    @IsDefined()
    payment_method_id: number;

    @Column()
    @IsDefined()
    token: string;

    @Column()
    @IsDefined()
    owner_name: string;

    @Column()
    @IsDefined()
    term: number;

    @Column()
    @IsDefined()
    installment: number;

    @Column()
    @IsDefined()
    bin: number;

    @Column()
    @IsDefined()
    last_four_digits: number;

    @Column({"type": "float"})
    @IsDefined()
    amount: number;

    @Column()
    aut: string;

    @Column()
    cupon: string;

    @Column()
    @IsDefined()
    observations: string;

    @Column()
    @IsDefined()
    status: string;

    @Column('date')
    process_date: Date;
}
