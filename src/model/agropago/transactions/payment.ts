import { PaymentOrderReduced, PaymentOrderReducedCreate } from './../orders/payment_order';
import { PaymentFee } from './../../../services/utils/calc';
import { TransactionValuesCreate, TransactionValues } from './transaction_values';
import { PaymentOrder } from 'model/agropago/orders/payment_order';
import { IsDefined, ValidateNested } from 'class-validator';
import { Entity, Column, PrimaryGeneratedColumn, OneToMany, JoinColumn, ManyToOne } from 'typeorm';
import { Type } from 'class-transformer';


@Entity({'name': 'payment'})
export class Payment {
    @PrimaryGeneratedColumn()
    @IsDefined()
    id: number;

    @PrimaryGeneratedColumn('uuid')
    @IsDefined()
    uuid: string;
    
    @Column('date')
    @IsDefined()
    date: Date;

    @Column()
    @IsDefined()
    status: string;

    @Column({"name": "amount", "type": "float"})
    @IsDefined()
    total: number;

    @IsDefined()
    payment_order_id: number;

    @IsDefined()
    @ManyToOne(type => PaymentOrder, paymentOrder => paymentOrder.payments)
    @JoinColumn({ name: "payment_order_id", referencedColumnName: "id"})
    payment_order: PaymentOrder;

    @Column('json')
    fees: PaymentFee;

    @IsDefined()
    @OneToMany(type => TransactionValues, item => item.payment)
    values: TransactionValues[]
}

@Entity({'name': 'payment'})
export class PaymentReduced {
    @PrimaryGeneratedColumn()
    @IsDefined()
    id: number;

    @PrimaryGeneratedColumn('uuid')
    @IsDefined()
    uuid: string;
    
    @Column('date')
    @IsDefined()
    date: Date;

    @Column()
    @IsDefined()
    status: string;

    @Column({"name": "amount", "type": "float"})
    @IsDefined()
    total: number;

    @IsDefined()
    @ManyToOne(type => PaymentOrder, paymentOrder => paymentOrder.payments)
    @JoinColumn({ name: "payment_order_id", referencedColumnName: "id"})
    payment_order: PaymentOrderReduced;

    @Column('json')
    fees: PaymentFee;

    @IsDefined()
    @OneToMany(type => TransactionValues, item => item.payment)
    values: TransactionValues[]
}

export class PaymentReducedCreate {
    @ValidateNested({ each: true })
    @Type(() => TransactionValuesCreate)
    @IsDefined()
    values: TransactionValuesCreate[]
}

export class PaymentFullCreate {
    @IsDefined()
    payment_order: PaymentOrderReducedCreate;

    @ValidateNested({ each: true })
    @Type(() => TransactionValuesCreate)
    @IsDefined()
    values: TransactionValuesCreate[];
}

export class PaymentCreate {
    @IsDefined()
    payment_order_id: number;

    @ValidateNested({ each: true })
    @Type(() => TransactionValuesCreate)
    @IsDefined()
    values: TransactionValuesCreate[]
}
