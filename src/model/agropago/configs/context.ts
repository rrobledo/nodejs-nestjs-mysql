export class Context {
    tenant_id: number;
    user_id: number;
    default_tenant_id: number = 1;

    constructor(tenant_id: number, user_id: number) {
        this.tenant_id = tenant_id;
        this.user_id = user_id;
    }
}
