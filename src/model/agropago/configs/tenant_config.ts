import { IsDefined } from 'class-validator';
import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity({'name': 'tenant_config'})
export class TenantConfig {
    @IsDefined()
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    @IsDefined()
    decidir_api_key: string;

    @Column()
    @IsDefined()
    payment_api_key: string;

    @Column({"type": "float"})
    agropago_rate: number;

    @Column({"type": "float"})
    agropago_iva_rate: number;

    @Column({"type": "float"})
    operation_iva_rate: number;

    @Column({"type": "float"})
    term_iva_rate: number;

    @Column({"type": "float"})
    installment_iva_rate: number;
   
}

