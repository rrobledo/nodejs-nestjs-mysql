import { AgroTransactions } from './../../model/agropago/transactions/transactions';
import { TransactionCreditCard, TransactionCreditCardReduced } from './../../model/agropago/transactions/transaction_credit_card';
import { Users } from './../../model/agropago/users/users';
import { PaymentOrderDetail } from './../../model/agropago/orders/payment_order_detail';
import { TenantConfig } from './../../model/agropago/configs/tenant_config';
import { PaymentOrder, PaymentOrderReduced } from './../../model/agropago/orders/payment_order';
import { PaymentMethod } from './../../model/agropago/payment_method/payment_method';
import { Module, HttpModule, HttpService } from '@nestjs/common';
import { LoggerModule } from 'utils/logger.module';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Payment, PaymentReduced } from 'model/agropago/transactions/payment';
import { TransactionValues } from 'model/agropago/transactions/transaction_values';

@Module({
  imports: [LoggerModule, 
            ConfigModule,
            TypeOrmModule.forRootAsync({
              imports: [ConfigModule],
              useFactory: (configService: ConfigService) => ({
                type: 'mysql',
                host: configService.get('mysql.host'),
                port: configService.get<number>('mysql.port'),
                username: configService.get('mysql.username'),
                password: configService.get('mysql.password'),
                database: configService.get('mysql.database'),
                entities: [
                  Users,
                  PaymentMethod,
                  PaymentOrder,
                  PaymentOrderReduced,
                  PaymentOrderDetail,
                  Payment,
                  PaymentReduced,
                  AgroTransactions,
                  TransactionValues,
                  TransactionCreditCard,
                  TransactionCreditCardReduced,
                  TenantConfig
                ],
                synchronize: false,
              }),
              inject: [ConfigService],
            }),
            TypeOrmModule.forFeature([
              Users,
              TenantConfig,
              PaymentMethod,
              PaymentOrder,
              PaymentOrderReduced,
              Payment,
              PaymentReduced,
              AgroTransactions,
              TransactionValues,
              TransactionCreditCard,
              TransactionCreditCardReduced,
              TenantConfig,
            ])
           ],
  providers: [],
  exports: [TypeOrmModule],
})

export class MysqlModule {}
