import { TransactionsRepository } from 'repositories/transactions.repository';
import { PaymentOrderDetail } from './../../model/agropago/orders/payment_order_detail';
import { InjectRepository } from '@nestjs/typeorm';
import { OrdersRepository } from './../orders.repository';
import { LoggerService } from 'nest-logger';
import { ConfigService } from '@nestjs/config';
import { PaymentOrder } from 'model/agropago/orders/payment_order';
import { Injectable } from '@nestjs/common';
import { Connection, Repository } from 'typeorm';
import * as MysqljUtils from './utils.mysql';

@Injectable()
export class OrdersRepositoryMysql extends OrdersRepository {

  constructor(private readonly config: ConfigService,
              private readonly logger: LoggerService,
              private connection: Connection,
              private repoTransactions: TransactionsRepository,
              @InjectRepository(PaymentOrder) private repoPaymentOrder: Repository<PaymentOrder>) {
      super();
    }

  async addPaymentOrder(paymentOrder: PaymentOrder): Promise<PaymentOrder> {
    const queryRunner = this.connection.createQueryRunner();
    await queryRunner.connect();
    await queryRunner.startTransaction();
    try {
      await queryRunner.manager.save(paymentOrder);
      for (let item of paymentOrder.items) {
        const itemToSave = new PaymentOrderDetail();
        itemToSave.item_id = item.item_id;
        itemToSave.description = item.description;
        itemToSave.amount = item.amount;
        itemToSave.payment_order = paymentOrder;
        await queryRunner.manager.save(itemToSave);
      };
    await queryRunner.commitTransaction();
    } catch (err) {
      await queryRunner.rollbackTransaction();
      throw err;
    } finally {
      await queryRunner.release();
    }
    return this.repoPaymentOrder.findOne({
      where: { id : paymentOrder.id },
      relations: ["beneficiary", "payer", "items", "payments"]}
      );
  }

  async getPaymentOrderById(paymentOrderId: number): Promise<PaymentOrder> {
    return this.repoPaymentOrder.findOne({
              where: { id : paymentOrderId },
              relations: ["beneficiary", "payer", "items"]
            });
  }

  async getPaymentOrderByFilter(filter: string, offset: number, limit: number): Promise<PaymentOrder[]> {
    let where = MysqljUtils.mongoDBQueryToMysqlWhere(filter, 'PaymentOrder');
    const orders = await this.repoPaymentOrder.find({
                            where: where,
                            relations: ["beneficiary", "payer", "items", "payments", "payments.values"]
                          });
    await Promise.all(orders.map(async order => {
      await this.repoTransactions.completePaymentValues(order.payments);
    }));
    return Promise.resolve(orders);
  }

  async cancelPaymentOrder(paymentOrder: PaymentOrder): Promise<any> {
    paymentOrder.status = 'CANCELED';
    return this.repoPaymentOrder.save(paymentOrder);
  }
}
