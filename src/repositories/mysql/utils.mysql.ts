import * as MongoParse from 'mongo-parse';
import { BadRequestException } from '@nestjs/common';

const getTypeAsString = (data): string => {
    if (typeof data === 'string') {
        return `'${data.toString()}'`;
    } else {
        return data.toString();
    }
};

const generateWhere = (parts, objRef: string): string => {
    let ret: string = '';

    parts.forEach(part => {
        if (part.operator === '$and') {
            let andOperand = '';
            part.parts.forEach(andPart => {
                if (andPart.parts.length > 0) {
                    andOperand = andOperand.concat(generateWhere(andPart.parts, objRef), ' AND ');
                }
            });
            ret = ret.concat(`(${andOperand.substr(0, andOperand.length - 4)})`);
        } else if (part.operator === '$or') {
            let orOperand = '';
            part.parts.forEach(orPart => {
            orOperand = orOperand.concat(generateWhere(orPart.parts, objRef), ' OR ');
            });
            ret = ret.concat(`(${orOperand.substr(0, orOperand.length - 4)})`);
        } else if (part.operator === '$eq') {
            if (objRef === '' ) {
                ret = `${part.field} = ${getTypeAsString(part.operand)}`;
            } else if (part.field.startsWith('__')) {
                ret = `${part.field.substr(2)} = ${getTypeAsString(part.operand)}`;
            } else {
                ret = `${objRef}.${part.field} = ${getTypeAsString(part.operand)}`;
            }
        } else if (part.operator === '$contain') {
            if (objRef === '') {
                ret = `${part.field} LIKE '%${part.operand}%'`;
            } else if (part.field.startsWith('__')) {
                ret = `${part.field.substr(2)} LIKE '%${part.operand}%'`;
            } else {
                ret = `${objRef}.${part.field} LIKE '%${part.operand}%'`;
            }
        } else if (part.operator === '$ne') {
            if (objRef === '') {
                ret = `${part.field} <> ${getTypeAsString(part.operand)}`;
            } else if (part.field.startsWith('__')) {
                ret = `${part.field.substr(2)} <> ${getTypeAsString(part.operand)}`;
            } else {
                ret = `${objRef}.${part.field} <> ${getTypeAsString(part.operand)}`;
            }
        } else if (part.operator === '$gt') {
            if (objRef === '') {
                ret = `${part.field} > ${getTypeAsString(part.operand)}`;
            } else if (part.field.startsWith('__')) {
                ret = `${part.field.substr(2)} > ${getTypeAsString(part.operand)}`;
            } else {
                ret = `${objRef}.${part.field} > ${getTypeAsString(part.operand)}`;
            }
        } else if (part.operator === '$gte') {
            if (objRef === '') {
                ret = `${part.field} >= ${getTypeAsString(part.operand)}`;
            } else if (part.field.startsWith('__')) {
                ret = `${part.field.substr(2)} >= ${getTypeAsString(part.operand)}`;
            } else {
                ret = `${objRef}.${part.field} >= ${getTypeAsString(part.operand)}`;
            }
        } else if (part.operator === '$lt') {
            if (objRef === '') {
                ret = `${part.field} < ${getTypeAsString(part.operand)}`;
            } else if (part.field.startsWith('__')) {
                ret = `${part.field.substr(2)} < ${getTypeAsString(part.operand)}`;
            } else {
                ret = `${objRef}.${part.field} < ${getTypeAsString(part.operand)}`;
            }
        } else if (part.operator === '$lte') {
            if (objRef === '') {
                ret = `${part.field} <= ${getTypeAsString(part.operand)}`;
            } else if (part.field.startsWith('__')) {
                ret = `${part.field.substr(2)} <= ${getTypeAsString(part.operand)}`;
            } else {
                ret = `${objRef}.${part.field} <= ${getTypeAsString(part.operand)}`;
            }
        } else if (part.operator === '$in') {
            const inOperand = part.operand.reduce((acc, val) => getTypeAsString(acc).concat(',', getTypeAsString(val)));
            if (objRef === '') {
                ret = `${part.field} IN (${inOperand})`;
            } else if (part.field.startsWith('__')) {
                ret = `${part.field.substr(2)} IN (${inOperand})`;
            } else {
                ret = `${objRef}.${part.field} IN (${inOperand})`;
            }
        } else if (part.operator === '$where') {
            ret = 'TRUE';
        }
    });
    return ret;
};

const mongoDBQueryToMysqlWhere = (filter: string, objRef: string): string => {
    try {
        const query = MongoParse.parse(JSON.parse(filter));
        return generateWhere(query.parts, objRef);
    } catch (exception) {
        throw new BadRequestException(exception.toString());
    }
};

export {
    mongoDBQueryToMysqlWhere,
};
