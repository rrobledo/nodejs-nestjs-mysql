import { AgroTransactions } from './../../model/agropago/transactions/transactions';
import { TenantConfig } from './../../model/agropago/configs/tenant_config';
import { Context } from './../../model/agropago/configs/context';
import { TransactionCreditCard, TransactionCreditCardReduced } from './../../model/agropago/transactions/transaction_credit_card';
import { TransactionValues } from 'model/agropago/transactions/transaction_values';
import { InjectRepository } from '@nestjs/typeorm';
import { LoggerService } from 'nest-logger';
import { ConfigService } from '@nestjs/config';
import { Injectable, ForbiddenException } from '@nestjs/common';
import { Connection, Repository } from 'typeorm';
import { TransactionsRepository } from 'repositories/transactions.repository';
import { Payment, PaymentReduced } from 'model/agropago/transactions/payment';
import * as MysqljUtils from './utils.mysql';

@Injectable()
export class TransactionsRepositoryMysql extends TransactionsRepository {

  constructor(private readonly config: ConfigService,
              private readonly logger: LoggerService,
              private connection: Connection,
              @InjectRepository(PaymentReduced) private repoPaymentReduced: Repository<PaymentReduced>,
              @InjectRepository(TransactionCreditCard) private repoCreditCard: Repository<TransactionCreditCard>,
              @InjectRepository(AgroTransactions) private repoTransactions: Repository<AgroTransactions>,
              @InjectRepository(TenantConfig) private repoTenantConfig: Repository<TenantConfig>) {
      super();
    }

  async addPayment(context: Context, payment: Payment): Promise<PaymentReduced> {
    const queryRunner = this.connection.createQueryRunner();
    await queryRunner.connect();
    await queryRunner.startTransaction();

    try {
      payment.status = 'REVIEW';
      const paymentSaved = await queryRunner.manager.save(payment);

      const transactionValueFee = new TransactionValues();
      transactionValueFee.operation_type = 'FEE';
      transactionValueFee.user = payment.payment_order.beneficiary;
      transactionValueFee.afected = payment.payment_order.payer;
      transactionValueFee.payment_type = 'VIRTUAL_CASH';
      transactionValueFee.currency = "ARS";
      transactionValueFee.amount = payment.fees.total;
      transactionValueFee.payment = paymentSaved;
      transactionValueFee.status = 'REVIEW';
      await queryRunner.manager.save(transactionValueFee);

      for (let value of payment.values) {
        const transactionValue = new TransactionValues();
				transactionValue.operation_type = 'PAYMENT';
				transactionValue.user = payment.payment_order.payer;
        transactionValue.afected = payment.payment_order.beneficiary;
        transactionValue.payment_type = value.payment_type;
        transactionValue.payment_method_id = value.payment_method_id;
        transactionValue.amount = value.amount;
				transactionValue.currency = 'ARS';
				transactionValue.payment = paymentSaved;
        transactionValue.status = 'REVIEW';
        const transactionValueSaved = await queryRunner.manager.save(transactionValue);

        if (transactionValue.payment_type = 'CREDIT_CARD') {
          const transactionCreditCard = new TransactionCreditCard()
          transactionCreditCard.id = transactionValueSaved.id;
          transactionCreditCard.payment_method_id = value.payment_method_id;
          transactionCreditCard.owner_name = value.options.owner_name;
          transactionCreditCard.term = value.options.term;
          transactionCreditCard.installment = value.options.installment;
          transactionCreditCard.token =  value.options.token;
          transactionCreditCard.token_agro = value.options.token_agro;
          transactionCreditCard.bin = value.options.bin;
          transactionCreditCard.last_four_digits = value.options.last_four_digits;
          transactionCreditCard.expiration_month = value.options.expiration_month;
          transactionCreditCard.expiration_year = value.options.expiration_year;
          transactionCreditCard.amount = value.amount;
          transactionCreditCard.amount_to_conciliate = value.amount - (value.fees.total - value.fees.agropago_fee - value.fees.agropago_fee_iva);
          transactionCreditCard.status = 'REVIEW';
          await queryRunner.manager.save(transactionCreditCard);
        }
      }
      await queryRunner.commitTransaction();
    } catch (err) {
      await queryRunner.rollbackTransaction();
      throw err;
    } finally {
      await queryRunner.release();
    }
    return this.getPaymentById(payment.id);
  }

  async deletePayment(paymentId: number): Promise<any> {
    const queryRunner = this.connection.createQueryRunner();
    await queryRunner.connect();
    await queryRunner.startTransaction();

    try {
      // get payment
      const payment = await this.repoPaymentReduced.findOne({
        where: { id : paymentId },
        relations: ["values"]}
      );

      // delete all values
      await Promise.all(payment.values.map(async value => {
        await queryRunner.manager
          .createQueryBuilder()
          .delete()
          .from(TransactionCreditCard)
          .where("id = :id", {id: value.id})
          .execute();

        await queryRunner.manager
          .createQueryBuilder()
          .delete()
          .from(TransactionValues)
          .where("id = :id", {id: value.id})
          .execute();
      }));

      // delete payment
      await queryRunner.manager
      .createQueryBuilder()
      .delete()
      .from(Payment)
      .where("id = :id", {id: paymentId})
      .execute();

      await queryRunner.commitTransaction();
    } catch (err) {
      await queryRunner.rollbackTransaction();
      throw err;
    } finally {
      await queryRunner.release();
    }
    return Promise.resolve();
  }

  async getPaymentById(paymentId: number): Promise<PaymentReduced> {
    const payment = await this.repoPaymentReduced.findOne({
                      where: { id : paymentId },
                      relations: ["payment_order", "values"]}
                    );
    await this.completePaymentValues([payment]);
    return Promise.resolve(payment);            
  }

  async completePaymentValues(payments: PaymentReduced[]): Promise<any> {
    await Promise.all(payments.map(async payment => {
      await Promise.all(payment.values.map(async value => {
        if (value.payment_type === 'CREDIT_CARD') {
          const creditCardValue = await this.repoCreditCard.findOne({where: {id : value.id}});
          value.options = creditCardValue;
        }
      }))
    }))
  }

  async getPaymentsByFilter(filter: string, offset: number, limit: number): Promise<PaymentReduced[]> {
    let where = MysqljUtils.mongoDBQueryToMysqlWhere(filter, 'PaymentReduced');
    const payments = await this.repoPaymentReduced.find({
                            where: where,
                            relations: ["payment_order", "values"]
                          });

    await this.completePaymentValues(payments);
    return Promise.resolve(payments);
  }

  async getTransactionsByFilter(filter: string, offset: number, limit: number): Promise<AgroTransactions[]> {
    let where = MysqljUtils.mongoDBQueryToMysqlWhere(filter, 'AgroTransactions');
    return this.repoTransactions.find({
                            where: where
                          });
  }

  /*
  async cancelPayment(paymentId: string): Promise<any> {
    return;
  }
  */
}
