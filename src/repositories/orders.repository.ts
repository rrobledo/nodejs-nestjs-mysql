import { PaymentOrder } from 'model/agropago/orders/payment_order';
import { Injectable } from '@nestjs/common';

@Injectable()
export class OrdersRepository {
  async addPaymentOrder(paymentOrder: PaymentOrder): Promise<PaymentOrder> {
    return;
  }

  async getPaymentOrderById(paymentOrderId: number): Promise<PaymentOrder> {
    return;
  }

  async getPaymentOrderByFilter(filter: string, offset: number, limit: number): Promise<PaymentOrder[]> {
    return;
  }

  async cancelPaymentOrder(paymentOrder: PaymentOrder): Promise<any> {
    return;
  }
}
