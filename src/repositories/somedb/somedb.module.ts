import { ConnectionNeo4j } from './connection.somedb';
import { Module } from '@nestjs/common';

import { LoggerModule } from 'utils/logger.module';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [LoggerModule, ConfigModule],
  providers: [ConnectionNeo4j],
  exports: [ConnectionNeo4j],
})

export class SomeDBModule {}
