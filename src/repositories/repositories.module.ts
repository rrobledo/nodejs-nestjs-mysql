import { ExternalModule } from './../services/external/external.module';
import { TransactionsRepositoryMysql } from './mysql/transactions.mysql';
import { TransactionsRepository } from './transactions.repository';
import { OrdersRepository } from './orders.repository';
import { OrdersRepositoryMysql } from './mysql/orders.mysql';
import { ConnectionNeo4j } from './somedb/connection.somedb';
import { SomeDBModule } from './somedb/somedb.module';
import { MysqlModule } from './mysql/mysql.module';
import { Module } from '@nestjs/common';

import { LoggerModule } from 'utils/logger.module';
import { ConfigModule } from '@nestjs/config';
import { ConfigService } from '@nestjs/config';
import { LoggerService } from 'nest-logger';

import { ItemsRepository } from './items.repository';
import { ItemsRepositoryNeo4j } from './somedb/items.neo4j';

@Module({
  imports: [
    LoggerModule, 
    ConfigModule,
    SomeDBModule,
    MysqlModule
  ],
  providers: [
    {
      provide: ItemsRepository,
      useFactory: (config: ConfigService, logger: LoggerService, conn: ConnectionNeo4j) => {
        return new ItemsRepositoryNeo4j(config, logger, conn);
      },
      inject: [ConfigService, LoggerService, ConnectionNeo4j]
    },
    {
      provide: OrdersRepository,
      useClass: OrdersRepositoryMysql
    },
    {
      provide: TransactionsRepository,
      useClass: TransactionsRepositoryMysql
    }
  ],
  exports: [ItemsRepository, MysqlModule, OrdersRepository, TransactionsRepository],
})

export class RepositoriesModule {}
