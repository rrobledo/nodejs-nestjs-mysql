import { AgroTransactions } from './../model/agropago/transactions/transactions';
import { Context } from './../model/agropago/configs/context';
import { Payment, PaymentReduced } from 'model/agropago/transactions/payment';
import { Injectable } from '@nestjs/common';

@Injectable()
export class TransactionsRepository {
  async addPayment(context: Context, payment: Payment): Promise<PaymentReduced> {
    return;
  }

  async deletePayment(paymentId: number): Promise<any> {
    return;
  }

  async getPaymentById(paymentId: number): Promise<PaymentReduced> {
    return;
  }

  async completePaymentValues(payments: Payment[]): Promise<any> {
    return;
  }

  async getPaymentsByFilter(filter: string, offset: number, limit: number): Promise<PaymentReduced[]> {
    return;
  }

  async getTransactionsByFilter(filter: string, offset: number, limit: number): Promise<AgroTransactions[]> {
    return;
  }

  /*
  async cancelPayment(paymentId: string): Promise<any> {
    return;
  }
  */
}
