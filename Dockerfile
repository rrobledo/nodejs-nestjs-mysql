FROM ubuntu:18.04

RUN cd ~ \
    && apt-get update \ 
    && apt-get install -y curl \
    && curl -sL https://deb.nodesource.com/setup_12.x -o nodesource_setup.sh \
    && bash nodesource_setup.sh \
    && apt-get install -y nodejs \
    && nodejs -v \
    && npm -v 

RUN npm install pm2@latest -g \
    && pm2 install typescript

RUN mkdir /shared \
    && chmod 777 /shared \
    && mkdir /shared/logs

ADD node_modules /usr/src/app/node_modules
ADD dist /usr/src/app/dist
ADD ts* /usr/src/app/
ADD package* /usr/src/app/
ADD nest* /usr/src/app/
ADD README.md /usr/src/app/
WORKDIR /usr/src/app

CMD ["pm2-runtime", "dist/main.js", "-i", "max"]
