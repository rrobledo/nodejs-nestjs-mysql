## Configure dasboard:

https://docs.aws.amazon.com/eks/latest/userguide/dashboard-tutorial.html

## Connect to the dashboard

```
kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep eks-admin | awk '{print $1}')
kubectl proxy
```

1. To access the dashboard endpoint, open the following link with a web browser: 

http://localhost:8001/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/#!/login

2. Choose Token, paste the <authentication_token> output from the previous command into the Token field, and choose SIGN IN.


