https://github.com/helm/charts/tree/master/incubator/kafka

helm repo add incubator http://storage.googleapis.com/kubernetes-charts-incubator

helm install kafka incubator/kafka -f values.yaml --name kafka --namespace sandbox

