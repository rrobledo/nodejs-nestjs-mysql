https://github.com/Kong/kong-operator/tree/master/helm-charts/kong#kong-service-parameters

helm repo add kong https://charts.konghq.com

helm delete kong --namespace sandbox
helm install kong kong/kong -f values.yaml --namespace sandbox
