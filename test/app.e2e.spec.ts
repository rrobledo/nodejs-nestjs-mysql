import { ConfigModule } from '@nestjs/config';
import { LoggerModule } from './../src/utils/logger.module';
import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import { HttpExceptionFilter } from 'utils/http-exception.filter';
import { ErrorFilter } from 'utils/error.filter';
import { ValidationPipe } from '@nestjs/common';
import * as request from 'supertest';
import * as casual from 'casual';
import { RepositoriesModule } from 'repositories/repositories.module';
import configuration from 'config/configuration';
import { ControllersModule } from 'api/rest/controllers/controllers.module';

describe('Payment Method', () => {
  let app: INestApplication;

  process.env.ENTERPRISE_API_URL = "https://api-sandbox.agropago.com/api/v1";
  process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [LoggerModule,
        ControllersModule,
        ConfigModule.forRoot({
                                load: [configuration],
                            }),
        RepositoriesModule
      ],
    }).compile();

    app = moduleFixture.createNestApplication();
    app.useGlobalFilters(new ErrorFilter());
    app.useGlobalFilters(new HttpExceptionFilter());
    app.useGlobalPipes(new ValidationPipe());
    app.enableCors();
    await app.init();
  });

  describe('Paymen 2 Faces', () => {
    let paymentOrderId = 0;
    let agroToken = "";
    let authToken = "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwidXNlcl9pZCI6IjEiLCJqdGkiOiJkNmY2ZDAyZDYyYWI0YmVhYWI5YjFhMTkxMDAxZjMyOSIsInN1YiI6ImV5SnliMnhsSWpvZ0lrRkVUVWxPSWl3Z0luVnpaWEpKWkNJNklERXNJQ0owWlc1aGJuUkpaQ0k2SURGOSIsImV4cCI6MTU5MTY2NTcwNn0.se6vO2pjuKh-_i-0XBlCT8ymB0YemE0RKUYwGpH5_do";

    it('POST /orders/payment_orders', () => {
      return request(app.getHttpServer())
      .post('/orders/payment_orders')
      .set('Authorization', authToken)
      .send(
      {
        "payer_email": "agropago.test02@gmail.com",
        "amount": 1000,
        "description": "Venta de Quimicos",
        "reference_number": "222222",
        "invoice_number": "234-234213",
        "accepted_payment_methods": [
          {
          "id": 2,
          "options": {
            "term": 30,
            "installment": 1
            }
        }
        ],
        "items": [
          {
            "item_id": 21212,
            "description": "Kripton",
            "amount": 1000
          }
        ]
      })
      .set('Accept', 'application/json')
      //.expect(201)
      .then(response => {
        paymentOrderId = response.body.id;
      });
    });

    it('POST developers.decidir.com/api/v2/tokens', () => {
      return request("https://developers.decidir.com")
      .post('/api/v2/tokens')
      .set('apikey', 'ca5b6ce5ac5048e196ece9506a2592ce')
      .send({
        "card_number": "4765900000421873 ",
        "card_expiration_month": "10",
        "card_expiration_year": "25",
        "security_code": "123",
        "card_holder_name": "Robledo Raul",
        "card_holder_identification": {
          "type": "DNI",
          "number": "22247322"
        }
      })
      .set('Accept', 'application/json')
      .expect(201)
      .then(response => {
        agroToken = response.body.id;
      });
    });

    it('POST /transactions/payment_orders/{id}/payments', () => {
      return request(app.getHttpServer())
      .post(`/transactions/payment_orders/${paymentOrderId}/payments`)
      .set('Authorization', authToken)
      .send({
        "values": [
          {
            "payment_type": "CREDIT_CARD",
            "payment_method_id": 2,
            "amount": 1000,      
              "options": {
                "token": agroToken,
                "term": 30,
                "installment": 1,
                "token_agro": "",
                "token_type": "T",
                "owner_name": "Raul Robledo",
                "bin": 476590,
                "expiration_year": 25,
                "expiration_month": 10,
                "last_four_digits": 1873
              }
          }
        ]
      })
      .set('Accept', 'application/json')
      .expect(201)
      .then(response => {
        console.log(response.body)
      });
    });
  
    afterAll(async () => {
      await app.close();
    });

  });
});
